<?php
/**
 * BuddyBoss Child Theme
 * > Assets management
 * > Theme functions
 *
 * @package BuddyBoss
 * @since 3.0
 */


/**
 * Sets up theme defaults
 *
 * @since BuddyBoss 3.0
 */
function buddyboss_child_setup()
{
  /*
   * Makes BuddyBoss Child available for translation.
   *
   * Translations can be added to the /languages/ directory.
   * If you're building a theme based on BuddyBoss, use a find and replace
   * to change 'buddyboss_child' to the name of your theme in all the template files.
   */
  load_theme_textdomain( 'buddyboss_child', get_stylesheet_directory() . '/languages' );

  /*
   * Add extra nav menus:
   */

  // register_nav_menus( array(
  //   'primary-menu'   => __( 'Child Menu Name' ),
  //   'secondary-menu' => __( 'Child Footer Menu' ),
  // ) );

  /*
   * Add custom image sizes
   */
  // add_image_size( 'buddyboss_child_gallery_tn', 175, 175, true );
}
add_action( 'after_setup_theme', 'buddyboss_child_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since BuddyBoss 3.0
 */
function buddyboss_child_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/


  /*
   * SCRIPTS: All scripts are loaded in /buddyboss/buddyboss-inc/theme.php
   *
   * Scripts (Handle - Filepath):
   * buddyboss-navigation                 /buddyboss/js/navigation.js
   * touch-swipe                          /buddyboss/js/jquery.touchSwipe.min.js
   * idangerous-swiper                    /buddyboss/js/idangerous.swiper.js
   * buddyboss-mobile-main                /buddyboss/js/mobile-main.js
   */

   /*
   * Enqueue a custom script
   */
   // wp_enqueue_script( 'handle-name', get_stylesheet_directory_uri() . '/js/javascript-file.js', array('jquery'), '1.0', true );


  /*
   * STYLES: All styles are loaded in /buddyboss/buddyboss-inc/theme.php
   */

  /*
   * Load wordpress.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-wp-frontend' );
   // wp_enqueue_style( 'buddyboss-child-wp-frontend', get_stylesheet_directory_uri().'/css/wordpress.css' );

  /*
   * Load buddypress.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-buddypress-frontend' );
   // wp_enqueue_style( 'buddyboss-child-buddypress-frontend', get_stylesheet_directory_uri().'/css/buddypress.css' );

  /*
   * Load bbpress.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-bbpress-frontend' );
   // wp_enqueue_style( 'buddyboss-child-bbpress-frontend', get_stylesheet_directory_uri().'/css/bbpress.css' );

  /*
   * Load plugins.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-wp-plugins' );
   // wp_enqueue_style( 'buddyboss-child-wp-plugins', get_stylesheet_directory_uri().'/css/plugins.css' );

  /*
   * Load adminbar-mobile.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-wp-adminbar-responsive' );
   // wp_enqueue_style( 'buddyboss-child-wp-adminbar-responsive', get_stylesheet_directory_uri().'/css/adminbar-mobile.css' );

  /*
   * Load adminbar-floating.css or adminbar-fixed.css from your child theme
   */
   // wp_deregister_style( 'buddyboss-wp-adminbar-floating' );
   // wp_deregister_style( 'buddyboss-wp-adminbar-fixed' );
   // $adminbar_float_status = get_option("buddyboss_adminbar", 0);
   // $adminbar_type = $adminbar_float_status == 0
   //               ? 'adminbar-floating'
   //               : 'adminbar-fixed';
   // wp_enqueue_style( 'buddyboss-child-wp-'.$adminbar_type, get_template_directory_uri().'/css/'.$adminbar_type.'.css' );


  /*
   * Styles
   */
  wp_enqueue_style( 'buddyboss-child-custom', get_stylesheet_directory_uri().'/css/custom.css' );
}
add_action( 'wp_enqueue_scripts', 'buddyboss_child_scripts_styles', 9999 );




  /*
   * Add your custom functions here
   */





?>