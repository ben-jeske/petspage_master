<?php
/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss/buddyboss-inc/theme.php
 */

/**
 * Add your own function files in /buddyboss-child-inc/.
 *
 * It's recommended to add custom functionality in that folder
 * and follow our naming convention, then loading the file in
 * child-init.php
 *
 * Naming convention: Try to semantically describe the functionality
 * Portfolio theme: /buddyboss-child-inc/portfolio.php (contains portfolio related functions)
 * Business directory theme: /buddyboss-child-inc/directory.php (contains directory related functions)
 */

$init_file = get_stylesheet_directory() . '/buddyboss-child-inc/child-init.php';

if ( ! file_exists( $init_file ) )
{
  $err_msg = __( 'BuddyBoss cannot find the child theme\'s starter file, should be located at: *wp root*/wp-content/themes/*child_theme_folder*/buddyboss-child-inc/child-init.php', 'buddyboss' );

  wp_die( $err_msg );
}

require_once( $init_file );



?>