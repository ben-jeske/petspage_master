<?php
/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since BuddyBoss 3.0
 */

function buddyboss_child_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'buddyboss', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'buddyboss' instances in all child theme files to 'buddyboss_child'.
  // load_theme_textdomain( 'buddyboss_child', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'buddyboss_child_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since BuddyBoss 3.0
 */
function buddyboss_child_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */
  wp_enqueue_style( 'buddyboss-child-custom', get_stylesheet_directory_uri().'/css/custom.css' );
}
add_action( 'wp_enqueue_scripts', 'buddyboss_child_scripts_styles', 9999 );


/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here

/* Add additional information to user profile's header */
add_action('bp_before_member_header_meta','bp_before_member_header_profile_fields');
function bp_before_member_header_profile_fields() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	$res2 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=2"); 
	foreach($res2 as $row)
	{
		$profile_desc = $row->value;
	}
	
	if($profile_desc != '')
	{
		echo '<h2>About</h2><p>'.$profile_desc.'</p>';
	}
}

add_role('Ambassador', 'Ambassador', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
    'delete_posts' => false, // Use false to explicitly deny
));

add_action('bp_member_header_tagline','bp_member_header_get_tagline');
function bp_member_header_get_tagline() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	
	$res3 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=3"); 
	foreach($res3 as $row)
	{
		$profile_tagline = $row->value;
	}
	
	if($profile_tagline != '')
	{
		echo '<blockquote style="padding:24px 0;">&#8212; '.$profile_tagline.'</blockquote>';
	}
}

//  Re-direct not-logged-in users to holding page
//
if(!is_user_logged_in() && (curPageURL() == 'https://petspage.com/')) {
    wp_redirect( 'https://petspage.com/member_register/', 302 );
    exit;
}

//  Get current page URL
//
function curPageURL() {

    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

    $pageURL .= "://";
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    /*
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }*/
    //echo  $pageURL ;
    return $pageURL;
}
 
/**
 * Users Rank Progress
 * @version 1.0
 */
function mycred_pro_users_rank_progress( $bp_displayed_user_id ) {
        // Load myCRED
        $mycred = mycred_get_settings();
 
        // Ranks are based on a total
        if ( $mycred->rank['base'] == 'total' )
                $key = $mycred->get_cred_id() . '_total';
        // Ranks are based on current balance
        else
                $key = $mycred->get_cred_id();
 
        // Get Balance
        $users_balance = $mycred->get_users_cred( $bp_displayed_user_id, $key );
       
        // Rank Progress
       
        // Get the users current rank post ID
        $users_rank = (int) mycred_get_users_rank( $bp_displayed_user_id, 'ID' );
       
        // Get the ranks set max
        $max = get_post_meta( $users_rank, 'mycred_rank_max', true );
       
// Calculate progress. We need a percentage with 1 decimal
if ( $users_balance > 0 )
   $progress = number_format( ( ( $users_balance / $max ) * 100 ), 1 );
else
   $progress = 0;
       
        // Visual Composer customization
        $options = '';
       
        // If progress is less then 100%, use the stipped animation to indicate progress
        if ( $progress < number_format( 100, 1 ) )
                $options = 'options="striped,animated"';
        // Else use the fixed background color to indicate completion
        else
                $progress = number_format( 100, 1 );
?>
        <?php echo do_shortcode( '[vc_row][vc_column width="1/1"][vc_progress_bar values="' . $progress . '|' . get_the_title( ++$users_rank ) . ' Progress -' . '" bgcolor="custom" ' . $options . ' custombgcolor="#563b73" title="" units="%"]<p><span class="description"></p>[/vc_column][/vc_row]' ); ?>
 
<?php
}

if(isset($_POST['profile-group-submit']))
{
	global $current_user;
	get_currentuserinfo();
	$curr_id = $current_user->ID;
	
	$upgrade = false;
	foreach($current_user -> roles as $role)
	{
		if(strtolower($role) == 'administrator' or strtolower($role) == 'upgrade' or strtolower($role) == 'ambassador')
		{
			$upgrade = true;
		}
	}
	
	$curr_id = $current_user->ID;
	$curr_username = $current_user->user_login;
	$user_id = $curr_id;
	$do_update = false;
	// Save BuddyPress Display name
	if(isset($_POST['public_value']))
	{
		if($upgrade)
		{
			if($_POST['public_value'] == 'profile')
			{
				if(isset($_POST['display_name']))
				{
					$display = trim(strip_tags($_POST['display_name']));
					$do_update = true;
				}
			}
			else if($_POST['public_value'] == 'business')
			{
				if(isset($_POST['business']))
				{
					$display = trim(strip_tags($_POST['business']));
					$do_update = true;
				}
			}
		}
		else
		{
			if(isset($_POST['display_name']))
			{
				$display = trim(strip_tags($_POST['display_name']));
				$do_update = true;
			}
		}	
	}
	elseif(isset($_POST['display_name']))
	{
		$display = trim(strip_tags($_POST['display_name']));
		$do_update = true;
	}
	
	if($do_update)
	{
		$table1 = "wp_bp_xprofile_data";
		$data_array1 = array('value' => $display);
		$where1 = array('user_id' => $user_id, 'field_id' => 1);
		$wpdb->update( $table1, $data_array1, $where1 );
	}
	
	// Save Description Field
	if(isset($_POST['profile_desc']))
	{
		// Check if field exits
		
		$profile_desc = trim(stripslashes($_POST['profile_desc']));
		$table1 = "wp_bp_xprofile_data";
		
		$data_array_desc = array('value' => $profile_desc);
		$where_desc = array('user_id' => $user_id, 'field_id' => 2);
		
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 2', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$wpdb->insert( $table1, array_merge($data_array_desc, $where_desc) );
		}
	}
	
	// Save Tagline Field
	if(isset($_POST['profile_tagline']))
	{
		$profile_tagline = trim(stripslashes($_POST['profile_tagline']));
		$table1 = "wp_bp_xprofile_data";
		$data_array_desc = array('value' => $profile_tagline);
		$where_desc = array('user_id' => $user_id, 'field_id' => 3);
		
		$data = array();
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 3', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$wpdb->insert( $table1, array_merge($data_array_desc, $where_desc) );
		}
	}
	
	// Save Zipcode Field
	if(isset($_POST['profile_zipcode']))
	{
		$profile_value = trim(stripslashes($_POST['profile_zipcode']));
		$table1 = "wp_bp_xprofile_data";
		$data_array_desc = array('value' => $profile_value);
		$where_desc = array('user_id' => $user_id, 'field_id' => 6);
		
		$data = array();
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 6', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$data_array_insert['user_id'] = $user_id;
			$data_array_insert['field_id'] = '6';
			$data_array_insert['value'] = $profile_value;
			
			$wpdb->insert( $table1, $data_array_insert, array('%d', '%d', '%s') );
		}
	}

if(isset($_POST['profile_date']))
	{
		$profile_value = trim(stripslashes($_POST['profile_date']));
		$table1 = "wp_bp_xprofile_data";
		$data_array_desc = array('value' => $profile_value);
		$where_desc = array('user_id' => $user_id, 'field_id' => 25);
		
		$data = array();
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 25', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$data_array_insert['user_id'] = $user_id;
			$data_array_insert['field_id'] = '25';
			$data_array_insert['value'] = $profile_value;
			
			$wpdb->insert( $table1, $data_array_insert, array('%d', '%d', '%s') );
		}
	}
	
	// Save Display Name and Business Names to the WP_USER
	if(isset($_POST['display_name']))
	{
		$wp_display_name = trim(strip_tags($_POST['display_name']));
		//update_user_meta($user_id, 'display_name', $wp_display_name);
		$table2 = "wp_users";
		$data_array2 = array('display_name' => $wp_display_name);
		$where2 = array('ID' => $user_id);
		$wpdb->update( $table2, $data_array2, $where2 );
	}
	
	if($upgrade && isset($_POST['business']))
	{
		$wp_business_name = trim(strip_tags($_POST['business']));
		$table3 = "wp_users";
		$data_array3 = array('business' => $wp_business_name);
		$where3 = array('ID' => $user_id);
		$wpdb->update( $table3, $data_array3, $where3 );
	}
	
	// Save Additional Fields
	
	$field_list = array(
		16 => 'field_16',
		17 => 'field_17',
		18 => 'field_18',
		19 => 'field_19',
		20 => 'field_20',
		21 => 'field_21',
		22 => 'field_22',
		23 => 'field_23',
		24 => 'field_24'
	);
	
	foreach($field_list as $id => $field_id)
	{
		if($upgrade && isset($_POST[$field_id]))
		{
			$value = trim(stripslashes($_POST[$field_id]));
			$table1 = "wp_bp_xprofile_data";
			$data_array_desc = array('value' => $value);
			$where_desc = array('user_id' => $user_id, 'field_id' => $id);
			
			$data = array();
			$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = '.$id, ARRAY_A);
			
			if(count($data) > 0)
			{
				$wpdb->update( $table1, $data_array_desc, $where_desc );
			}
			else
			{
				$wpdb->insert( $table1, array_merge($data_array_desc, $where_desc) );
			}
		}
	}
	
	// Redirect
  	$path = esc_url( home_url('/pet-lovers/') ).$curr_username.'/profile/edit/group';
  	header("Location: $path");
}

if ( function_exists( 'mycred_label' ) ) :
        /**
         * Step 1: Register Widget
         * @version 1.0
         */
        add_action( 'widgets_init', 'mycred_register_custom_widgets' );
        function mycred_register_custom_widgets()
        {
            register_widget( 'myCRED_Widget_This_Weeks_Leaderboard' );
        }
 
        /**
         * Step 2: Construct Widget
         * @version 1.2
         */
        if ( ! class_exists( 'myCRED_Widget_This_Weeks_Leaderboard' ) ) {
                class myCRED_Widget_This_Weeks_Leaderboard extends WP_Widget {
 
                        // Constructor
                        public function __construct() {
                                $name = mycred_label();
                                // Basic details about our widget
                                $widget_ops = array(
                                        'classname'   => 'widget-mycred-this-weeks-leaderboard',
                                        'description' => __( 'Show the leaderboard for a given timeframe.', 'mycred' )
                        );
                                $this->WP_Widget( 'mycred_widget_this_weeks_leaderboard', __( 'This weeks Leaderboard', 'mycred' ), $widget_ops );
                                $this->alt_option_name = 'mycred_widget_this_weeks_leaderboard';
                        }
 
                        // Widget Output (what users see)
                        public function widget( $args, $instance ) {
                                extract( $args, EXTR_SKIP );
 
                                // Check if we want to show this to visitors
                                if ( ! $instance['show_visitors'] && ! is_user_logged_in() ) return;
                       
                                // Get the leaderboard
                                $leaderboard = $this->get_leaderboard( $instance['number'], $widget_id );
                       
                                // Load myCRED
                                $mycred = mycred_get_settings();
 
                                // Start constructing Widget
                                echo $before_widget;
                       
                                // Title (if not empty)
                                if ( ! empty( $instance['title'] ) ) {
                                        echo $before_title;
                                        // Allow general tempalte tags in the title
                                        echo $mycred->template_tags_general( $instance['title'] );
                                        echo $after_title;
                                }
 
                                // Construct unorganized list for each row
                                echo '<ul class="mycred-this-weeks-leaderboard">';
                                foreach ( $leaderboard as $position => $data ) {
                                        echo '<li class="dt"><span>' . $data->display_name . '</span></li> <li class="dd"> ' . $mycred->format_creds( $data->total ) . '      </li>';
                                }
                                echo '</ul>';
 
                                echo $after_widget;
                        }
 
                        // Widget Settings (when editing / setting up widget)
                        public function form( $instance ) {
                                $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'This Weeks Leaderboard', 'mycred' );
                                $number = isset( $instance['number'] ) ? abs( $instance['number'] ) : 5;
                                $show_visitors = isset( $instance['show_visitors'] ) ? 1 : 0; ?>
 
<p class="myCRED-widget-field">
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'mycred' ); ?>:</label>
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p class="myCRED-widget-field">
        <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of users', 'mycred' ); ?>:</label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo $number; ?>" size="3" class="align-right" />
</p>
<p class="myCRED-widget-field">
        <input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'show_visitors' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'show_visitors' ) ); ?>" value="1"<?php checked( $show_visitors, 1 ); ?> class="checkbox" />
        <label for="<?php echo esc_attr( $this->get_field_id( 'show_visitors' ) ); ?>"><?php _e( 'Visible to non-members', 'mycred' ); ?></label>
</p>
<?php if ( isset( $this->id ) && $this->id !== false ) : ?>
<p class="myCRED-widget-field">
        <input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'reset_leaderboard' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'reset_leaderboard' ) ); ?>" value="1" class="checkbox" />
        <label for="<?php echo esc_attr( $this->get_field_id( 'reset_leaderboard' ) ); ?>"><?php _e( 'Reset Leaderboard', 'mycred' ); ?></label>
</p>
<?php
                                endif;
                        }
 
                        // Save Widget Settings
                        public function update( $new_instance, $old_instance ) {
                                $instance = $old_instance;
                                $instance['number'] = (int) $new_instance['number'];
                                $instance['title'] = trim( $new_instance['title'] );
                                $instance['show_visitors'] = $new_instance['show_visitors'];
                       
                                if ( isset( $new_instance['reset_leaderboard'] ) && $this->id !== false ) {
                                        delete_transient( 'mycred_twl_' . $this->id );
                                }
 
                                mycred_flush_widget_cache( 'mycred_widget_this_weeks_leaderboard' );
                                return $instance;
                        }
 
                        // Grabs the leaderboard
                        public function get_leaderboard( $number = 5, $widget_id = '' ) {
                                // Get transient
                                $data = get_transient( 'mycred_twl_' . $widget_id );
                                $this_week = date_i18n( 'W' );
                       
                                // If a transient is set, check if it is time for an update
                                if ( $data !== false ) {
                                        reset( $data );
                                        $saved_week = key( $data );
                                        // Compare the first array key (which holds the week number) to this weeks
                                        // If they do not match, it is a new week and we need to make a new query.
                                        // Same goes for empty results (new sites)
                                        if ( $this_week != $saved_week || empty( $data[ $saved_week ] ) )
                                                $data = false;
                                        // Else return the results
                                        else
                                                $data = $data[ $this_week ];
                                }
 
                                // If transient does not exist or a new number is set, do a new DB Query       
                                if ( $data === false || $number != 5 ) {
                                        // Start over
                                        $data = array();
                               
                                        // Load the wpdb class
                                        global $wpdb;
                               
                                        $mycred = $wpdb->prefix . 'myCRED_log';
                                        $SQL = "
SELECT m.user_id, u.display_name, sum( m.creds ) AS total
FROM {$mycred} m
LEFT JOIN {$wpdb->users} u
        ON u.ID = m.user_id
WHERE ( m.time >= %d AND m.time <= %d AND m.creds > 0 )
GROUP BY m.user_id
ORDER BY total DESC LIMIT 0,%d;";
                               
                                        // Save results under this week
                                        $now = date_i18n( 'U' );
                                        $last_week = strtotime( "-1 week" );
                                        $data[ $this_week ] = $wpdb->get_results( $wpdb->prepare( $SQL, $last_week, $now, $number ) );
                                        // Save results for a week
                                        set_transient( 'mycred_twl_' . $widget_id, $data, WEEK_IN_SECONDS );
                                        $data = $data[ $this_week ];
                                }
                       
                                return $data;
                        }
                }
        }
endif;

?>