<table class="notifications">
<div class="span6" data-step="2" data-intro="Ok, wasn't that fun?" data-position='right'>
	<thead>
		<tr>
			<th class="icon"></th>
			<th class="title"><?php _e( 'Notification', 'buddypress' ); ?></th>
			<th class="date"><?php _e( 'Date Received', 'buddypress' ); ?></th>
			<th class="actions"><?php _e( 'Actions',    'buddypress' ); ?></th>
		</tr>
	</thead>
</div>

	<tbody>

		<?php while ( bp_the_notifications() ) : bp_the_notification(); ?>

			<tr>
				<td></td>
				<td><?php bp_the_notification_description();  ?></td>
				<td><?php bp_the_notification_time_since();   ?></td>
				<td><?php bp_the_notification_action_links(); ?></td>
			</tr>

		<?php endwhile; ?>

	</tbody>
    
        <script type="text/javascript">
      if (RegExp('multipage', 'gi').test(window.location.search)) {
        introJs().start();
      }
    </script>
</table>