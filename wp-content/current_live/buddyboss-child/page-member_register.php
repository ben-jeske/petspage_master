<?php
if (is_user_logged_in())
{
	header('Location: '.site_url());
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  id="facebook" class="" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><?php wp_head(); ?>
<meta name="viewport" content="width=device-width">
<title>PetsPage Login</title>
<link href="/new_register_user_files/css/style.css" rel="stylesheet" type="text/css" />
<link href="/new_register_user_files/fonts/font-style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="/new_register_user_files/css/SDOHtEcw6fD.css">
<link type="text/css" rel="stylesheet" href="/new_register_user_files/css/VDoI332ncGf.css">
<script src="/new_register_user_files/js/jquery-1.11.0.min.js"></script>
<script src="/new_register_user_files/js/zebra_tooltips.js"></script>
<script src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="small-device.css" />
<script type="text/javascript">

// OneAll Social Library
var oa = document.createElement('script');
oa.type = 'text/javascript'; oa.async = true;
oa.src = '//petspage.api.oneall.com/socialize/library.js'
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(oa, s)

// Google reCaptcha Initalization
function showRecaptcha() {
	Recaptcha.create("6LcahvASAAAAAPwAb9MAD5WPItvtQBIUVL8dEg-S", 'recaptcha', {
	theme: "clean"
	});
}
</script>
<style>
.modal-box { display:none;position:fixed; z-index:1000; background-color:rgba(0,0,0,0.7); top:0; left:0; width:100%; height:100%; }
.modal-box .modal-box-inner { background-color:#fff; border:solid 5px #300948; border-radius:10px; padding:20px; margin:10% auto; width:300px;}
.modal-box form { margin: 20px 0; }
.modal-box form label { display:block; margin:5px 0;}
.logon_form p {
	margin-top:30px !important;
	position:relative;
}

.logon_form p a#lost_password_link {
	position:absolute;
	bottom:-20px;
	left:0;
}

.logon_form form p.sign input.submit_button {
	margin-top:16px;
}

.logon_form p ._5dbc, .logon_form p ._5dbd{
	display:none;position:absolute;right:29px;top:5px
}

.logon_form p._5634 ._5dbc, .logon_form p._5634 ._5dbd{
	display:inline;
}

#lost_password_popup p {
	position:relative;
}

#lost_password_popup p ._5dbc {
	display:none;position:absolute;right:4px;top:22px
}

#lost_password_popup p._5634 ._5dbc {
	display:inline-block;
}

#lost_password_popup p #user_login {
	display: block;
	width: 100%;
	box-sizing: border-box;
}
</style>

</head>

<body>

<div class="modal-box" id="message_box_popup">
    <div class="modal-box-inner">
        <h2 id="modal_heading"></h2>
        <p id="modal_message"></p>
    </div>
</div>

<div class="modal-box" id="lost_password_popup">
    <div class="modal-box-inner">
        <form action="https://petspage.com/my-account/lost-password/?redirect_to" method="post" class="lost_reset_password">
	<p>Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.</p>
	<p class="form-row form-row-first">
	<label for="user_login">Username or email</label> <input class="input-text" type="text" name="user_login" id="user_login" title="username or email address is required">
	<i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i>
	</p>
	<div class="clear"></div>
	<p class="form-row"><input type="submit" class="button" name="reset" value="Reset Password"></p>
	<input type="hidden" id="_n" name="_n" value="dda1c05b62"><input type="hidden" name="_wp_http_referer" value="/member_register/?resetsent=1">
	</form>
    </div>
</div>


<div id="pets_login">
  <div id="login_wrapper">
    <div class="login_header">
      <div class="login_logo"><a href=""><img src="/new_register_user_files/images/new-logo2.png" alt="" /></a></div>
      <div class="logon_form">
          <h2>Already a registered member? Sign in here or use your Social account to login below!</h2>
        <form id="login_form" name="loginform" action="https://petspage.com/wp-login.php" method="post">
          <p>
            <input type="text" id="login_username" placeholder="Username" value="" name="log" title="Please enter the username you registered with" >
            <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i>
          </p>
          <p>
            <input type="password" id="login_password" placeholder="Password" value="" name="pwd" title="Please enter the password you registered with">
            <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i><!--<a href="<?php echo wp_lostpassword_url( ); ?>" id="lost_password_link">Forgot your password?</a>--><a href="https://petspage.com/wp-login.php?action=lostpassword" style="position: absolute;bottom: -20px;left: 0;">Forgot your password?</a>
          </p>
           <p class="sign">
           <input name="wp-submit" type="submit" class="submit_button" value="Log In" />
           <input type="hidden" name="redirect_to" value="https://petspage.com/">
           <input type="hidden" name="testcookie" value="1">
           </p>
        </form>
        
        
      </div>
    </div>
    <div class="login_section">
    <h2>Welcome to PetsPage.com - A Place for Social Pet Lovers!</h2>
    <div class="login_section_left">
<?php 
$result = $wpdb->get_results( "SELECT * FROM ".$wpdb -> prefix."users");	
?>
<?php
foreach($result as $row)
{
?>
<?php
global $bp;
if(bp_core_fetch_avatar( array( 'item_id' => $row->ID, 'no_grav' => true,'html'=> false ) ) != bp_core_avatar_default())
{
   //echo bp_core_fetch_avatar( array( 'item_id' => $row->ID, 'no_grav' => true,'html'=> false ) );
$set_pic .= $row->ID.",";
}
?>
<?php
}
global $bp;
$set_pic = substr($set_pic, 0, -1);
$set_users = explode(",",$set_pic);
$i = 0;
shuffle($set_users);
?>
<div class="pets_images">
<?php
foreach($set_users as $key=>$value)
{
if($i==6) break;
?>
<div class="pets_ima_inner">
<?php //echo get_avatar($value,180); 
?>
  <?php 
  echo bp_core_fetch_avatar( array(
    'item_id' => $value,
    'type'     => 'full',
    'width'    => 150,
    'height'  => 150
));
?>
</div>
<?php
$i++;
} 
?>
</div>
    <!--  
    <div class="pets_images">
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/image32.png" alt="" />
    </div>
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/images1.png" alt="" />
    </div>
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/images2.png" alt="" />
    </div>
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/images3.png" alt="" />
    </div>
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/images4.png" alt="" />
    </div>
    <div class="pets_ima_inner">
    <img src="/new_register_user_files/images/images5.png" alt="" />
    </div>
    </div>
    end -->
    <h4 style="text-align: center;">Where Pet Lovers from around the world come to<br /> share, socialize, and most importantly, smile! <br />You'll love our Social Pets!</h4>
    </div>
    <div class="login_section_right">
    <h3 style="margin-bottom: 10px;">Join to enter our $4,000 Prize Giveaway with 80 chances to win!</h3>
      
		 <?php do_action('oa_social_login'); ?>
                
    <span style="position: relative; top: -13px;">    
        <p>Or fill out the registration below - It takes less than 2 minutes</p>
      <div class="_li">
  <div id="globalContainer" class="uiContextualLayerParent">
    <div id="content" class="fb_content clearfix">
      <div class="pvl">
              <div class="_6_ _74">
                <div class="_58mf">
                      <div id="reg_box" class="registration_redesign">
                        <form method="post" id="reg" name="reg" action="/registermember/">
                          <div id="reg_form_box" class="large_form">
                            <div class="clearfix _58mh">
                              <div class="mrm lfloat _ohe">
                                <div class="_5dbb" id="u_0_0">
                                  <div class="uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput">
                                    <div aria-hidden="true" class="placeholder">Your Name</div>
                                    <input class="inputtext _58mg _5dba displayname" data-type="text" name="displayname" aria-required="1" placeholder="" id="u_0_1" aria-label="First Name" type="text" title="Enter your First and Last Name">
                                  </div>
                                  <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                              </div>
                              <div class="mbm rfloat _ohf">
                                <div class="_5dbb" id="u_0_2">
                                  <div class="uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput">
                                    <div aria-hidden="true" class="placeholder">Username</div>
                                    <input class="inputtext _58mg _5dba username" data-type="text" name="username" aria-required="1" placeholder="" id="u_0_3" aria-label="Last Name" type="text" title="Enter a Username. You will use this to login with">
                                  </div>
                                  <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                              </div>
                            </div>
                            <div class="mbm">
                              <div class="_5dbb" id="u_0_4">
                                <div class="uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput">
                                  <div aria-hidden="true" class="placeholder">Your Email</div>
                                  <input class="inputtext _58mg _5dba emailaddress" data-type="text" name="email" aria-required="1" placeholder="" id="u_0_5" aria-label="Your Email" type="text" title="Enter a valid Email address. We'll send you a verification">
                                </div>
                                <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                            </div>
                            <div class="mbm" id="u_0_6">
                              <div class="_5dbb" id="u_0_7">
                                <div class="uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput">
                                  <div aria-hidden="true" class="placeholder">Password</div>
                                  <input class="inputtext _58mg _5dba password" data-type="text" name="password" aria-required="1" placeholder="" id="u_0_8" aria-label="Password" type="password" title="Enter a password with at least one number and a Capital letter">
                                </div>
                                <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                            </div>
                            <div class="mbm">
                              <div class="_5dbb" id="u_0_9">
                                <div class="uiStickyPlaceholderInput uiStickyPlaceholderEmptyInput">
                                  <div aria-hidden="true" class="placeholder">Confirm Password</div>
                                  <input class="inputtext _58mg _5dba confirmpassword" data-type="text" name="password_confirm" aria-required="1" placeholder="" id="u_0_a" value="" aria-label="Confirm Password" type="password" title="Retype your password from above">
                                </div>
                                <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                            </div>
                            
                            
                            <div class="mbm">
                              <div class="_5dbb" id="u_0_11" style="font-size:12px">
                                  <label><input type="checkbox" id="agreement" name="agreement" value="1" title="You need to agree with our Terms of Use to Sign Up" /> <span style="position:relative; top:-2px;">By signing up, you agree to our <a href="http://www.petspage.com/Terms-Agreement.pdf">Terms of Use</a>.</span></label>
                                <i class="_5dbc img sp_2xp1ac sx_2bac47"></i><i class="_5dbd img sp_2xp1ac sx_429f2b"></i></div>
                            </div>
                            
                            <div class="mbm">
                              <div class="_5dbb" id="recaptcha"></div>
                            </div>
                            
                            <div class="_58mu" id="u_0_h">
                              <p class="_58mv"></p>
                            </div>
                            <div class="clearfix">
                              <button type="submit" class="_6j mvm _6wk _6wl _58mi _3ma _6o _6v" name="websubmit" id="u_0_i">Sign Up</button>
                              <span class="hidden_elem _58ml" id="u_0_l"><img class="img" src="Welcome%20to%20Facebook%20-%20Log%20In,%20Sign%20Up%20or%20Learn%20More_files/GsNJNwuI-UM.gif" alt="" height="11" width="16"></span></div>
                          </div>
                        </form>
                        <div id="reg_error" class="_58mn hidden_elem">
                          <div id="reg_error_inner" class="_58mo">An error occurred. Please try again.</div>
                        </div>
                        <div id="reg_pages_msg" class="_58mk"></div>
                      </div>
                    </div>
              </div>
            </div>
      
    </div>
  </div>
</div>
      </span>

<script src="/new_register_user_files/js/main.js"></script>
            
    </div>
    </div>
  </div>
  <div class="login_footer">
  <div id="login_wrapper">
  <div id="footer-credits">
                <p>&copy; 2014 PetsPage &nbsp;&middot;&nbsp; <a style="color: white;" href="https://www.petspage.com/Terms-Agreement.pdf">Terms of Use &#38; Privacy Policy</a></p>
            </div>
            <div class="footer_social">
            	<ul>
                  <li><div class="social_1"><a target="_blank" href="https://www.facebook.com/mypetadvocate">&nbsp;</a></div></li>
                  <li><div class="social_2"><a target="_blank" href="https://twitter.com/KarenBostick">&nbsp;</a></div></li>
                  <li><div class="social_8"><a target="_blank" href="http://www.linkedin.com/company/pets-page">&nbsp;</a></div></li>
                  <li><div class="social_6"><a target="_blank" href="http://www.youtube.com/user/PetsPageTV">&nbsp;</a></div></li>
                  <li><div class="social_3"><a target="_blank" href="http://instagram.com/petspage_com">&nbsp;</a></div></li>
                  <li><div class="social_5"><a target="_blank" href="http://www.pinterest.com/petspage/">&nbsp;</a></div></li>                 
                  <li><div class="social_7"><a target="_blank" href="mailto:tinks@petspage.com">&nbsp;</a></div></li>
                  
							</ul>
          </div>
  </div>
  </div>
</div>
<script>
	$('.modal-box').click(function(e) {
		$(this).hide();
	});
	$('.modal-box .modal-box-inner').click(function(e) {
		e.stopPropagation();
	});
	$('.login_required').click(function(e) {
		$('.modal-box').show();
		e.preventDefault();
	});
	
	function show_message_box(heading, message) {
		var modal = $('#message_box_popup');
		modal.find('#modal_heading').html(heading);
		modal.find('#modal_message').html(message);
		modal.show();
	}
	
	$('#lost_password_link').click(function(e) {
		var modal = $('#lost_password_popup');
		modal.show();
		e.preventDefault();
	});
</script>
</body>
</html>