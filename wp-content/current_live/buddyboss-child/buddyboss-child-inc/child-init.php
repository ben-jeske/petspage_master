<?php
/**
 * BuddyBoss Child Theme init functions
 *
 * @package BuddyBoss
 * @since 3.0
 */

/**
 * Include required files. For any custom functionality
 * you should create files in /buddyboss-child-inc/ and
 * include them here.
 *
 * For example:
 *
 * $includes = array(
 *   'child-theme.php',
 *   'my-functions.php',
 *   'my-functions2.php'
 * );
 *
 */

$includes = array(
  'child-theme.php'
);

$includes_dir = trailingslashit( apply_filters( 'buddyboss_child_include_folder', get_stylesheet_directory() . '/buddyboss-child-inc' ) );

$filtered_includes = apply_filters( 'buddyboss_child_includes', $includes );

foreach( $filtered_includes as $include )
{
  $include_file_path = $includes_dir . $include;

  if ( file_exists( $include_file_path ) )
  {
    require_once( $include_file_path );
  }
}


?>