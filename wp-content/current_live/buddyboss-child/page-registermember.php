<?php
/*
	Template Name: registermember
*/
if(strtolower($_SERVER['REQUEST_METHOD']) == 'post')
{
	$res = array();
	$action = "register";
	
	if(isset($_POST['action']))
	{
		$action = trim(strtolower($_POST['action']));
	}
	
	switch($action)
	{
		/****************************************************************
		 * Login Form Processing
		 */
		case 'do_login':
			$user_data['user_login'] = '';
			$user_data['user_password'] = '';
			
			if(isset($_POST['user_login']) && $_POST['user_login'] != '')
			{
				$user_data['user_login'] = esc_attr($_POST['user_login']);
			}
			
			if(isset($_POST['user_password']) && $_POST['user_password'] != '')
			{
				$user_data['user_password'] = trim($_POST['user_password']);
			}
			
			$user = wp_signon( $user_data, false );
			
			if ( is_wp_error($user) )
			{
				$res['error'] = true;
				$res['message'] = $user->get_error_message();
			}
			else
			{
				$res['error'] = false;
			}
		break;
		
		/****************************************************************
		 * Lost Password
		 */
		case 'do_reset':
			$email = trim($_POST['user_login']);
			
			if( empty( $email ) )
			{
				$res['error'] = true;
				$res['message'] = 'Enter a username or e-mail address..';
			}
			else if( ! is_email( $email ))
			{
				if( username_exists( $email ) )
				{
					// Get user data by field and data, other field are ID, slug, slug and login
					$user = get_user_by( 'login', $email );
				}
				else
				{
					$res['error'] = true;
					$res['message'] = 'Invalid username or e-mail address.';
				}
			}
			else if( ! email_exists($email) )
			{
				$res['error'] = true;
				$res['message'] = 'There is no user registered with that email address.';
			}
			else
			{
				// Get user data by field and data, other field are ID, slug, slug and login
				$user = get_user_by( 'email', $email );
			}
			
			if( !$res['error'] )
			{
				// lets generate our new password
				$random_password = wp_generate_password( 8, false );
				
				
				$update_user = wp_update_user( array (
						'ID' => $user->ID, 
						'user_pass' => $random_password
					)
				);
				
				// if  update user return true then lets send user an email containing the new password
				if( $update_user )
				{
					$to = $email;
					$subject = 'Your new password';
					$sender = get_option('name');
					$headers  = 'MIME-Version: 1.0' . "\r\n";
                                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                        // Additional headers
                                        $headers .= 'From: Petspage <tinks@petspage.com>' . "\r\n";

					$message = "Hi there,\n
	\n
	Your password was successfully changed.\n
	\n
	Your new password is: {$random_password}\n
	\n
	Please login with your username and the above password. After you login change the password immediately.\n
	\n
	Regards,\n
	PetsPage.com";
					$mail = wp_mail( $to, $subject, $message, $headers );
					if( $mail )
						$res['error'] = false;
						$res['message'] = 'Password was reset successfully. Please check your email for your new password.';
						
				}
				else
				{
					$res['error'] = true;
					$res['message'] = 'Oops something went wrong updaing your account.';
				}
			}
		break;
		
		/****************************************************************
		 * Default to Register
		 */
		default:	
			
			// Verifying reCaptcha
			$recaptcha_var['privatekey'] = '6LcahvASAAAAAJVE5gGu7z6d9ep6d1C9z8ic2F_s';
			$recaptcha_var['remoteip'] = $_SERVER['REMOTE_ADDR'];
			$recaptcha_var['challenge'] = $_POST['recaptcha_challenge'];
			$recaptcha_var['response'] = $_POST['recaptcha_response'];
			
			$rec_v = curl_init("http://www.google.com/recaptcha/api/verify");
			
			curl_setopt( $rec_v, CURLOPT_POST, 1 );
			curl_setopt( $rec_v, CURLOPT_POSTFIELDS, $recaptcha_var );
			curl_setopt( $rec_v, CURLOPT_HEADER, 0 );
			curl_setopt( $rec_v, CURLOPT_RETURNTRANSFER, 1 );
			
			$rec_reply = explode("\n", curl_exec( $rec_v ));
			curl_close( $rec_v );
			
			if( strtolower(trim($rec_reply[0])) == 'false' )
			{
				$res['error'] = true;
				$res['message'] = 'The human verfication code does not match. Please enter again. You can load another if you are having trouble solving the current one';
				break;
			}
			
			// Continue to the rest of the validation
			$email_address = $_POST['email'];
			
			if(filter_var($email_address, FILTER_VALIDATE_EMAIL))
			{
				if(!email_exists($email_address))
				{
								
					if(isset($_POST['username']) && $_POST['username'] != '')
					{
						$user_data['username'] = esc_attr($_POST['username']);
					}
		
					if(!username_exists($user_data['username']))
					{
						if(isset($_POST['displayname']) && $_POST['displayname'] != 'displayname')
						{
							$user_data['displayname'] = esc_attr($_POST['displayname']);
						}
						
						if(isset($_POST['password']) && $_POST['password'] != '')
						{
							$user_data['password'] = trim($_POST['password']);
						}
						
						$user_id = wp_create_user($user_data['username'], $user_data['password'], $email_address);
						
						wp_update_user(
							array(
								'ID' => $user_id,
								'display_name' => $user_data['displayname'],
							)
						);
						
						$url = site_url();
						$activation_message = "Hi {$user_data['displayname']}!
		
We’re so happy you joined our social pet community! If you love pets and you love socializing, you’re in the right place! Have fun connecting one on one with pet lovers from all over the world by sharing, socializing and smiling together!
\n\n
Thanks for registering!

Is your pet a Social Pet SuperStar? Create a social pet page for them or become of fan of our many SuperStars! Have fun engaging and showcasing your pet’s personality and check out our cuties!
\n\n
Have questions for other pet lovers or pet professionals? Post it in our Forum! Our community loves to help each other by sharing great information! Do you have advice to give? Share it with others in our Forum!
\n\n
If you’re a pet professional, e-mail Tinks@PetsPage.com for information on how to stand out in our community!
\n\n
If you have a pet related business - please follow the link below to upgrade to business page:
\n\n
{$url}/membership/
\n\n
Thank you so much for joining us! Please visit our FAQs for more information about how to engage with others in our community and if you’d like more information about Karen & Tinks and PetsPage.com’s mission, please visit About Us. If you would like assistance setting up your profile, social pet page, or anything else, please check out our community forum or reply to this e-mail for personalized assistance!
\n\n
Check back often as we’ll be adding even more ways for you to have fun on PetsPage.com! Great things are in store for Pet Lovers! We’re so happy you found us!
\n\n
See you on PetsPage.com!
						";
						
						wp_mail( $email_address, 'Thanks for registering! PetsPage.com', $activation_message );
						
						/*
						wp_new_user_notification( $user_id, $user_data['password'] );
						*/
						
						$user = wp_signon( array('user_login' => $user_data['username'], 'user_password' => $user_data['password'] ), false );
						if ( is_wp_error($user) )
						{
							echo $user->get_error_message();
							die();
						}
						else
						{
							$res['error'] = false;
							$res['redirect_to'] = '/pet-lovers/'.$user_data['username'].'/profile/edit/group/1/';
						}
					}
					else
					{
						$res['error'] = true;
						$res['message'] = 'Error: Username already taken. Please choose another';
					}
				}
				else
				{			
					$res['error'] = true;
					$res['message'] = 'Error: Email already registered. Please choose another';
				}
			}
			else
			{	
				$res['error'] = true;
				$res['message'] = 'Error: Invalid Email Address';
			}
	}
	
	header('Content-Type: text/json');
	echo json_encode($res);
}
else
{
	header('Location: /');
}