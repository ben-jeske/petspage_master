<?php
/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss/buddyboss-inc/theme.php
 */

/**
 * Add your own function files in /buddyboss-child-inc/.
 *
 * It's recommended to add custom functionality in that folder
 * and follow our naming convention, then loading the file in
 * child-init.php
 *
 * Naming convention: Try to semantically describe the functionality
 * Portfolio theme: /buddyboss-child-inc/portfolio.php (contains portfolio related functions)
 * Business directory theme: /buddyboss-child-inc/directory.php (contains directory related functions)
 */

$init_file = get_stylesheet_directory() . '/buddyboss-child-inc/child-init.php';

if ( ! file_exists( $init_file ) )
{
  $err_msg = __( 'BuddyBoss cannot find the child theme\'s starter file, should be located at: *wp root*/wp-content/themes/*child_theme_folder*/buddyboss-child-inc/child-init.php', 'buddyboss' );

  wp_die( $err_msg );
}

/* Add additional information to user profile's header */
add_action('bp_before_member_header_meta','bp_before_member_header_profile_fields');
function bp_before_member_header_profile_fields() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	$res2 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=2"); 
	foreach($res2 as $row)
	{
		$profile_desc = $row->value;
	}
	
	if($profile_desc != '')
	{
		echo '<h2>About</h2><p>'.$profile_desc.'</p>';
	}
}

add_action('bp_member_header_tagline','bp_member_header_get_tagline');
function bp_member_header_get_tagline() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	
	$res3 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=3"); 
	foreach($res3 as $row)
	{
		$profile_tagline = $row->value;
	}
	
	if($profile_tagline != '')
	{
		echo '<blockquote style="padding:24px 0;">&#8212; '.$profile_tagline.'</blockquote>';
	}
}



/* Add fields to registration form */

add_action('bp_after_signup_profile_fields', 'bp_after_signup_profile_fields_add_fields');

function bp_after_signup_profile_fields_add_fields() {
	$fields = '
	<div class="register-section" id="basic-details-section">
    <h4>Account Details</h4>
    <label for="signup_username">Username (required)</label>
                    <input type="text" name="signup_username" id="signup_username" value="">
    <label for="signup_email">Email Address (required)</label>
                    <input type="text" name="signup_email" id="signup_email" value="">
    <label for="signup_password">Choose a Password (required)</label>
                    <input type="password" name="signup_password" id="signup_password" value="">
    <label for="signup_password_confirm">Confirm Password (required)</label>
                    <input type="password" name="signup_password_confirm" id="signup_password_confirm" value="">
</div>';
}

require_once( $init_file );

add_role('Ambassador', 'Ambassador', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
    'delete_posts' => false, // Use false to explicitly deny
));

/*add_action( 'admin_init', 'redirect_non_admin_users' );

function redirect_non_admin_users() {
	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
		wp_redirect( home_url() );
		exit;
	}
}*/

?>