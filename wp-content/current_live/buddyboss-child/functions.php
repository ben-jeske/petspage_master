<?php
/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss/buddyboss-inc/theme.php
 */

/**
 * Add your own function files in /buddyboss-child-inc/.
 *
 * It's recommended to add custom functionality in that folder
 * and follow our naming convention, then loading the file in
 * child-init.php
 *
 * Naming convention: Try to semantically describe the functionality
 * Portfolio theme: /buddyboss-child-inc/portfolio.php (contains portfolio related functions)
 * Business directory theme: /buddyboss-child-inc/directory.php (contains directory related functions)
 */

$init_file = get_stylesheet_directory() . '/buddyboss-child-inc/child-init.php';

if ( ! file_exists( $init_file ) )
{
  $err_msg = __( 'BuddyBoss cannot find the child theme\'s starter file, should be located at: *wp root*/wp-content/themes/*child_theme_folder*/buddyboss-child-inc/child-init.php', 'buddyboss' );

  wp_die( $err_msg );
}

/* Add additional information to user profile's header */
add_action('bp_before_member_header_meta','bp_before_member_header_profile_fields');
function bp_before_member_header_profile_fields() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	$res2 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=2"); 
	foreach($res2 as $row)
	{
		$profile_desc = $row->value;
	}
	
	if($profile_desc != '')
	{
		echo '<h2>About</h2><p>'.$profile_desc.'</p>';
	}
}

add_action('bp_member_header_tagline','bp_member_header_get_tagline');
function bp_member_header_get_tagline() {
	global $current_user;
	get_currentuserinfo();
	$curr_id = bp_displayed_user_id();
	// Get profile data for description and tagline
	global $wpdb;		
	
	$res3 = $wpdb->get_results( "SELECT value FROM wp_bp_xprofile_data WHERE user_id=$curr_id AND field_id=3"); 
	foreach($res3 as $row)
	{
		$profile_tagline = $row->value;
	}
	
	if($profile_tagline != '')
	{
		echo '<blockquote style="padding:24px 0;">&#8212; '.$profile_tagline.'</blockquote>';
	}
}



/* Add fields to registration form */

add_action('bp_after_signup_profile_fields', 'bp_after_signup_profile_fields_add_fields');

function bp_after_signup_profile_fields_add_fields() {
	$fields = '
	<div class="register-section" id="basic-details-section">
    <h4>Account Details</h4>
    <label for="signup_username">Username (required)</label>
                    <input type="text" name="signup_username" id="signup_username" value="">
    <label for="signup_email">Email Address (required)</label>
                    <input type="text" name="signup_email" id="signup_email" value="">
    <label for="signup_password">Choose a Password (required)</label>
                    <input type="password" name="signup_password" id="signup_password" value="">
    <label for="signup_password_confirm">Confirm Password (required)</label>
                    <input type="password" name="signup_password_confirm" id="signup_password_confirm" value="">
</div>';
}

require_once( $init_file );

add_role('Ambassador', 'Ambassador', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
    'delete_posts' => false, // Use false to explicitly deny
));

/*add_action( 'admin_init', 'redirect_non_admin_users' );

function redirect_non_admin_users() {
	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
		wp_redirect( home_url() );
		exit;
	}
}*/

//
//  Re-direct not-logged-in users to holding page
//
if(!is_user_logged_in() && (curPageURL() == 'https://petspage.com/')) {
    wp_redirect( 'https://petspage.com/member_register/', 302 );
    exit;
}

/**
 * Users Rank Progress
 * @version 1.0
 */
function mycred_pro_users_rank_progress( $bp_displayed_user_id ) {
        // Load myCRED
        $mycred = mycred_get_settings();
 
        // Ranks are based on a total
        if ( $mycred->rank['base'] == 'total' )
                $key = $mycred->get_cred_id() . '_total';
        // Ranks are based on current balance
        else
                $key = $mycred->get_cred_id();
 
        // Get Balance
        $users_balance = $mycred->get_users_cred( $bp_displayed_user_id, $key );
       
        // Rank Progress
       
        // Get the users current rank post ID
        $users_rank = (int) mycred_get_users_rank( $bp_displayed_user_id, 'ID' );
       
        // Get the ranks set max
        $max = get_post_meta( $users_rank, 'mycred_rank_max', true );
       
// Calculate progress. We need a percentage with 1 decimal
if ( $users_balance > 0 )
   $progress = number_format( ( ( $users_balance / $max ) * 100 ), 1 );
else
   $progress = 0;
       
        // Visual Composer customization
        $options = '';
       
        // If progress is less then 100%, use the stipped animation to indicate progress
        if ( $progress < number_format( 100, 1 ) )
                $options = 'options="striped,animated"';
        // Else use the fixed background color to indicate completion
        else
                $progress = number_format( 100, 1 );
?>
        <?php echo do_shortcode( '[vc_row][vc_column width="1/1"][vc_progress_bar values="' . $progress . '|' . get_the_title( ++$users_rank ) . ' Progress -' . '" bgcolor="custom" ' . $options . ' custombgcolor="#563b73" title="" units="%"]<p><span class="description"></p>[/vc_column][/vc_row]' );

}

//
//  Get current page URL
//
function curPageURL() {

    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

    $pageURL .= "://";
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    /*
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }*/
    //echo  $pageURL ;
    return $pageURL;
}


if ( function_exists( 'mycred_label' ) ) :
        /**
         * Step 1: Register Widget
         * @version 1.0
         */
        add_action( 'widgets_init', 'mycred_register_custom_widgets' );
        function mycred_register_custom_widgets()
        {
            register_widget( 'myCRED_Widget_This_Weeks_Leaderboard' );
        }
 
        /**
         * Step 2: Construct Widget
         * @version 1.2
         */
        if ( ! class_exists( 'myCRED_Widget_This_Weeks_Leaderboard' ) ) {
                class myCRED_Widget_This_Weeks_Leaderboard extends WP_Widget {
 
                        // Constructor
                        public function __construct() {
                                $name = mycred_label();
                                // Basic details about our widget
                                $widget_ops = array(
                                        'classname'   => 'widget-mycred-this-weeks-leaderboard',
                                        'description' => __( 'Show the leaderboard for a given timeframe.', 'mycred' )
                        );
                                $this->WP_Widget( 'mycred_widget_this_weeks_leaderboard', __( 'This weeks Leaderboard', 'mycred' ), $widget_ops );
                                $this->alt_option_name = 'mycred_widget_this_weeks_leaderboard';
                        }
 
                        // Widget Output (what users see)
                        public function widget( $args, $instance ) {
                                extract( $args, EXTR_SKIP );
 
                                // Check if we want to show this to visitors
                                if ( ! $instance['show_visitors'] && ! is_user_logged_in() ) return;
                       
                                // Get the leaderboard
                                $leaderboard = $this->get_leaderboard( $instance['number'], $widget_id );
                       
                                // Load myCRED
                                $mycred = mycred_get_settings();
 
                                // Start constructing Widget
                                echo $before_widget;
                       
                                // Title (if not empty)
                                if ( ! empty( $instance['title'] ) ) {
                                        echo $before_title;
                                        // Allow general tempalte tags in the title
                                        echo $mycred->template_tags_general( $instance['title'] );
                                        echo $after_title;
                                }
 
                                // Construct unorganized list for each row
                                echo '<ul class="mycred-this-weeks-leaderboard">';
                                foreach ( $leaderboard as $position => $data ) {
                                        echo '<li class="dt">#<span>' . ++$kk . ' ' . $data->display_name . '</span></li> <li class="dd"> ' . $mycred->format_creds( $data->total ) . '      </li>';
                                }
                                echo '</ul>';
 
                                echo $after_widget;
                        }
 
                        // Widget Settings (when editing / setting up widget)
                        public function form( $instance ) {
                                $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'This Weeks Leaderboard', 'mycred' );
                                $number = isset( $instance['number'] ) ? abs( $instance['number'] ) : 5;
                                $show_visitors = isset( $instance['show_visitors'] ) ? 1 : 0; ?>
 
<p class="myCRED-widget-field">
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'mycred' ); ?>:</label>
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p class="myCRED-widget-field">
        <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of users', 'mycred' ); ?>:</label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo $number; ?>" size="3" class="align-right" />
</p>
<p class="myCRED-widget-field">
        <input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'show_visitors' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'show_visitors' ) ); ?>" value="1"<?php checked( $show_visitors, 1 ); ?> class="checkbox" />
        <label for="<?php echo esc_attr( $this->get_field_id( 'show_visitors' ) ); ?>"><?php _e( 'Visible to non-members', 'mycred' ); ?></label>
</p>
<?php if ( isset( $this->id ) && $this->id !== false ) : ?>
<p class="myCRED-widget-field">
        <input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'reset_leaderboard' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'reset_leaderboard' ) ); ?>" value="1" class="checkbox" />
        <label for="<?php echo esc_attr( $this->get_field_id( 'reset_leaderboard' ) ); ?>"><?php _e( 'Reset Leaderboard', 'mycred' ); ?></label>
</p>
<?php
                                endif;
                        }
 
                        // Save Widget Settings
                        public function update( $new_instance, $old_instance ) {
                                $instance = $old_instance;
                                $instance['number'] = (int) $new_instance['number'];
                                $instance['title'] = trim( $new_instance['title'] );
                                $instance['show_visitors'] = $new_instance['show_visitors'];
                       
                                if ( isset( $new_instance['reset_leaderboard'] ) && $this->id !== false ) {
                                        delete_transient( 'mycred_twl_' . $this->id );
                                }
 
                                mycred_flush_widget_cache( 'mycred_widget_this_weeks_leaderboard' );
                                return $instance;
                        }
 
                        // Grabs the leaderboard
                        public function get_leaderboard( $number = 5, $widget_id = '' ) {
                                // Get transient
                                $data = get_transient( 'mycred_twl_' . $widget_id );
                                $this_week = date_i18n( 'W' );
                       
                                // If a transient is set, check if it is time for an update
                                if ( $data !== false ) {
                                        reset( $data );
                                        $saved_week = key( $data );
                                        // Compare the first array key (which holds the week number) to this weeks
                                        // If they do not match, it is a new week and we need to make a new query.
                                        // Same goes for empty results (new sites)
                                        if ( $this_week != $saved_week || empty( $data[ $saved_week ] ) )
                                                $data = false;
                                        // Else return the results
                                        else
                                                $data = $data[ $this_week ];
                                }
 
                                // If transient does not exist or a new number is set, do a new DB Query       
                                if ( $data === false || $number != 5 ) {
                                        // Start over
                                        $data = array();
                               
                                        // Load the wpdb class
                                        global $wpdb;
                               
                                        $mycred = $wpdb->prefix . 'myCRED_log';
                                        $SQL = "
SELECT m.user_id, u.display_name, sum( m.creds ) AS total
FROM {$mycred} m
LEFT JOIN {$wpdb->users} u
        ON u.ID = m.user_id
WHERE ( m.time >= %d AND m.time <= %d AND m.creds > 0 )
GROUP BY m.user_id
ORDER BY total DESC LIMIT 0,%d;";
                               
                                        // Save results under this week
                                        $now = date_i18n( 'U' );
                                        $last_week = strtotime( "-1 week" );
                                        $data[ $this_week ] = $wpdb->get_results( $wpdb->prepare( $SQL, $last_week, $now, $number ) );
                                        // Save results for a week
                                        set_transient( 'mycred_twl_' . $widget_id, $data, WEEK_IN_SECONDS );
                                        $data = $data[ $this_week ];
                                }
                       
                                return $data;
                        }
                }
        }
endif;
?>