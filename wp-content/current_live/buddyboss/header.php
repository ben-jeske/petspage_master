<?php
/**
 * The Header for your theme.
 *
 * Displays all of the <head> section and everything up until <div id="main">
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */
?>
<?php 
ob_start();
error_reporting(0);


if(isset($_POST['profile-group-submit']))
{
	global $current_user;
	get_currentuserinfo();
	$curr_id = $current_user->ID;
	
	$upgrade = false;
	foreach($current_user -> roles as $role)
	{
		if(strtolower($role) == 'administrator' or strtolower($role) == 'upgrade' or strtolower($role) == 'ambassador')
		{
			$upgrade = true;
		}
	}
	
	$curr_id = $current_user->ID;
	$curr_username = $current_user->user_login;
	$user_id = $curr_id;
	$do_update = false;
	// Save BuddyPress Display name
	if(isset($_POST['public_value']))
	{
		if($upgrade)
		{
			if($_POST['public_value'] == 'profile')
			{
				if(isset($_POST['display_name']))
				{
					$display = trim(strip_tags($_POST['display_name']));
					$do_update = true;
				}
			}
			else if($_POST['public_value'] == 'business')
			{
				if(isset($_POST['business']))
				{
					$display = trim(strip_tags($_POST['business']));
					$do_update = true;
				}
			}
		}
		else
		{
			if(isset($_POST['display_name']))
			{
				$display = trim(strip_tags($_POST['display_name']));
				$do_update = true;
			}
		}	
	}
	elseif(isset($_POST['display_name']))
	{
		$display = trim(strip_tags($_POST['display_name']));
		$do_update = true;
	}
	
	if($do_update)
	{
		$table1 = "wp_bp_xprofile_data";
		$data_array1 = array('value' => $display);
		$where1 = array('user_id' => $user_id, 'field_id' => 1);
		$wpdb->update( $table1, $data_array1, $where1 );
	}
	
	// Save Description Field
	if(isset($_POST['profile_desc']))
	{
		// Check if field exits
		
		$profile_desc = trim(stripslashes($_POST['profile_desc']));
		$table1 = "wp_bp_xprofile_data";
		
		$data_array_desc = array('value' => $profile_desc);
		$where_desc = array('user_id' => $user_id, 'field_id' => 2);
		
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 2', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$wpdb->insert( $table1, array_merge($data_array_desc, $where_desc) );
		}
	}
	
	// Save Tagline Field
	if(isset($_POST['profile_tagline']))
	{
		$profile_tagline = trim(stripslashes($_POST['profile_tagline']));
		$table1 = "wp_bp_xprofile_data";
		$data_array_desc = array('value' => $profile_tagline);
		$where_desc = array('user_id' => $user_id, 'field_id' => 3);
		
		$data = array();
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 3', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$wpdb->insert( $table1, array_merge($data_array_desc, $where_desc) );
		}
	}
	
	// Save Zipcode Field
	if(isset($_POST['profile_zipcode']))
	{
		$profile_value = trim(stripslashes($_POST['profile_zipcode']));
		$table1 = "wp_bp_xprofile_data";
		$data_array_desc = array('value' => $profile_value);
		$where_desc = array('user_id' => $user_id, 'field_id' => 4);
		
		$data = array();
		$data = $wpdb -> get_results('select* from wp_bp_xprofile_data where user_id = '.$user_id.' AND field_id = 4', ARRAY_A);
		
		if(count($data) > 0)
		{
			$wpdb->update( $table1, $data_array_desc, $where_desc );
		}
		else
		{
			$data_array_insert['user_id'] = $user_id;
			$data_array_insert['field_id'] = '4';
			$data_array_insert['value'] = $profile_value;
			
			$wpdb->insert( $table1, $data_array_insert, array('%d', '%d', '%s') );
		}
	}
	
	// Save Display Name and Business Names to the WP_USER
	if(isset($_POST['display_name']))
	{
		$wp_display_name = trim(strip_tags($_POST['display_name']));
		//update_user_meta($user_id, 'display_name', $wp_display_name);
		$table2 = "wp_users";
		$data_array2 = array('display_name' => $wp_display_name);
		$where2 = array('ID' => $user_id);
		$wpdb->update( $table2, $data_array2, $where2 );
	}
	
	if($upgrade && isset($_POST['business']))
	{
		$wp_business_name = trim(strip_tags($_POST['business']));
		$table3 = "wp_users";
		$data_array3 = array('business' => $wp_business_name);
		$where3 = array('ID' => $user_id);
		$wpdb->update( $table3, $data_array3, $where3 );
	}
	// Redirect
  	$path = esc_url( home_url('/pet-lovers/') ).$curr_username.'/profile/edit/group';
  	header("Location: $path");
}

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE > 8]>
<html class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if ! IE  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

  <style>
    	.main-navigation li a {
		border-bottom: 0;
		color: #fff;
		font-weight: bold;
		line-height: 1;
		padding: 14px 17px !important;
		font-size: 14px;
		white-space: nowrap;
		word-wrap: break-word;
	}
    

.modal-box { display:none;position:fixed; z-index:100000; background-color:rgba(0,0,0,0.7); top:0; left:0; width:100%; height:100%; }
.modal-box .modal-box-inner { background-color:#fff; border:solid 5px #300948; border-radius:10px; padding:20px; margin:10% auto; width:300px;}
.modal-box form { margin: 20px 0; }
.modal-box form label { display:block; margin:5px 0;}
</style>
  <style>
    	.main-navigation li a {
		border-bottom: 0;
		color: #fff;
		font-weight: bold;
		line-height: 1;
		padding: 14px 17px !important;
		font-size: 14px;
		white-space: nowrap;
		word-wrap: break-word;
	}

.modal-box { display:none;position:fixed; z-index:100000; background-color:rgba(0,0,0,0.7); top:0; left:0; width:100%; height:100%; }
.modal-box .modal-box-inner { background-color:#fff; border:solid 5px #300948; border-radius:10px; padding:20px; margin:10% auto; width:300px;}
.modal-box form { margin: 20px 0; }
.modal-box form label { display:block; margin:5px 0;}
#ajaxsearchpro1 {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
    border: 0 none #000000;
    border-radius: 0 !important;
    box-shadow: 0 0 0 0 #D2DBD9 !important;
    height: auto !important;
    overflow: hidden;
    width: 100%;
}
#ajaxsearchpro1 .probox .proinput {
    box-shadow: none;
    color: #212121;
    float: left;
    font-family: 'Arial',Helvetica,sans-serif;
    font-size: 12px;
    font-weight: normal;
    height: 100%;
    line-height: 15px;
    margin: 0 0 0 10px !important;
    padding:3px 0 !important;
    position: relative;
    width: 70% !important;
}
#ajaxsearchpro1 .probox .proinput input{font-size:15px !important;}
#ajaxsearchpro1 .probox {
    background: none repeat scroll 0 0 #FFFFFF;
    border: medium none !important;
    border-radius: 0 !important;
    box-shadow: 0 0 0 0 #CCCCCC inset !important;
    height: 30px;
    margin: 4px;
    overflow: hidden;
    width: auto;
}


</style> 
  
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-2.ico" type="image/x-icon">
<!-- BuddyPress and bbPress Stylesheets are called in wp_head, if plugins are activated -->
<?php wp_head(); ?>
</head>

<body <?php if ( current_user_can('manage_options') ) : ?>id="role-admin"<?php endif; ?> <?php body_class(); ?>>


<div class="main_top" style=" margin:0px; padding:0px; width:100%; background:#300948; float:left; position:fixed; z-index:9999999;">
    <div class="main_top_inner" style="width:1032px; margin:0px auto;">
        <?php if (is_user_logged_in()){ ?>
            <div class="ajax_main" style="width:45%; float:left; margin:0px; padding:0px;" >
                <div class="main_area_ajax" style="width:100%; float:left; margin:0px;" >
                    <div class="ajax_left" style="width:auto; float:left; margin:7px 10px 0px 0px">
                        <a href="http://petspage.com"><img src="<?php bloginfo('template_url') ?>/images/pp.png" /></a>
                    </div>
                  <!--
                    <div class="ajax_right" style="width:90%; float:left; margin:0px; padding:0px;">
                         <?php  echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?>
                    </div>
										!-->
                </div>
            </div>
        <?php } ?> 
         
        <?php do_action( 'buddyboss_before_header' ); ?>
     </div>
 </div>

<header id="masthead" class="site-header" role="banner">

	<div class="header-inner">

		<?php if (get_option("buddyboss_custom_logo", FALSE)!= FALSE) {
				$logo = get_option("buddyboss_custom_logo");
			} else {
				$logo = get_bloginfo("template_directory")."/images/logo.jpg";
			} ?>

		<div id="logo" class="custom-logo">
				<a href="<?php echo site_url() ?>" title="<?php _e( 'Home', 'buddypress' )?>"><img src="<?php echo $logo ?>"/></a>
		</div>
        
        <div class="custom-tagline">A Place for Social Pet Lovers!</div>

	</div>

	<nav id="site-navigation" class="main-navigation" role="navigation">
		<div class="nav-inner">

			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'nav-menu clearfix' ) ); ?>
		</div>
	</nav><!-- #site-navigation -->
</header><!-- #masthead -->

<?php do_action( 'buddyboss_after_header' ); ?>

<div id="main-wrap"> <!-- Wrap for Mobile -->
    <div id="mobile-header">
    	<div class="mobile-header-inner">
        	<div class="left-btn">
				<?php if (is_user_logged_in()){ ?>
                    <a href="#" id="user-nav" class="closed "></a>
                <?php }
				else if (!is_user_logged_in() && buddyboss_is_bp_active() && !bp_hide_loggedout_adminbar( false )){?>
                    <a href="#" id="user-nav" class="closed "></a>
                <?php } ?>
            </div>

            <div class="mobile-header-content">
            	<a class="mobile-site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
            </div>

            <div class="right-btn">
                <a href="#" id="main-nav" class="closed"></a>
            </div>
        </div>
    </div><!-- #mobile-header -->

    <div id="inner-wrap"> <!-- Inner Wrap for Mobile -->
    	<?php do_action( 'buddyboss_inside_wrapper' ); ?>
        <div id="page" class="hfeed site">
			<div id="swipe-area">
            </div>
            <div id="main" class="wrapper">
