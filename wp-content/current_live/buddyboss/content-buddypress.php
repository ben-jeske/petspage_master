<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */
?>

<?php
/**
 * Get page title & content
 */
global $bp;

$custom_title = $custom_content = false;

$bp_title = get_the_title();

if ( bp_is_directory() )
{
  foreach ( (array) $bp->pages as $page_key => $bp_page )
  {
  	if ( bp_is_page( $page_key ) )
  	{
  		$page_id = $bp_page->id;

  		$page_query = new WP_query( array(
  			'post_type' => 'page',
  			'page_id' => $page_id
  		) );

  		while( $page_query->have_posts() )
  		{
  			$page_query->the_post();

				$custom_title = get_the_title();
				$custom_content = wpautop( get_the_content() );
			}

			wp_reset_postdata();
    }
  }

  // If we have a custom title and need to grab a BP title button
  if ( $custom_title != false && strpos( $bp_title, '&nbsp;<a' ) != false )
  {
		$bp_title_parts = explode( '&nbsp;<a', $bp_title, 2 );

		$custom_title .= '&nbsp;<a' . $bp_title_parts[1];
  }
}

// Fall back to BP generated title if we didn't grab a custom one above
if ( ! $custom_title )
	$custom_title = $bp_title;
?>

		<header class="entry-header"><?php
					if(bp_displayed_user_id() > 0)
					{
						?> 

      <?php 
      $user_info = get_userdata(bp_displayed_user_id());
      $user_roles = $user_info->roles;
      foreach($user_roles as $key => $value)
      {
            if($value == "Ambassador")
            {?>
<h1 class="entry-title" style="float:left;" id="Ambassador_title">
				<?php echo $custom_title; ?>
			</h1>            <?php
break; 
}
else
if($value == "Upgrade")
            {?>
<h1 class="entry-title" style="float:left;" id="upgrade_profile">
				<?php echo $custom_title; ?>
			</h1>            <?php
break; 
}
else{
?>
<h1 class="entry-title" style="float:left;">
				<?php echo $custom_title; ?>
</h1>
<?php
break;
}
      }
?>
			<!-- <h1 class="entry-title" style="float:left;">
				<?php echo $custom_title; ?>
			</h1> -->
<?php 
      $user_info = get_userdata(bp_displayed_user_id());
      $user_roles = $user_info->roles;
      foreach($user_roles as $key => $value)
      {
            if($value == "Ambassador")
            {?>
<p style="float: left; padding: 0px; color: rgb(59, 21, 85); font-family: Arial,Helvetica,sans-serif; font-weight: bold; font-size: 17px; margin-top: 10px;"><img src="<?php bloginfo('template_url') ?>/images/pp1.png" alt="" style="float: left; width: auto; margin: -4px 4px 4px 7px;" />Ambassador</p>
<div style="clear:both"></div>
            <?php }
      }
?>

<div style="clear:both;"></div>
                        <?php
					//	do_action( 'bp_member_header_tagline' );
					}
					else {
				?>
            	<?php
					}
				?>
		</header>

		<div class="entry-content">
			<?php if ( $custom_content ) echo $custom_content; ?>

			<?php the_content(); ?>
		</div><!-- .entry-content -->

		<footer class="entry-meta">
			<?php edit_post_link( __( 'Edit', 'buddyboss' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->

