<?php
/**
// Template name: Member
 */
 if ( is_user_logged_in() ) 
{
	add_filter('add_to_cart_text', 'woo_custom_cart_button_text');
	function woo_custom_cart_button_text() 
	{
						return __('Upgrade', 'woocommerce');
	}
}

if ( !is_user_logged_in() ) 
{
	add_filter('add_to_cart_text', 'woo_custom_cart_button_text');
	function woo_custom_cart_button_text() 
	{
						return __('Sign Up', 'woocommerce');
	}
}

get_header(); ?>
<link href="<?php bloginfo('template_url') ?>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url') ?>/font/style.css" rel="stylesheet" type="text/css" />
<style>
  .tooltips p a {
    color: black;
  }
  .tooltips p a:hover {
    color: black;
    text-decoration: none;
   }
</style>
<div id="rav_container">
  <div id="rav_wrapper">
    <div class="rav_section">
      <h2 style="text-align: center;">Welcome Pet Business Professionals!</h2>
      <h2>Upgrade to a Business Membership to socialize with Pet Lovers from around the world representing your business! You'll enjoy all the great benefits of upgrading listed below!</h2>
      <div class="rav_heading">
      <div class="rav_heading_inner">
      <h3>Professional Membership </h3>
      </div>
      
        
      <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
                          <div class="rav_heading_area" id="cat<?php echo get_the_ID(); ?>">
                            <h3><a href="<?php echo get_permalink($loop->post->ID) ?>" class="tooltips" title="<?php echo get_post_meta( get_the_ID(), 'hover', true ); ?>"><?php the_title(); ?></a></h3>
                            
                          </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>      
                      
      </div>
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p></p>
      </div>
      
      <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
                  <div class="rav_inner_area" id="rav_price<?php echo get_the_ID(); ?>">
                  <p><?php echo $product->get_price_html(); ?></p>
                  </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>
      

            
      </div>
      
      <div class="rav_heading">
      <div class="rav_heading_inner" id="vis_area">
      <h3>Benefits</h3>
      </div>
      <div class="rav_heading_area" id="cat1">
      <h3 id="rav_vis"></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      </div>
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Stand out in Feeds with a Professional Title relevant to your business.">Professional Member Title</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>

      
      </div>
      
       <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Display your Business name, information and more to potential customers.">Upgraded Business Page</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>

      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Display and Post Updates as your Business in all Feeds.">Post Updates as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price1">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_p">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_s">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>

      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Contact potential customers by sending them Friend Requests.">Friend Requests as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Send Private Messages as your Business and reach out to your customers.">Messages as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Post on our Forums representing your Business. Post offers and information about your product to potential customers.">Forum Posts as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Stand out on the site with a larger display name in all site Feeds.">Larger Display Name in Feeds</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      </div>
      

      
      
       
      </div>
 <!--     <div class="rav_inner">
      <div class="rav_inner_left">
      <p>Offers</p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      </div>-->
    <?php 
$flag="";
$user_id = get_current_user_id();
      $user_info = get_userdata($user_id);
      $user_roles = $user_info->roles;
      foreach($user_roles as $key => $value)
      {
        $value;
      } ?>

      <div class="rav_inner_san">
      <div class="rav_inner_san_left">
      <p>&nbsp;</p>
      </div>
              <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
        <div class="rav_inner_san_area" id="rav_price_<?php echo get_the_ID(); ?>">
        		<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
        </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>
     </div>
      
    </div>
    
  </div>
</div>
    <h3 style="text-align: center; margin-top: 25px; color:#6F5092 !important;">As a Professional Member, you’ll also be eligible for our exclusive Social Booster and Social Growth Advertising Services.</h3>

		<h3 style="text-align: center; margin-top: 25px; color:#6F5092 !important;" >Please <a style="color: black;" href="mailto:karen@petspage.com">send an e-mail to us</a> or call <a style="color: black;" href="callto:2085144757">(208) 514-4757</a> to find out more!</h3>
<?php get_footer(); ?>