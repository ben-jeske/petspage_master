<?php
/**
 * To view theme functions, navigate to /buddyboss-inc/theme.php
 *
 * @package BuddyBoss
 * @since BuddyBoss 3.0
 */

$init_file = get_template_directory() . '/buddyboss-inc/init.php';

if ( ! file_exists( $init_file ) )
{
	$err_msg = __( 'BuddyBoss cannot find the starter file, should be located at: *wp root*/wp-content/themes/buddyboss/buddyboss-inc/init.php', 'buddyboss' );

	wp_die( $err_msg );
}

require_once( $init_file );


add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );
function custom_pre_get_posts_query( $q )
{

if (!$q->is_main_query() || !is_shop()) return;
if ( ! is_admin() )
{
$q->set( 'tax_query', array(array(
'taxonomy' => 'product_cat',
'field' => 'id',
'terms' => array( 171 ),
'operator' => 'NOT IN'
)));

}
}



require_once( $init_file );

add_role('Ambassador', 'Ambassador', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
    'delete_posts' => false, // Use false to explicitly deny
));

add_role('Upgrade', 'Upgrade', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
    'delete_posts' => false, // Use false to explicitly deny
));
?>