<?php

/* ADMIN OPTIONS */

/**
 * Add BuddyBoss to the Appearance menu
 */
function buddyboss_admin_menu()
{
	global $wp_admin_bar;

	add_theme_page( 'BuddyBoss Settings', 'BuddyBoss', 'manage_options', 'buddyboss_settings_menu', 'buddyboss_general_settings' );
}
add_action('admin_menu', 'buddyboss_admin_menu');

/**
 * General Settings Page
 */
function buddyboss_general_settings()
{
	global $buddyboss;

	$error = false;
	$error_messages = array();

	$updated = false;
	$updated_messages = array();

	// the 'pb' token defines if we're posting a new picture
	// todo: create a better admin panel
	// todo: create more semantic post tokens
	// todo: hook this into the Wordpress media library
	if ( !empty($_POST['pb']) )
	{
		// Let's get the newly uploaded file information
		$file = $_FILES['logo'];

		// Check if we have an upload
		if ( !empty($file) && $file['error'] == 4)
		{
			// We're missing crucial file informaiton, return an error
			$error_messages[] = "Please specify the logo file to upload.";
			$error = true;
		}

		// We have no errors, proceed with upload
		else if ( !empty($file) && $file['error'] == 0)
		{
			// This is our status flag that will stop processing at various
			// statges if an error is detected
			$upload_success =  TRUE;

			// Define acceptable MIME types
			$mime = array
			(
				'image/gif' => 'gif',
				'image/jpeg' => 'jpeg',
				'image/png' => 'png',
				'application/x-shockwave-flash' => 'swf',
				'image/psd' => 'psd',
				'image/bmp' => 'bmp',
				'image/tiff' => 'tiff',
				'image/tiff' => 'tiff',
				'image/jp2' => 'jp2',
				'image/iff' => 'iff',
				'image/vnd.wap.wbmp' => 'bmp',
				'image/xbm' => 'xbm',
				'image/vnd.microsoft.icon' => 'ico'
			);

			// Extract variables needed from $_FILE array
			$size	= $file['size'] / 1024; // in KB
			$type	= $file['type'];
			$name	= $file['name'];
			$tmp	= $file['tmp_name'];

			// Calculate extension and image size
			$ext	= strtolower(substr(strrchr($name, "."), 1));
			$img_info = getimagesize($tmp);

			// Maximum file size is 150kb
			// todo: create a variable and maybe admin option for this
			if ($size > 150)
			{
				$error_messages[] = "File size must be no more than 150 KB";
				$error = true;
				$upload_success = FALSE;
			}

			// Check if it's an image
			if ( empty( $img_info ) )
			{
				$error_messages[] = "Make sure the image type is JPG, GIF, or PNG.";
				$error = true;
				$upload_success = FALSE;
			}

		}

		// There was an unknown error, let's inform the user
		else {
			$error_messages[] = "There has been an error uploading the logo. Try again";
			$error = true;
			$upload_success = FALSE;
		}

		// If the status of $upload_success is TRUE we've verified the upload's success
		if ($upload_success)
		{
			// Prepare file name
			$file_mime = $img_info['mime'];

			// Set a proper extension
			if( $ext == 'jpc' || $ext == 'jpx' || $ext == 'jb2' )
			{
				$extension = $ext;
			}
			else {
				$extension = ( $mime[$file_mime] == 'jpeg' ) ? 'jpg' : $mime[$file_mime];
			}

			if( !$extension )
			{
				$extension = '';
				$name = str_replace('.', '', $name);
			}

			// Parse the file name and force a proper extension
			$file_name = strtolower($name);
			$file_name = str_replace(' ', '-', $file_name);
			$file_name = substr($file_name, 0, -strlen($ext));
			$file_name .= $extension;

			// Get the uploads directory and proper path
			$upload_dir = wp_upload_dir();
			$path = $upload_dir['path'];
			$rel  = $upload_dir['url'].'/'.$file_name;

			// Attempt to move the uploaded file and save to it's new name
			$upload_success = move_uploaded_file(realpath($tmp), $path."/".$file_name);

			// If everything went OK the upload was a success and the new logo's location
			// will be save to the WP options table.
			// todo: create an image cropper and use the WP media library functionality
			// todo: allow the user to select an image from the WP media library as their logo
			if ( $upload_success )
			{
				$updated_messages[] = "Your logo has been uploaded";
				$updated = true;
				update_option("buddyboss_custom_logo", $rel);
				update_option("buddyboss_custom_logo_file", $path."/".$file_name);
			}

			// If the upload failed to be moved let's spit out an error and some general advice
			else {
				$error_messages[] = "Upload error: Make sure your wp-content/uploads folder is writable (<a href='http://codex.wordpress.org/Changing_File_Permissions' target='_blank'>755 or 777</a>)";
				$error = true;
			}
		}
	}

	// Delete the logo
	if (isset($_POST['del']))
	{
		$file = get_option('buddyboss_custom_logo_file');

		// Attempt to delete the current file:
		$status = unlink($file);

		// Delete the options from WP options table
		delete_option('buddyboss_custom_logo');
		delete_option('buddyboss_custom_logo_file');

		// Return success/errors
		$updated = true;
		$updated_messages[] = $status ? "Reverted to no logo." : "We couldn't delete your current logo at $file.";
	}

	// Let's check the state we should enable
	if (isset($_POST['adminbar']))
	{
		// 0 = floating
		// 1 = fixed
		$state = intval( $_POST["adminbar"] );

		update_option( 'buddyboss_adminbar', $state );
		$state_lbl = ($state == 0) ? 'floating' : 'fixed';
		$updated_messages[] = "Admin Bar is set to <strong>$state_lbl</strong>.";
		$updated = true;
	}

	// Check to see if we're activating the wall component.
	$wall_activate_log = '';

	// Let's check the state we should enable
	if (isset($_POST['wall']))
	{
		// 0 = false = not enabled
		// 1 = false - enabled
		$state  = ( intval($_POST["wall"]) === 1 );

		// Based on the state posted and the current state, return error
		// or success messages and perform required actions

		if ( $state === TRUE && !function_exists( 'bp_head' ) )
		{
			update_option( 'buddyboss_wall_on', 0 );
			$error_messages[] = "Wall cannot be turned on without BuddyPress activated.";
			$error = true;
		}
		elseif ( $state === TRUE && ( !bp_is_active( 'friends' ) || !bp_is_active( 'activity' ) ) )
		{
			update_option( 'buddyboss_wall_on', 0 );
			$error_messages[] = "Wall cannot be turned on without activity and the friend's component activated.";
			$error = true;
		}
		else {
			update_option( 'buddyboss_wall_on', $state );
			$state_lbl = ($state === TRUE)? "enabled": "disabled";
			$updated_messages[] = "User Wall Posting is <strong>$state_lbl</strong>.";
			$updated = true;

			if ($state == 1) $wall_activate_log = buddyboss_wall_on_activate();
			if ($state == 0) $wall_activate_log = buddyboss_wall_on_deactivate();

		}
	}

	// Check to see if we're activating the picture component.
	$pics_activate_log = '';

	// Let's check the state we should enable
	if (isset($_POST['pics']))
	{
		// 0 = false = not enabled
		// 1 = false - enabled
		$state = ( intval($_POST["pics"]) === 1 );

		// Based on the state posted and the current state, return error
		// or success messages and perform required actions

		if ( $state === TRUE && !function_exists( 'bp_head' ) )
		{
			update_option( 'buddyboss_pics_on', 0 );
			$error_messages[] = "Photo Uploading cannot be turned on without BuddyPress activated.";
			$error = true;
		}
		elseif ( $state === TRUE && ( !bp_is_active( 'activity' ) ) )
		{
			update_option( 'buddyboss_pics_on', 0 );
			$error_messages[] = "Photo Uploading cannot be turned on without the activity component activated.";
			$error = true;
		}
		else {
			update_option( 'buddyboss_pics_on', $state );
			$state_lbl = ($state === TRUE)? "enabled": "disabled";
			$updated_messages[] = "User Photo Uploading is <strong>$state_lbl</strong>.";
			$updated = true;

			if ($state == 1)
			{
				$pics_activate_log = function_exists( 'buddyboss_pics_on_activate' )
													 ? buddyboss_pics_on_activate()
													 : '';
			}
			elseif ( $state == 0 )
			{
				$pics_activate_log = function_exists( 'buddyboss_pics_on_deactivate' )
													 ? buddyboss_pics_on_deactivate()
													 : '';
			}

		}
	}

	// Set default message if not already defined
	$message = '';

	if ( $updated || $error )
	{
		$message = '<div id="message">';

		if ( $error && ! empty( $error_messages ) )
		{
			$message .= '<div class="error"><p>'.implode( '</p><p>', $error_messages ).'</p></div><!-- /.error -->';
		}

		if ( $updated && ! empty( $updated_messages ) )
		{
			$message .= '<div class="updated"><p>'.implode( '</p><p>', $updated_messages ).'</p></div><!-- /.updated -->';
		}

		$message .= '</div> <!-- /#message -->';
	}

	// Hide BuddyPress dependant settings (Profile Walls and Photo Uploading) if BuddyPress is disabled
	$bp_is_enabled = function_exists( 'bp_is_active' );
	if (!function_exists('bp_is_active'))  $bpdisabledhide = 'style="display:none"';
	if (!function_exists('bp_is_active'))  $bpdisabledmessage = '<div id="message" class="updated"><p>Install and activate the <strong>BuddyPress</strong> plugin to enable Profile Walls and Photo Uploading.</p></div>';

	// Default markup when user hasn't uploaded a logo
	$logo_markup = '<h1 class="site-title">' . get_bloginfo( 'name' ) . '</h1>'
            	 . '<p class="site-description">' . get_bloginfo( 'description' ) . '</p>';

	// Grab the current logo, falling back to the site name and tag line
	$custom_logo = get_option( 'buddyboss_custom_logo', FALSE );

	if ( $custom_logo )
	{
		$logo_markup = '<img src="' . $custom_logo . '" class="buddyboss_logo_preview" />';
	}

	// Prepare HTML for radio button status
	$wall_on_status = (get_option("buddyboss_wall_on", 0) == 1 && function_exists("friends_get_alphabetically") == TRUE) ? "checked": "";
	$wall_off_status = (get_option("buddyboss_wall_on", 0) == 0) ? "checked": "";

	$adminbar_float_status = (get_option("buddyboss_adminbar", 0) == 0) ? "checked": "";
	$adminbar_fixed_status = (get_option("buddyboss_adminbar", 0) == 1) ? "checked": "";

	$pics_on_status = (get_option("buddyboss_pics_on", 0) == 1) ? "checked": "";
	$pics_off_status = (get_option("buddyboss_pics_on", 0) == 0) ? "checked": "";

	// Echo the HTML for the admin panel
	$html = <<<EOF

	<div class="wrap">

		<style type="text/css">
			.buddyboss_intro_text {
				padding-top: 5px;
			}
			.buddyboss_divider {
				width: 100%;
				height: 1px;
				line-height: 0;
				overflow: hidden;
				background: #ddd;
				margin: 20px 0 25px;
			}
			.buddyboss_logo_preview {
				border: 1px solid #ccc;
			}
			.buddyboss_subtext {
				color: #777;
			}
		</style>

		<h2>BuddyBoss Theme Settings</h2>

		$message

		<div class="welcome-panel">
			<div class="welcome-panel-content">
				
				<h3>Welcome to BuddyBoss</h3>
				<p class="about-description">Thanks for purchasing BuddyBoss! Here are some links to get you started.</p>

				<div class="welcome-panel-column-container">

					<div class="welcome-panel-column">
						<h4>Get Started</h4>
						<a class="button button-primary button-hero" href="http://www.buddyboss.com/tutorials/buddyboss-setup/" target="_blank">Setup Instructions</a>
						<p><a href="http://www.buddyboss.com/affiliates/affiliate-program/" target="_blank">Earn money with our affiliate program!</a></p>
					</div>


					<div class="welcome-panel-column welcome-panel-last">
						<h4>Need some help?</h4>
							<ul>
								<li><a href="http://www.buddyboss.com/faq/" target="_blank">Frequently Asked Questions</a></li>
								<li><a href="http://www.buddyboss.com/support-forums/" target="_blank">Support Forums</a></li>
								<li><a href="http://www.buddyboss.com/release-notes/" target="_blank">Current Version &amp; Release Notes</a></li>
								<li><a href="http://www.buddyboss.com/updating/" target="_blank">How to Update</a></li>
								<li><a href="http://www.buddyboss.com/child-themes/" target="_blank">Guide to Child Themes</a></li>
							</ul>
					</div>

				</div>

			</div>
		</div>

		<div class="buddyboss_divider"></div>

		<h3>Custom Logo</h3>

		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row">Preview</th>
					<td>
						<div id="headimg">
							$logo_markup
						</div>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">Select Image</th>
					<td>
						<p>
							You can select an image to be shown at the top of your site and in the admin screen by uploading from your computer.<br />
							The image will be used at the same size as you upload it.<br />
							Suggested maximum width is <strong>320 pixels</strong>. Bigger images will not look good on the login screen.
						</p>
						<form enctype="multipart/form-data" method="post">
							<label class="buddyboss_subtext">Choose an image from your computer:</label>
							<br />
							<input type="file" name="logo" />
							<input class="button-primary" type="submit" name="pb" value="Upload" />
							<input class="button-secondary" type="submit" name="del" value="Revert to default" />
						</form>
					</td>
				</tr>
			</tbody>
		</table>

		<div class="buddyboss_divider"></div>
		<form method="post">

		<h3>Admin Bar</h3>
		<p>
			Select your preferred front-end admin bar (Toolbar) location.
		</p>

		<table class="form-table">
			<tbody>
				<tr>
					<th>
						<label>
							<input type="radio" name="adminbar" value="0" $adminbar_float_status /> &nbsp;<strong>Float</strong> the admin bar menus so they move with the page.
						</label>
						<br />
						<label>
							<input type="radio" name="adminbar" value="1" $adminbar_fixed_status /> &nbsp;<strong>Fix</strong> the admin bar menus so they are docked to the top of the page.
						</label>
					</th>
				</tr>
			</tbody>
		</table>
		<br/>
EOF;

	if ( $bp_is_enabled )
	{
		$html .= <<<EOF

		<!-- div will hide Wall and Photo uploading if BuddyPress is disabled -->
		<div $bpdisabledhide>

			<h3>User Wall Posting</h3>
			<p>
				Make sure BuddyPress <strong>Activity Streams</strong> and <strong>Friend Connections</strong> are enabled for the Wall to function. Go to <em>Settings > BuddyPress > Components</em>.
			</p>

			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label>
								<input type="radio" name="wall" value="1" $wall_on_status /> &nbsp;Allow users to have Profile Walls, News Feeds, and Likes.
							</label>
							<br />
							<label>
								<input type="radio" name="wall" value="0" $wall_off_status /> &nbsp;Disable
							</label>
						</th>
					</tr>
				</tbody>
			</table>
			<br/>

			<h3>User Photo Uploading</h3>

			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label>
								<input type="radio" name="pics" value="1" $pics_on_status /> &nbsp;Allow users to upload photos.
							</label>
							<br />
							<label>
								<input type="radio" name="pics" value="0" $pics_off_status /> &nbsp;Disable
							</label>
						</th>
					</tr>
				</tbody>
			</table>

		</div><!-- end bpdisabled -->
EOF;
	}

	$html .= <<<EOF
		<p class="submit">
			<input type="submit" class="button button-primary" name="buddyboss_admin_update" value="Save configuration" />
		</p>

		</form>

		<div class="buddyboss_divider"></div>


		$bpdisabledmessage

	</div><!-- end .wrap -->
	<div class="clear"></div>
EOF;
	echo $html;
}
?>