<?php
/**
 * @package BuddyBoss
 */

/**
 * Sets up theme customizer
 */
function buddyboss_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}
add_action( 'customize_register', 'buddyboss_customize_register' );

/* This will be expanded upon in a later theme update */