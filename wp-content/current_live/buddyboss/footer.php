<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */
?>
	</div><!-- #main .wrapper -->
</div><!-- #page -->

<footer id="colophon" role="contentinfo">

    <?php if ( is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4') || is_active_sidebar('footer-5') ) : ?>

        <div class="footer-inner-top">
            <div class="footer-inner widget-area">

                <?php if ( is_active_sidebar('footer-1') ) : ?>
                    <div class="footer-widget">
                        <?php dynamic_sidebar( 'footer-1' ); ?>
                    </div><!-- .footer-widget -->
                <?php endif; ?>

                <?php if ( is_active_sidebar('footer-2') ) : ?>
                    <div class="footer-widget">
                        <?php dynamic_sidebar( 'footer-2' ); ?>
                    </div><!-- .footer-widget -->
                <?php endif; ?>

                <?php if ( is_active_sidebar('footer-3') ) : ?>
                    <div class="footer-widget">
                        <?php dynamic_sidebar( 'footer-3' ); ?>
                    </div><!-- .footer-widget -->
                <?php endif; ?>

                <?php if ( is_active_sidebar('footer-4') ) : ?>
                    <div class="footer-widget">
                        <?php dynamic_sidebar( 'footer-4' ); ?>
                    </div><!-- .footer-widget -->
                <?php endif; ?>

                <?php if ( is_active_sidebar('footer-5') ) : ?>
                    <div class="footer-widget last">
                        <?php dynamic_sidebar( 'footer-5' ); ?>
                    </div><!-- .footer-widget -->
                <?php endif; ?>

            </div><!-- .footer-inner -->
        </div><!-- .footer-inner-top -->

    <?php endif; ?>

    <div class="footer-inner-bottom">
        <div class="footer-inner">

            <div id="footer-links">
                <ul class="footer-menu">
                    <?php if ( has_nav_menu( 'secondary-menu' ) ) { ?>
                        <?php wp_nav_menu( array( 'container' => false, 'menu_id' => 'nav', 'theme_location' => 'secondary-menu', 'items_wrap' => '%3$s' ) ); ?>
                    <?php  } ?>
                </ul>
            </div>

            <div id="footer-credits">
                <p>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> &nbsp;&middot;&nbsp; <a href="http://www.petspage.com/Terms-Agreement.pdf" target="_blank" title="mobile BuddyPress themes">Terms of Use</a></p>
            </div>
          <div class="footer_social">
            	<ul>
                  <li><div class="social_1"><a href="https://www.facebook.com/mypetadvocate" target="_blank">&nbsp;</a></div></li>
                  <li><div class="social_2"><a href="https://twitter.com/KarenBostick" target="_blank">&nbsp;</a></div></li>
                  <li><div class="social_8"><a href="http://www.linkedin.com/company/pets-page" target="_blank">&nbsp;</a></div></li>
                  <!--<li><div class="social_4"><a href="#" target="_blank">&nbsp;</a></div></li>-->
                  <li><div class="social_6"><a href="http://www.youtube.com/user/PetsPageTV" target="_blank">&nbsp;</a></div></li>
                  <li><div class="social_3"><a href="http://instagram.com/petspage_com" target="_blank">&nbsp;</a></div></li>
                  <li><div class="social_5"><a href="http://www.pinterest.com/petspage/" target="_blank">&nbsp;</a></div></li>                 
                  <li><div class="social_7"><a href="mailto:tinks@petspage.com" target="_blank">&nbsp;</a></div></li>
                  
							</ul>
          </div>
    	</div><!-- .footer-inner -->
    </div><!-- .footer-inner-bottom -->

    <?php do_action( 'bp_footer' ) ?>

</footer><!-- #colophon -->

</div> <!-- #inner-wrap -->

</div><!-- #main-wrap (Wrap For Mobile) -->

<?php wp_footer(); ?>

<?php if ( !is_user_logged_in() ) : ?>
<script>
	$('.modal-box').click(function(e) {
		$(this).hide();
	});
	$('.modal-box .modal-box-inner').click(function(e) {
		e.stopPropagation();
	});
	$('.login_required').click(function(e) {
		$('.modal-box').show();
		e.preventDefault();
	});
</script>
<?php endif; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38438574-1', 'petspage.com');
  ga('send', 'pageview');

</script>

</body>
</html>