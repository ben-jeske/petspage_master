<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form role="search" method="get" id="bbp-search-index-form" action="<?php bbp_search_url(); ?>">
		<label class="screen-reader-text" for="bbp_search">
        	<input tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" placeholder="<?php _e( 'Search for:', 'bbpress' ); ?>"/>
        </label>
		
		<input tabindex="<?php bbp_tab_index(); ?>" class="button" type="submit" id="bbp_search_submit" value="<?php esc_attr_e( 'Search', 'bbpress' ); ?>" />
</form>

<!--

<form action="" method="get" id="search-members-form">
		<label><input type="text" name="s" id="members_search" placeholder="Search Members..."></label>
		<input type="submit" id="members_search_submit" name="members_search_submit" value="Search">
	<a href="#" id="clear-input"> </a>
 </form>
 -->   
    

