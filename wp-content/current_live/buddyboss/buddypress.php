<?php
/**
 * The template for displaying BuddyPress content.
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */

get_header(); ?>

	<!-- if widgets are loaded for any BuddyPress component, display the BuddyPress sidebar -->
	<?php if (
		( is_active_sidebar('members') && bp_is_page( BP_MEMBERS_SLUG ) && !bp_is_member() ) ||
		( is_active_sidebar('profile') && bp_is_member() ) ||
		( is_active_sidebar('groups') && bp_is_page( BP_GROUPS_SLUG ) && !bp_is_group() ) ||
		( is_active_sidebar('group') && bp_is_group() ) ||
		( is_active_sidebar('activity') && bp_is_page( BP_ACTIVITY_SLUG ) ) ||
		( is_active_sidebar('blogs') && bp_is_page( BP_BLOGS_SLUG ) && bp_is_active( 'blogs' ) ) ||
		( is_active_sidebar('forums') && bp_is_page( BP_FORUMS_SLUG ) )
	): ?>
		<div class="page-right-sidebar">

	<!-- if not, hide the sidebar -->
	<?php else: ?>
		<div class="page-full-width">
	<?php endif; ?>

			<!-- BuddyPress template content -->
			<div id="primary" class="site-content">

					<div id="content" role="main">

						<article>
						<?php while ( have_posts() ): the_post(); ?>
							<?php get_template_part( 'content', 'buddypress' ); ?>
							<?php comments_template( '', true ); ?>
						<?php endwhile; // end of the loop. ?>
						</article>

					</div><!-- #content -->

			</div><!-- #primary -->

			<?php get_sidebar('buddypress'); ?>

		</div><!-- closing div -->

<?php get_footer(); ?>