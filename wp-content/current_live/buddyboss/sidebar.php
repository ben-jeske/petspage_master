<?php
/**
 * The sidebar containing the widget area for WordPress blog posts and pages.
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */
?>
	
<!-- default WordPress sidebar -->
<div id="secondary" class="widget-area" role="complementary">
	<?php if ( is_active_sidebar('sidebar') ) : ?>
		<?php dynamic_sidebar( 'sidebar' ); ?>
	<?php else: ?>
		<aside class="widget widget-error">
			<p>Please <?php if ( !is_user_logged_in() ) : ?><a href="<?php echo wp_login_url(); ?>">log in</a> and<?php endif; ?> add widgets to the "Main Sidebar" widget area.</p>
			<?php if ( is_user_logged_in() ) : ?><p><a href="<?php echo get_option('siteurl') ?>/wp-admin/widgets.php">Add Widgets &raquo;</a></p><?php endif; ?>
		</aside>
	<?php endif; ?>
</div><!-- #secondary -->