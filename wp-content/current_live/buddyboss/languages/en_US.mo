Þ          ¼      \      Ð     Ñ     U  $   p          ²     Ä  >   Ú          ,  
   F  )   Q     {  (        ¹  H   Õ  H        g       S    U  ê  ¢   @     ã  4   	  !   8	     Z	     j	  C   	     É	     â	     
  +   
     =
  *   W
      
  R   £
  S   ö
     J     g  W                                                           	                              
            %1$s mentioned you in the group "%2$s":

"%3$s"

To view and respond to the message, log in and visit: %4$s

---------------------
 All Groups <span>%s</span> Any site member can join this group. Group Description (required) Group Memberships Group Name (required) Group content and activity will be visible to any site member. Group: Extra Links My Groups <span>%s</span> New Groups Notify group members of changes via email Post In Group Forum: The activity of groups I am a member of. There were no groups found. This group will be listed in the groups directory and in search results. This group will not be listed in the groups directory or search results. This is a private group This is a public group You are not a member of any groups so you don't have any group forums you can post in. To start posting, first find a group that matches the topic subject you'd like to start. If this group does not exist, why not <a href='%s'>create a new group</a>? Once you have joined or created the group you can post your topic in that group's forum. Project-Id-Version: BuddyBoss 3 v3.0.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2014-01-03 16:32-0700
Last-Translator: Ben <ben@boldvapor.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.6.3
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
Language: en_US
X-Poedit-SearchPath-0: .
 %1$s mentioned you on the Social Pet page for â%2$sâ:

â%3$sâ

To view and respond to the message, log in and visit: %4$s

ââââââââââ
 All Social Pets <span>%s</span> Any site member can become a fan of this Social Pet. Social Pet Description (required) Social Pet Fans Social Pet Name (required) Social Pet content and activity will be visible to any site member. Social Pets: Extra Links My Social Pets <span>%s</span> New Social Pets Notify Social Pet fans of changes via email Post In Social Pet Forum: The activity of Social Pets I am a fan of. There were no Social Pets found. This Social Pet will be listed in the Social Pets directory and in search results. This Social Pet will not be listed in the Social PetÆ directory or search results. This is a private Social Pet This is a public Social Pet You are not a fan of any Social Pets so you donât have any Social Pet forums you can post in. To start posting, first find a Social Pet you like. If this Social Pet does not exist, why not <a href=â%sâ>create a new Social Pet</a>? Once you have become a fan or created the Social Pet you can post your topic in that Social Petâs forum. 