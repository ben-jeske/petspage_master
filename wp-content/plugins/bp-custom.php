<?php 

// this function initializes the iframe elements 

function add_iframe($initArray) {
$initArray['extended_valid_elements'] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]";
return $initArray;
}

// this function alters the way the WordPress editor filters your code
add_filter('tiny_mce_before_init', 'add_iframe');

// change "en_US" to your locale
define( 'BPLANG', 'en_US' );
if ( file_exists( WP_LANG_DIR . '/' . BPLANG . '.mo' ) ) {
    load_textdomain( 'buddypress', WP_LANG_DIR . '/' . BPLANG . '.mo' );
}

define ( 'BP_GROUPS_SLUG', 'social-pets' );
define ( 'BP_MEMBERS_SLUG', 'pet-lovers' );
 
define ( 'BP_AVATAR_THUMB_WIDTH', 250 );
define ( 'BP_AVATAR_THUMB_HEIGHT', 250 );
define ( 'BP_AVATAR_FULL_WIDTH', 500 );
define ( 'BP_AVATAR_FULL_HEIGHT', 500 );
define ( 'BP_AVATAR_ORIGINAL_MAX_FILESIZE', 40000096 );

?>