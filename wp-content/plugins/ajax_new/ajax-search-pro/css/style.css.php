<?php echo ((isset($style['script-inputfont']))?$style['script-inputfont']:""); ?>
<?php echo ((isset($style['script-descfont']))?$style['script-descfont']:""); ?>
<?php echo ((isset($style['script-titlefont']))?$style['script-titlefont']:""); ?>
<?php echo ((isset($style['script-titlehoverfont']))?$style['script-titlehoverfont']:""); ?>
<?php echo ((isset($style['script-authorfont']))?$style['script-authorfont']:""); ?>
<?php echo ((isset($style['script-datefont']))?$style['script-datefont']:""); ?>
<?php echo ((isset($style['script-showmorefont']))?$style['script-showmorefont']:""); ?>
<?php echo ((isset($style['script-groupfont']))?$style['script-groupfont']:""); ?>
<?php echo ((isset($style['script-exsearchincategoriestextfont']))?$style['script-exsearchincategoriestextfont']:""); ?>
<?php echo ((isset($style['script-groupbytextfont']))?$style['script-groupbytextfont']:""); ?>  


.clear {
  clear: both;
}

.hiddend {
  display:none;
}

#ajaxsearchpro<?php echo $id; ?> textarea:focus, 
#ajaxsearchpro<?php echo $id; ?> input:focus{
    outline: none;
}

#ajaxsearchpro<?php echo $id; ?> {
  width: 100%;
  height: 40px;
  border-radius: 5px;
  background: #d1eaff;
  background: <?php echo $style['boxbackground']; ?>;
  overflow: hidden;
  <?php echo $style['boxborder']; ?>
  <?php echo $style['boxshadow']; ?>
}

#ajaxsearchpro<?php echo $id; ?> .probox {
  width: auto;
  margin: 4px;
  height: 30px;
  border-radius: 5px;
  background: #FFF;
  overflow: hidden;
  border: 1px solid #FFF;
  box-shadow: 1px 0 3px #CCCCCC inset;
  background: <?php echo $style['inputbackground']; ?>;
  <?php echo $style['inputborder']; ?>
  <?php echo $style['inputshadow']; ?>
}

#ajaxsearchpro<?php echo $id; ?> .probox .proinput {
  width: auto;
  height: 100%;
  margin: 2px 0px 0px 10px;
  padding: 5px;
  float: left;
  box-shadow: none;
  position: relative;
  <?php echo $style['inputfont']; ?>
}

#ajaxsearchpro<?php echo $id; ?> .probox .proinput input {
  border: 0px;
  background: transparent;
  width: 100%;
  box-shadow: none;
  margin: 0;
  padding: 0;
  left: 0;
  <?php echo $style['inputfont']; ?>
}

#ajaxsearchpro<?php echo $id; ?> .probox .proinput input.autocomplete {
  border: 0px;
  background: transparent;
  width: 100%;
  box-shadow: none;
  margin: 0;
  padding: 0;
  left: 0;
  <?php echo $style['inputfont']; ?>
}

#ajaxsearchpro<?php echo $id; ?> .probox .proinput.iepaddingfix {
  padding-top: 0;
}

#ajaxsearchpro<?php echo $id; ?> .probox .proinput .loading {
  width: 32px;
  background: #000;
  height: 100%;
  box-shadow: none;
}

#ajaxsearchpro<?php echo $id; ?> .probox .proloading,
#ajaxsearchpro<?php echo $id; ?> .probox .promagnifier,
#ajaxsearchpro<?php echo $id; ?> .probox .prosettings  {
  width: 32px;
  height: 32px;
  background: none;
  float: right;
  box-shadow: none;
  margin: 0;
  padding: 0;
}

#ajaxsearchpro<?php echo $id; ?> .probox .proloading {
  background: url("<?php echo plugins_url().((isset($style['selected-loadingimage']) && $style['selected-loadingimage']!='')?$style['selected-loadingimage']:$style['loadingimage']); ?>?v=18") no-repeat;
  background-position:center center;
  visibility: hidden; 
}

#ajaxsearchpro<?php echo $id; ?> .probox .promagnifier {
  background: url("<?php echo  plugins_url().((isset($style['selected-magnifierimage']) && $style['selected-magnifierimage']!='')?$style['selected-magnifierimage']:$style['magnifierimage']); ?>?v=18") no-repeat <?php echo $style['magnifierbackground']; ?>;
  background-position:center center;
  cursor: pointer;
}


#ajaxsearchpro<?php echo $id; ?> .probox .prosettings {
  background: url("<?php echo  plugins_url().((isset($style['selected-settingsimage']) && $style['selected-settingsimage']!='')?$style['selected-settingsimage']:$style['settingsimage']); ?>?v=18") no-repeat <?php echo $style['settingsbackground']; ?>;
  background-position:center center;
  cursor: pointer;
}

#ajaxsearchprores<?php echo $id; ?> * {
  text-decoration: none;
  text-shadow: none;
}

#ajaxsearchprores<?php echo $id; ?> {
  padding: 4px;
  background: #D1EAFF;
  background: <?php echo $style['resultsbackground']; ?>;
  border-radius: 3px;
  <?php echo $style['resultsborder']; ?>
  <?php echo $style['resultshadow']; ?>
  position: absolute;
  visibility: hidden;
  z-index:1100;
}

#ajaxsearchprores<?php echo $id; ?> .results .nores {
  overflow: hidden;
  width: auto;
  height: 100%;
  line-height: auto;
  text-align: center;
  margin: 0;
  background: #FFF;
}

#ajaxsearchprores<?php echo $id; ?> .results .nores .keyword{
  padding: 0 6px;
  cursor: pointer;
  <?php echo $style['descfont']; ?>
  font-weight: bold;
}

#ajaxsearchprores<?php echo $id; ?> .results {
  overflow: hidden;
  width: auto;
  height: 0;
  margin: 0;
  padding: 0;
}

#ajaxsearchprores<?php echo $id; ?> .results .item {
  overflow: hidden;
  width: auto;
  height: 70px;
  margin: 0;
  margin-bottom: -3px;
  padding: 3px;
  position: relative;
  background: #f4f4f4;
  background: <?php echo $style['resultscontainerbackground']; ?>;
  border-radius: 3px;
  border-left: 1px solid rgba(255, 255, 255, 0.6);
  border-right: 1px solid rgba(255, 255, 255, 0.4);
}

#ajaxsearchprores<?php echo $id; ?> .results .item:last-child  {
  margin-bottom: 0px;
}


#ajaxsearchprores<?php echo $id; ?> .results .item .image {
  overflow: hidden;
  width: 70px;
  height: 70px;
  background: #000;
  background: <?php echo $style['resultscontentbackground']; ?>;
  margin: 0;
  padding: 0;
  float: left;
}

#ajaxsearchprores<?php echo $id; ?> .results .item .content {
  overflow: hidden;
  width: 50%;
  height: 70px;
  background: #fff;
  background: <?php echo $style['resultscontentbackground']; ?>;
  margin: 0;
  padding: 0 10px;
  float: right;
}

#ajaxsearchprores<?php echo $id; ?> .results .item .content h3 {
  margin: 0;
  padding: 0;
  line-height: inherit;
  <?php echo $style['titlefont']; ?>
}

#ajaxsearchprores<?php echo $id; ?> .results .item .content h3 a span.overlap {
  position:absolute; 
  width:100%;
  height:100%;
  top:0;
  left: 0;
  z-index: 1;
  background-image: url('empty.gif');
}

#ajaxsearchprores<?php echo $id; ?> .results .item .content h3 a {
  margin: 0;
  padding: 0;
  line-height: inherit;
  <?php echo $style['titlefont']; ?>
}

#ajaxsearchprores<?php echo $id; ?> .results .item .content h3 a:hover {
  <?php echo $style['titlehoverfont']; ?>
}

#ajaxsearchprores<?php echo $id; ?> .results .item div.etc {
  margin: 2px 3px;
  padding: 0;
  line-height: 10px;
  <?php echo $style['authorfont']; ?>
}
#ajaxsearchprores<?php echo $id; ?> .results .item .etc .author {
  padding: 0;
  <?php echo $style['authorfont']; ?>
}
#ajaxsearchprores<?php echo $id; ?> .results .item .etc .date {
  margin: 0 0 0 10px;
  padding: 0;
  <?php echo $style['datefont']; ?>  
}
#ajaxsearchprores<?php echo $id; ?> .resdrg {                                                                                                                                     
  height: auto;
}

#ajaxsearchprores<?php echo $id; ?> .results .item p.desc {
  margin: 2px 0px;
  padding: 0;
  <?php echo $style['descfont']; ?>
}

#ajaxsearchprores<?php echo $id; ?> .mCSB_container{
	width:auto;
	margin-right:20px;
	overflow:hidden;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_container.mCS_no_scrollbar{
	margin-right:0;
}
#ajaxsearchprores<?php echo $id; ?> .mCustomScrollBox .mCSB_scrollTools{
	width:16px;
	height:100%;
	top:0;
	right:0;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_draggerContainer{
	height:100%;
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	box-sizing:border-box;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp+.mCSB_draggerContainer{
	padding-bottom:40px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_draggerRail{
	width:10px;
	height:100%;
	margin:0 auto;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
	border-radius:10px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger{
	cursor:pointer;
	width:100%;
	height:30px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar{
	width:10px;
	height:100%;
	margin:0 auto;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
	border-radius:10px;
	text-align:center;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown{
	height:20px;
	-overflow:hidden;
	margin:0 auto;
	cursor:pointer;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown{
	bottom:0;
	margin-top:-40px;
}

#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_container{
	height:auto;
	margin-right:0;
	margin-bottom:30px;
	overflow:hidden;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_container.mCS_no_scrollbar{
	margin-bottom:0;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal.mCustomScrollBox .mCSB_scrollTools{
	width:100%;
	height:16px;
	top:auto;
	right:auto;
	bottom:0;
	left:0;
	overflow:hidden;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_draggerContainer{
	height:100%;
	width:auto;
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	box-sizing:border-box;
	overflow:hidden;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_buttonLeft+.mCSB_draggerContainer{
	padding-bottom:0;
	padding-right:20px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_draggerRail{
	width:100%;
	height:2px;
	margin:7px 0;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
	border-radius:10px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_dragger{
	width:30px;
	height:100%;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar{
	width:100%;
	height:4px;
	margin:6px auto;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
	border-radius:10px;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_buttonLeft,
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_buttonRight{
	width:20px;
	height:100%;
	overflow:hidden;
	margin:0 auto;
	cursor:pointer;
	float:left;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_horizontal .mCSB_scrollTools .mCSB_buttonRight{
	right:0;
	bottom:auto;
	margin-left:-40px;
	margin-top:-16px;
	float:right;
}


#ajaxsearchprores<?php echo $id; ?> .mCustomScrollBox .mCSB_scrollTools{
	opacity:0.75;
}
#ajaxsearchprores<?php echo $id; ?> .mCustomScrollBox:hover .mCSB_scrollTools{
	opacity:1;
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_draggerRail{
	background:#000; /* rgba fallback */
	background:rgba(0,0,0,0.4);
	filter:"alpha(opacity=40)"; -ms-filter:"alpha(opacity=40)"; /* old ie */
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar{
	background:#fff; /* rgba fallback */
	background:rgba(<?php echo wpdreams_hex2rgb($style['overflowcolor']); ?>,0.9);
	filter:"alpha(opacity=90)"; -ms-filter:"alpha(opacity=90)"; /* old ie */
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar{
	background:rgba(<?php echo wpdreams_hex2rgb($style['overflowcolor']); ?>,0.95);
	filter:"alpha(opacity=95)"; -ms-filter:"alpha(opacity=95)"; /* old ie */
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar{
	background:rgba(<?php echo wpdreams_hex2rgb($style['overflowcolor']); ?>,1);
	filter:"alpha(opacity=100)"; -ms-filter:"alpha(opacity=100)"; /* old ie */
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonLeft,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonRight{
	padding: 10px 0 0 0;
  background:0;
	opacity:0.4;
	filter:"alpha(opacity=40)"; -ms-filter:"alpha(opacity=40)"; /* old ie */
}

#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown { height:0;position: relative; } 
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown:after { top: 100%; border: solid transparent; content: " "; height: 0; width: 0; position: absolute;} 
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown:after { border-color: rgba(136, 183, 213, 0); border-top-color: <?php echo $style['arrowcolor']; ?>; border-width: 8px; left: 50%; margin-left: -8px; }
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp { position: relative; margin:10px 0 0 0; height: 0; } 
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp:after { bottom: 100%; border: solid transparent; content: " "; height: 0; width: 0; position: absolute; } 
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp:after { border-color: rgba(136, 183, 213, 0); border-bottom-color:  <?php echo $style['arrowcolor']; ?>; border-width: 8px; left: 50%; margin-left: -8px; }
 
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp{
	background-position:0 0;
	/* 
	sprites locations are 0 0/-16px 0/-32px 0/-48px 0 (light) and -80px 0/-96px 0/-112px 0/-128px 0 (dark) 
	*/
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown{
	background-position:0 -20px;
	/* 
	sprites locations are 0 -20px/-16px -20px/-32px -20px/-48px -20px (light) and -80px -20px/-96px -20px/-112px -20px/-128px -20px (dark) 
	*/
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonLeft{
	background-position:0 -40px;
	/* 
	sprites locations are 0 -40px/-20px -40px/-40px -40px/-60px -40px (light) and -80px -40px/-100px -40px/-120px -40px/-140px -40px (dark) 
	*/
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonRight{
	background-position:0 -56px;
	/* 
	sprites locations are 0 -56px/-20px -56px/-40px -56px/-60px -56px (light) and -80px -56px/-100px -56px/-120px -56px/-140px -56px (dark) 
	*/
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp:hover,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown:hover,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonLeft:hover,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonRight:hover{
	opacity:0.75;
	filter:"alpha(opacity=75)"; -ms-filter:"alpha(opacity=75)"; /* old ie */
}
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonUp:active,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonDown:active,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonLeft:active,
#ajaxsearchprores<?php echo $id; ?> .mCSB_scrollTools .mCSB_buttonRight:active{
	opacity:0.9;
	filter:"alpha(opacity=90)"; -ms-filter:"alpha(opacity=90)"; /* old ie */
}    

#ajaxsearchprores<?php echo $id; ?> span.highlighted{
    font-weight: bold;
    color: #d9312b;
    background-color: #eee;
    color: <?php echo $style['highlightcolor'] ?>;
    background-color: <?php echo $style['highlightbgcolor'] ?>;
}

#ajaxsearchprores<?php echo $id; ?> p.showmore {
  text-align: center;
  padding: 0;
  margin: 0;
  <?php echo $style['showmorefont']; ?> 
}

#ajaxsearchprores<?php echo $id; ?> p.showmore a{
  <?php echo $style['showmorefont']; ?> 
}

#ajaxsearchprores<?php echo $id; ?> .group {
  background: #DDDDDD;
  background: <?php echo $style['exsearchincategoriesboxcolor']; ?>;
  border-radius: 3px 3px 0 0;
  border-top: 1px solid rgba(<?php echo wpdreams_hex2rgb($style['resultscontainerbackground']); ?>, 0.7);
  border-left: 1px solid rgba(<?php echo wpdreams_hex2rgb($style['resultscontainerbackground']); ?>, 0.7);
  border-right: 1px solid rgba(<?php echo wpdreams_hex2rgb($style['resultscontainerbackground']); ?>, 0.7);
  margin: 10px 0 -3px;
  padding: 7px 0 7px 10px;
  position: relative;
  z-index: 1000;
  <?php echo $style['groupbytextfont']; ?> 
}

#ajaxsearchprores<?php echo $id; ?> .group:first-of-type {
  margin: 0px 0 -3px;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings  {
  width: 200px;
  height: auto;
  background: <?php echo $style['settingsdropbackground'] ?>;
  position: absolute;
  display: none;
  z-index: 1101;
  border-radius: 0 0 3px 3px;
  box-shadow: 2px 2px 3px -1px #AAAAAA;
  visibility: hidden;
  padding: 0 0 8px 0;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option {
  margin: 10px;
  *padding-bottom: 10px;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings.ie78 .option {
  margin-bottom: 0 !important;
  padding-bottom: 0 !important;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .label {
  float: left;
  font-size: 14px;
  line-height: 24px;
  margin: 6px 10px 0 0;
  width: 143px;
  text-shadow: none;
  padding: 0;
  border: none;
  background: transparent;
  color: <?php echo $style['settingsdropbackgroundfontcolor'] ?>;
}

/* SQUARED THREE */
#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option input[type=checkbox] {	
  display:none;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings.ie78 .option input[type=checkbox] {	
  display:block;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings.ie78 .label {	
  float:right !important;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option {
	width: 20px;	
	position: relative;
  float: left;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option label {
	cursor: pointer;
	position: absolute;
	width: 20px;
	height: 20px;
	top: 0;
  padding: 0;
	border-radius: 4px;
	-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);
	-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);
	box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);
  background: #45484d;
	background: -webkit-linear-gradient(top, #222222 0%, #45484d 100%);
	background: -moz-linear-gradient(top, #222222 0%, #45484d 100%);
	background: -o-linear-gradient(top, #222222 0%, #45484d 100%);
	background: -ms-linear-gradient(top, #222222 0%, #45484d 100%);
	background: linear-gradient(top, #222222 0%, #45484d 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#222222', endColorstr='#45484d',GradientType=0 );
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings.ie78 .option label {
  display:none;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	opacity: 0;
	content: '';
	position: absolute;
	width: 9px;
	height: 5px;
	background: transparent;
	top: 4px;
	left: 4px;
	border: 3px solid #fcfff4;
	border-top: none;
	border-right: none;

	-webkit-transform: rotate(-45deg);
	-moz-transform: rotate(-45deg);
	-o-transform: rotate(-45deg);
	-ms-transform: rotate(-45deg);
	transform: rotate(-45deg);
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings.ie78 .option label:after {
  display:none;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option label:hover::after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	opacity: 0.3;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings .option input[type=checkbox]:checked + label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	opacity: 1;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings fieldset {
  position:relative;
  float:left;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings fieldset .categoryfilter {
  max-height: <?php echo $style['exsearchincategoriesheight']; ?>px;
  overflow: auto;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings fieldset {
  background: transparent;
  font-size: 0.9em;
  margin: 5px 0 0;
  padding: 0px;
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings  fieldset legend {
  padding: 5px 0 0 10px;
  margin: 0;
  <?php echo $style['exsearchincategoriestextfont']; ?>
}

#ajaxsearchprosettings<?php echo $id; ?>.searchsettings fieldset .label {
  width: 130px;
}