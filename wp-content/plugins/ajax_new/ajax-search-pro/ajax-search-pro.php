<?php
/*
Plugin Name: Ajax Search Pro
Plugin URI: http://wp-dreams.com
Description: Ajax Search Pro is a powerful ajax powered search engine for WordPress. Compatible with WordPress 3.4+, WooCommerce, JigoShop and WP-ecommerce.
Version: 1.81
Author: Ernest Marcinko
Author URI: http://wp-dreams.com
*/
?>
<?php
error_reporting(0);
  define( 'AJAXSEARCHPRO_PATH', plugin_dir_path(__FILE__) );
  define( 'AJAXSEARCHPRO_DIR', 'ajax-search-pro');

  /* Egyedi suffix class nevekhez k�z�s term�kekn�l */
  global $wpdreams_unique;
  $wpdreams_unique = md5(plugin_dir_url(__FILE__)); 
  
  /*A headerbe �rkez� scripteket �s css f�jlokat csak itt lehet hozz�adni, alpageken nem! Ott m�r az az action lefutott! */
  
  /* Includes only on ASP admin pages */
  if (isset($_GET) && isset($_GET['page']) && (
    $_GET['page']=="ajax-search-pro/settings.php" ||
    $_GET['page']=="ajax-search-pro/statistics.php" ||
    $_GET['page']=="ajax-search-pro/comp_check.php" ||
    $_GET['page']=="ajax-search-pro/cache_settings.php" 
  )) {
    require_once(AJAXSEARCHPRO_PATH."/settings/types.inc.php");
    require_once(AJAXSEARCHPRO_PATH."/includes/compatibility.class.php");
    require_once(AJAXSEARCHPRO_PATH."/compatibility.php");
  }
  
  /* Includes only on ASP admin pages, frontend, ajax requests */
  if ((isset($_GET) && isset($_GET['page']) && (
    $_GET['page']=="ajax-search-pro/settings.php" ||
    $_GET['page']=="ajax-search-pro/statistics.php" ||
    $_GET['page']=="ajax-search-pro/comp_check.php" ||
    $_GET['page']=="ajax-search-pro/cache_settings.php" 
  )) || !is_admin() || (isset($_POST) && isset($_POST['action'])) ) {
    require_once(AJAXSEARCHPRO_PATH."/settings/default_options.php");
    require_once(AJAXSEARCHPRO_PATH."/settings/admin-ajax.php");
    require_once(AJAXSEARCHPRO_PATH."/functions.php");
    require_once(AJAXSEARCHPRO_PATH."/includes/shortcodes.php");
    require_once(AJAXSEARCHPRO_PATH."/search.php");
  }
  require_once(AJAXSEARCHPRO_PATH."/includes/widgets.php");
  $funcs = new ajaxsearchproFuncCollector();
  /*
    Create pages
  */
  add_action( 'admin_menu', array($funcs, 'navigation_menu') );  
  
  /*
    Add Hacks
  */
 
  register_activation_hook( __FILE__, array($funcs, 'ajaxsearchpro_activate') );
  add_action('wp_print_styles', array($funcs, 'styles'));
  add_action('wp_enqueue_scripts', array($funcs, 'scripts'));
  add_action('wp_footer', array($funcs, 'footer'));
  if (isset($_GET) && isset($_GET['page']) && (
    $_GET['page']=="ajax-search-pro/settings.php" ||
    $_GET['page']=="ajax-search-pro/statistics.php" ||
    $_GET['page']=="ajax-search-pro/comp_check.php" ||
    $_GET['page']=="ajax-search-pro/cache_settings.php"
  ))
    add_action( 'admin_enqueue_scripts', array($funcs, 'scripts') );
  //add_action('wp_ajax_reorder_slides', array($funcs, 'reorder_slides')); 

  class ajaxsearchproFuncCollector {
  
    function ajaxsearchpro_activate() {

      global $wpdb;
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      $table_name = $wpdb->prefix . "ajaxsearchpro";
      $query = "
        CREATE TABLE IF NOT EXISTS `$table_name` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` text NOT NULL,
          `data` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
      "; 
      dbDelta($query);
      $table_name = $wpdb->prefix . "ajaxsearchpro_statistics";
      $query = "
        CREATE TABLE IF NOT EXISTS `$table_name` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `search_id` int(11) NOT NULL,
          `keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
          `num` int(11) NOT NULL,
          `last_date` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
      "; 
      dbDelta($query);
      $query = "ALTER TABLE `$table_name` MODIFY `keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL"; 
      dbDelta($query);
      $wpdb->query($query);
    }
        
    function navigation_menu() {
      if (current_user_can('manage_options'))  {
      	if (!defined("EMU2_I18N_DOMAIN")) define('EMU2_I18N_DOMAIN', "");
        add_menu_page( 
      	 __('Ajax Search Pro', EMU2_I18N_DOMAIN),
      	 __('Ajax Search Pro', EMU2_I18N_DOMAIN),
      	 'manage_options',
      	 AJAXSEARCHPRO_DIR.'/settings.php',
      	 '',
      	 plugins_url('/icon.png', __FILE__),
         "207.9"
        );   
      	add_submenu_page( 
        	AJAXSEARCHPRO_DIR.'/settings.php',
        	__("Ajax Search Pro", EMU2_I18N_DOMAIN),
        	__("Search Statistics", EMU2_I18N_DOMAIN),
        	'manage_options',
        	AJAXSEARCHPRO_DIR.'/statistics.php'
      	);
      	add_submenu_page( 
        	AJAXSEARCHPRO_DIR.'/settings.php',
        	__("Ajax Search Pro", EMU2_I18N_DOMAIN),
        	__("Error check", EMU2_I18N_DOMAIN),
        	'manage_options',
        	AJAXSEARCHPRO_DIR.'/comp_check.php'
      	);
      	add_submenu_page( 
        	AJAXSEARCHPRO_DIR.'/settings.php',
        	__("Ajax Search Pro", EMU2_I18N_DOMAIN),
        	__("Cache Settings", EMU2_I18N_DOMAIN),
        	'manage_options',
        	AJAXSEARCHPRO_DIR.'/cache_settings.php'
      	);          
      }          
    }
     
    function styles() {
      wp_register_style('wpdreams-scroller', plugin_dir_url(__FILE__).'css/jquery.mCustomScrollbar.css');
      wp_enqueue_style('wpdreams-scroller');   
    }
    
    function scripts() {
      global $wp_version;
      //Load special jQuery for avoiding conflicts :)
      //jQuery 1.7.2 in aspjQuery object :)
      
      if (wpdreams_ismobile() && 0) {
        wp_register_script('wpdreams-aspjquery',  plugins_url('js/nomin/aspjquery.js' , __FILE__));
        wp_enqueue_script('wpdreams-aspjquery');
        wp_register_script('wpdreams-aspjquery-ui', plugins_url('js/nomin/jquery.ui.js' , __FILE__),  array('wpdreams-aspjquery'));
        wp_enqueue_script('wpdreams-aspjquery-ui');
        wp_register_script('wpdreams-dragfix', plugins_url('js/nomin/jquery.drag.fix.js' , __FILE__), array('wpdreams-aspjquery-ui'));
        wp_enqueue_script('wpdreams-dragfix');
        wp_register_script('wpdreams-easing', plugins_url('js/nomin/jquery.easing.js' , __FILE__), array('wpdreams-aspjquery'));
        wp_enqueue_script('wpdreams-easing');
        wp_register_script('wpdreams-mousewheel',plugins_url('js/nomin/jquery.mousewheel.min.js' , __FILE__), array('wpdreams-aspjquery'));
        wp_enqueue_script('wpdreams-mousewheel');
        wp_register_script('wpdreams-scroll', plugins_url('js/nomin/jquery.mCustomScrollbar.js' , __FILE__), array('wpdreams-aspjquery', 'wpdreams-mousewheel'));
        wp_enqueue_script('wpdreams-scroll');
        wp_register_script('wpdreams-highlight', plugins_url('js/nomin/jquery.highlight.js' , __FILE__), array('wpdreams-aspjquery'));
        wp_enqueue_script('wpdreams-highlight');
        wp_register_script('wpdreams-ajaxsearchpro', plugins_url('js/nomin/jquery.ajaxsearchpro.js' , __FILE__), array('wpdreams-aspjquery', "wpdreams-scroll"));
        wp_enqueue_script('wpdreams-ajaxsearchpro');
      } else {
        wp_register_script('wpdreams-ajaxsearchpro', plugins_url('js/jquery.ajaxsearchpro.min.js' , __FILE__));
        wp_enqueue_script('wpdreams-ajaxsearchpro');
      }  
      wp_localize_script( 'wpdreams-ajaxsearchpro', 'ajaxsearchpro', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

    } 
    
    function footer() {
      ?>
        <script>
        (function($){
           $(document).ready(function() {
              function preCache() {
                var data = {
              	  action: 'ajaxsearchpro_precache'  
              	};
              	$.post(ajaxsearchpro.ajaxurl, data, function(response) {
                   console.log(response);
              	}, "json"); 
              }
              preCache()
              setInterval(function(){
                 preCache();
              }, 15000);
           });
         })(aspjQuery);
        </script>
      <?php     
    }  
  }


?>
