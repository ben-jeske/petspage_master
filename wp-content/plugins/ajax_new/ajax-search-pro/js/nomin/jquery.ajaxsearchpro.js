/*! Ajax Search pro 1.5 js */
(function( $ ){  
  var methods = {
     init : function( options ) {      
       var $this =  $.extend({}, this, methods); 
       $this.searching = false;
       $this.o = $.extend({}, options);
       $this.n = new Object();
       $this.n.container = $(this);
       $this.n.probox = $('.probox', this);
       $this.n.proinput = $('.proinput', this);
       $this.n.text = $('.proinput input.orig', this);
       $this.n.textAutocomplete = $('.proinput input.autocomplete', this);
       $this.n.loading = $('.proinput .loading', this);
       $this.n.proloading = $('.proloading', this);
       $this.n.promagnifier = $('.promagnifier', this);
       $this.n.prosettings = $('.prosettings', this);
       $this.n.searchsettings = $('.searchsettings', this);
       $this.n.resultsDiv = $(this).next();
       $this.n.showmore = $('.showmore a', $this.n.resultsDiv);
       $this.n.items = $('.item', $this.n.resultsDiv);
       $this.n.results =  $('.results', $this.n.resultsDiv);
       $this.n.resdrg =  $('.resdrg', $this.n.resultsDiv);
       $this.n.drag = $('.resdrg', $this.n.resultsDiv);
       $this.o.id = $this.n.container.attr('id').match(/^ajaxsearchpro(.*)/)[1];
       $this.firstClick = true;
       $this.cleanUp();
       $this.n.text.val($this.o.defaultsearchtext); 
       $this.n.textAutocomplete.val('');  
       
       $this.n.resultsDiv.appendTo("body");
       $this.n.searchsettings.appendTo("body");

       if ( $.browser.msie && $.browser.version<9 ) {
          $this.n.searchsettings.addClass("ie78");
       }

       $this.n.text.click(function(){
          if ($this.firstClick) {
            $(this).val('');
            $this.firstClick = false;
          }
       });
       $this.n.resultsDiv.css({
        opacity: 0
       });
       $(document).bind("click touchend", function(){
          if ($this.opened==false) return;
          $this.hideResults();
          $this.hideSettings();
       });
       $(this).bind("click touchend", function(e){
          e.stopImmediatePropagation();
       });
       $this.n.resultsDiv.bind("click touchend", function(e){
          e.stopImmediatePropagation();
       });
       $this.n.searchsettings.bind("click touchend", function(e){
          e.stopImmediatePropagation();
       });
       $this.scroll = $this.n.results.mCustomScrollbar({
          scrollButtons:{ 
            enable:true, 
            scrollType:"pixels", 
            scrollSpeed:parseInt($this.o.resultitemheight), 
            scrollAmount:parseInt($this.o.resultitemheight)
          },
          callbacks:{
            onScroll:function(){
                if (is_touch_device()) return;
                var top = parseInt($('.mCSB_container', $this.n.results).position().top);
                var children = $('.mCSB_container .resdrg').children();
                var overall = 0;
                var prev = 3000;
                var diff = 4000;
                var s_diff = 10000;
                var s_overall = 10000;
                var $last = null;
                children.each(function(){
                    diff = Math.abs((Math.abs(top)-overall));
                    if (diff<prev) {
                      s_diff = diff;
                      s_overall = overall;
                      $last = $(this);
                    } 
                    overall += $(this).outerHeight(true);
                    prev = diff;
                });
                if ($last.hasClass('group'))
                  s_overall = s_overall+($last.outerHeight(true)-$last.outerHeight(false));
                $(".mCSB_container", $this.n.resultsDiv).animate({
                  top: -s_overall
                }); 
            }
          }
       });
       $this.n.prosettings.click(function(){
          if ($this.n.prosettings.attr('opened')==0) {
            $this.showSettings();
          } else {
            $this.hideSettings();          
          }
       });
           
       var t;
       $(window).bind("resize", function(){
           $this.resize();
       });
       $(window).bind("scroll", function() {
           $this.scrolling(false);
       });
       $(window).trigger('resize');
       $(window).trigger('scroll');
       $this.n.promagnifier.add($this.n.text).bind('click keyup', function(e){
         if (window.event) {
           $this.keycode = window.event.keyCode;
           $this.ktype = window.event.type;
         } else if (e) {
           $this.keycode = e.which;
           $this.ktype = e.type;
         }

         if ($(this).hasClass('orig') && $this.ktype=='click') return;
         if ($this.o.redirectonclick==1 && $this.ktype=='click') {
          location.href = $this.o.homeurl+'?s='+$this.n.text.val();
          return;
         }
         if ($this.o.triggeronclick==0 && $this.ktype=='click') return;

         if (($this.o.triggerontype==0 && $this.ktype=='keyup') || ($this.ktype=='keyup' && is_touch_device())) return;
         clearTimeout(t);
         t = setTimeout (function() {
          $this.search();
          t = null;
         }, 700);
       });
       var tt;
       if ($this.o.autocomplete==1 && !is_touch_device()) {
         $this.n.text.keydown(function(e){
            if (window.event) {
              $this.keycode = window.event.keyCode;
              $this.ktype = window.event.type;
            } else if (e) {
              $this.keycode = e.which;
              $this.ktype = e.type;
            }
            if($this.keycode==39) {
              $this.n.text.val($this.n.textAutocomplete.val());
            } else {
                clearTimeout(tt);
                tt = setTimeout (function() {
                $this.autocomplete();
                tt = null;
               }, 300);
            }
         }); 
       }
       return $this;
     },
     destroy : function( ) {
       return this.each(function(){
         var $this =  $.extend({}, this, methods);
         $(window).unbind($this);
       })
     },
     searchfor: function(phrase) {
        $(".proinput input",this).val(phrase).trigger("keyup");
     },
     autocomplete : function() {
        var $this =  $.extend({}, this, methods);  
        var val = $this.n.text.val();
        if ($this.n.text.val()=='') {
          $this.n.textAutocomplete.val('');
          return;
        }
        var autocompleteVal = $this.n.textAutocomplete.val();
        if (autocompleteVal!='' && autocompleteVal.indexOf(val)==0) {
          return;
        } else {
          $this.n.textAutocomplete.val('');
        }
        var data = {
      	  action: 'ajaxsearchpro_autocomplete',
          asid: $this.o.id,
          sauto: $this.n.text.val() 
      	};
      	$.post(ajaxsearchpro.ajaxurl, data, function(response) {
          $this.n.textAutocomplete.val(response);
        });
     },
     search : function() {
        var $this =  $.extend({}, this, methods);
        if ($this.searching && 0) return;
        if ($this.n.text.val().length<$this.o.charcount) return;
        $this.searching = true;
        $this.n.proloading.css({
           visibility: "visible"
        });
        $this.hideSettings();
        $this.hideResults();
        var data = {
      	  action: 'ajaxsearchpro_search',
          aspp: $this.n.text.val(),
          asid: $this.o.id,
          options: $('form' ,$this.n.searchsettings).serialize()  
      	};
      	$.post(ajaxsearchpro.ajaxurl, data, function(response) {
          $this.n.resdrg.html("");
          if (response.error!=null) {
            $this.n.resdrg.append("<div class='nores error'>"+response.error+"</div>");
          } else if (response.nores!=null && response.keywords!=null) {
            var str = $this.o.noresultstext+" "+$this.o.didyoumeantext+"<br>";
            for(var i=0;i<response.keywords.length;i++) {
              str = str + "<span class='keyword'>"+response.keywords[i]+"</span>";
            }
            $this.n.resdrg.append("<div class='nores'>"+str+"</div>");
            $(".keyword", $this.n.resdrg).bind('click', function(){
               $this.n.text.val($(this).html());
               $this.n.promagnifier.trigger('click');
            });
            if ($this.n.showmore!=null) {
              $this.n.showmore.css({
                 'display': 'none'
              });  
            }
          } else if (response.length>0 || response.grouped!=null) {
              if (response.grouped!=null) {
                $.each(response.items, function(){
                    group = $this.createGroup(this.name);
                    group = $(group);
                    $this.n.resdrg.append(group);
                    $.each(this.data, function(){
                      result = $this.createResult(this);
                      result = $(result);
                      $this.n.resdrg.append(result);
                    });
                });
              } else {
                for(var i=0;i<response.length;i++) {
                    result = $this.createResult(response[i]);
                    result = $(result);
                    $this.n.resdrg.append(result);
                }
              }
              if ($this.n.showmore!=null) {
                $this.n.showmore.css({
                   'display': 'inline-block'
                });  
              }
          } else {
            $this.n.resdrg.append("<div class='nores'>"+$this.o.noresultstext+"</div>");
            if ($this.n.showmore!=null) {
              $this.n.showmore.css({
                 'display': 'none'
              });  
            }
          }
          $this.n.items = $('.item', $this.n.resultsDiv);
          $this.showResults();
          $this.n.proloading.css({
             visibility: "hidden"
          });
          if ($this.n.showmore!=null) {
             $this.n.showmore.attr('href', $this.o.homeurl+'?s='+$this.n.text.val());
          }  
      	}, "json"); 
     },

     createResult : function(r) {
        var $this =  $.extend({}, this, methods);
        var imageDiv = "";
        var desc = "";
        var authorSpan = "";
        var dateSpan = "";
        if (r.image!=null && r.image!="") {
          imageDiv = "\
               <div class='image' style='width:"+$this.o.imagewidth+"px;height:"+$this.o.imageheight+"px;'>\
                <img src='"+r.image+"'> \
                </div>";
        }
        if ($this.o.showauthor==1) {
          authorSpan = "<span class='author'>"+r.author+"</span>"; 
        }
        if ($this.o.showdate==1) {
          dateSpan = "<span class='date'>"+r.date+"</span>";
        }
        if ($this.o.showdescription==1) {
          desc = r.content;
        }                
        var id = 'item_'+$this.o.id;
        var clickable = ""
        if ($this.o.resultareaclickable==1) {
          clickable = "<span class='overlap'></span>";
        }
        var result = "\
             <div class='item' id='"+id+"' style='height:"+$this.o.resultitemheight+"px;'> \
              "+imageDiv+" \
              <div class='content' style='height:"+$this.o.resultitemheight+"px;'> \
                <h3><a href='"+r.link+"'>"+r.title+clickable+"</a></h3>   \
                        <div class='etc'>"+authorSpan+" \
                        "+dateSpan+"</div> \
                <p class='desc'>"+desc+"</p>        \
              </div>     \
              <div class='clear'></div>   \
             </div>";
        return result;
     },
     
     createGroup: function(r) {
        var $this =  $.extend({}, this, methods);
        return "<div class='group'>"+r+"</div>";
     },     
     
     showResults : function( ) {
        var $this =  $.extend({}, this, methods); 
        $this.scrolling(true);
        var top = $this.n.resultsDiv.position().top;
        $this.n.resultsDiv.css({
           "top": top-100,
           opacity: 0,
           visibility: "visible"
        }).animate({
           "top": top,
           opacity: 1
        },{
          complete: function() {
            $(".mCS_no_scrollbar", $this.resultsDiv).css({
              "top":0
            });
          }
        });
        if ($this.n.items.length>0) {
          var count = (($this.n.items.length<$this.o.itemscount)?$this.n.items.length:$this.o.itemscount);
          var groups = $('.group', $this.n.resultsDiv);
          var i = 0;
          var h = 0;
          $this.n.items.each(function(){
              if (i<count) h += $(this).outerHeight(true);
              i++;
          });
          if (groups.length>0) {
             h += groups.outerHeight(true);
          }
          $this.n.results.css({
            height: h+3
          });
          $this.scroll.mCustomScrollbar('update');
          if ($this.o.highlight==1) {
            var wholew = (($this.o.highlightwholewords==1)?true:false);
            $("div.item", $this.n.resultsDiv).highlight($this.n.text.val().split(" "), { element: 'span', className: 'highlighted', wordsOnly: wholew });
          }
           
        } 
        $this.resize(); 
        if ($this.n.items.length==0) {
          var h = ($('.nores', $this.n.results).outerHeight(true)>($this.o.resultitemheight)?($this.o.resultitemheight):$('.nores', $this.n.results).outerHeight(true));
          $this.n.results.css({
            height: 11110
          }); 
          $this.scroll.mCustomScrollbar('update');
          $this.n.results.css({
            height: $('.nores', $this.n.results).outerHeight(true)
          }); 
        }

        $this.scrolling(true);
        $this.searching = false;
        
     },
     hideResults : function( ) {
        var $this =  $.extend({}, this, methods);
        $this.n.resultsDiv
        .animate({
           opacity: 0
        },{
          complete: function() {
            $(this).css({
              visibility: "hidden"
            });
          }
        });
     },
     showSettings : function( ) {
       var $this =  $.extend({}, this, methods);
       $this.scrolling(true);
       $this.n.searchsettings.css({
        opacity: 0,
        visibility: "visible",
        top: "-=50px"
       });
       $this.n.searchsettings.animate({
        opacity: 1,
        top: "+=50px"
       });
       $this.n.prosettings.attr('opened', 1);       
     },   
     hideSettings : function( ) {
       var $this =  $.extend({}, this, methods);
       $this.n.searchsettings.animate({
        opacity: 0
       }, {
        complete: function() {
          $(this).css({
            visibility: "hidden"
          });
        }
       });
       $this.n.prosettings.attr('opened', 0);
     },
     cleanUp: function( ) {
        var $this =  $.extend({}, this, methods);
        if ($('.searchsettings', $this.n.container).length>0) {
               $('body>#ajaxsearchprosettings'+$this.o.id).remove();
               $('body>#ajaxsearchprores'+$this.o.id).remove();
        }
     },  
     resize : function( ) {
       var $this =  $.extend({}, this, methods);    
       $this.n.proinput.css({
          width: ($this.n.probox.width()-8-($this.n.proinput.outerWidth(false)-$this.n.proinput.width())-$this.n.proloading.outerWidth(true)-$this.n.prosettings.outerWidth(true)-$this.n.promagnifier.outerWidth(true)-10)
       });
       $this.n.text.css({
          width: $this.n.proinput.width()-2+$this.n.proloading.outerWidth(true),
          position: 'absolute',
          zIndex: 2
       });
       $this.n.textAutocomplete.css({
          width: $this.n.proinput.width()-2+$this.n.proloading.outerWidth(true),
          position: 'absolute',
          top: $this.n.text.position().top,
          left: $this.n.text.position().left, 
          opacity: 0.25,
          zIndex: 1
       });
       if ($this.n.prosettings.attr('opened')!=0) {
         $this.n.searchsettings.css({
           display: "block",
           top: $this.n.prosettings.offset().top+$this.n.prosettings.height()-2,
           left: $this.n.prosettings.offset().left+$this.n.prosettings.width()-$this.n.searchsettings.width() 
         });
       }
       if ($this.n.resultsDiv.css('visibility')!='hidden') {
         $this.n.resultsDiv.css({
            width: $this.n.container.width()-($this.n.resultsDiv.outerWidth(true)-$this.n.resultsDiv.width()),
            top: $this.n.container.offset().top+$this.n.container.outerHeight(true)+10,
            left: $this.n.container.offset().left
         }); 
         
         $('.content', $this.n.items).each(function(){
            var imageWidth = (($(this).prev().css('display')=="none")?0:$(this).prev().outerWidth(true));
            $(this).css({
             width: ($(this.parentNode).width()-$(this).prev().outerWidth(true)-$(this).outerWidth(false) + $(this).width())
            }); 
         });
        }   
     },
     scrolling: function(ignoreVisibility) {
       var $this =  $.extend({}, this, methods);
       if (ignoreVisibility==true || $this.n.searchsettings.css('visibility')=='visible') {
         $this.n.searchsettings.css({
           display: "block",
           top: $this.n.prosettings.offset().top+$this.n.prosettings.height()-2,
           left: $this.n.prosettings.offset().left+$this.n.prosettings.width()-$this.n.searchsettings.width() 
         });
       }
       if (ignoreVisibility==true || $this.n.resultsDiv.css('visibility')=='visible') {
         $this.n.resultsDiv.css({
            width: $this.n.container.width()-($this.n.resultsDiv.outerWidth(true)-$this.n.resultsDiv.width()),
            top: $this.n.container.offset().top+$this.n.container.outerHeight(true)+10,
            left: $this.n.container.offset().left
         }); 
         $('.content', $this.n.items).each(function(){
            var imageWidth = (($(this).prev().css('display')=="none")?0:$(this).prev().outerWidth(true));
            $(this).css({
             width: ($(this.parentNode).width()-$(this).prev().outerWidth(true)-$(this).outerWidth(false) + $(this).width())
            }); 
         });
       }
     }
  };

  $.fn.ajaxsearchpro = function( method ) {
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.ajaxsearchpro' );
    }    
  
  };
	function is_touch_device(){
		return !!("ontouchstart" in window) ? 1 : 0;
	}
})( aspjQuery );
