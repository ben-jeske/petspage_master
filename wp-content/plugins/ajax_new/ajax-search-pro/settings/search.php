<?php
  global $sliders;
  global $wpdb;
  //include "types.class.php";
  $params = array();
  /* General Options */
  $_search_default['triggeronclick'] = 1;
  $_search_default['redirectonclick'] = 0;
  $_search_default['triggerontype'] = 1;
  $_search_default['searchinposts'] =  1;
  $_search_default['searchinpages'] =  1;
  $_search_default['searchinproducts'] =  1;
  $_search_default['searchintitle'] =  1;
  $_search_default['searchincontent'] =  1;
  $_search_default['searchinexcerpt'] =  1;
  $_search_default['customfields'] = "";
  $_search_default['searchinbpusers'] =  1;
  $_search_default['searchinbpgroups'] =  1;
  $_search_default['searchinbpforums'] =  1;
  $_search_default['searchindrafts'] =  0;
  $_search_default['searchinpending'] =  0;
  $_search_default['exactonly'] =  0;
  $_search_default['searchinterms'] =  0;
  $_search_default['keywordsuggestions'] = 1;
  $_search_default['keywordsuggestionslang'] = "en";
  $_search_default['charcount'] =  3;
  $_search_default['maxresults'] =  30;
  $_search_default['itemscount'] =  4;
  $_search_default['resultitemheight'] =  70;
  $_search_default['imagesettings'] = 'show:1;cache:1;featured:0;content:1;excerpt:2;imagenum:1;width:70;height:70;';
  $_search_default['settings-imagesettings'] = array();
  $_search_default['settings-imagesettings']['show'] = 1;
  $_search_default['settings-imagesettings']['cache'] = 1;
  $_search_default['settings-imagesettings']['width'] = 70;
  $_search_default['settings-imagesettings']['height'] = 70;
  $_search_default['settings-imagesettings']['imagenum'] = 1;
  $_search_default['settings-imagesettings']['from'] = array(
    0=>"featured",
    1=>"content",
    2=>"excerpt"
  ); 
  $_search_default['imagebg'] = "#ffffff";
  $_search_default['orderby'] = "
     Title descending|post_title DESC;
     Title ascending|post_title ASC;
     Date descending|post_date DESC;
     Date ascending|post_date ASC||
     post_date DESC
  ";
  
  /* Multisite Options */
  $_search_default['searchinblogtitles'] = 0;
  $_search_default['blogresultstext'] = "Blogs";

  /* Frontend search settings Options */
  $_search_default['showexactmatches'] = 1;
  $_search_default['showsearchinposts'] = 1;
  $_search_default['showsearchinpages'] = 1;
  $_search_default['showsearchintitle'] = 1;
  $_search_default['showsearchincontent'] = 1;
  $_search_default['showsearchincomments'] = 1;
  $_search_default['showsearchinexcerpt'] = 1;
  $_search_default['showsearchinbpusers'] = 0;
  $_search_default['showsearchinbpgroups'] = 0;
  $_search_default['showsearchinbpforums'] = 0;
  
  $_search_default['exactmatchestext'] = "Exact matches only";
  $_search_default['searchinpoststext'] = "Search in posts";
  $_search_default['searchinpagestext'] = "Search in pages";
  $_search_default['searchintitletext'] = "Search in title";
  $_search_default['searchincontenttext'] = "Search in content";
  $_search_default['searchincommentstext'] = "Search in comments";
  $_search_default['searchinexcerpttext'] = "Search in excerpt";
  $_search_default['searchinbpuserstext'] = "Search in users";
  $_search_default['searchinbpgroupstext'] = "Search in groups";
  $_search_default['searchinbpforumstext'] = "Search in forums";
  
  $_search_default['showsearchincategories'] = 1;
  $_search_default['exsearchincategories'] = "";
  $_search_default['exsearchincategoriesheight'] = 200;
  $_search_default['exsearchincategoriestext'] = "Filter by Categories";
  $_search_default['exsearchincategoriestextfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#055e94;font-size:12px;line-height:15px;';                    
  
  /* Layout Options */
  $_search_default['defaultsearchtext'] = '';
  $_search_default['showmoreresults'] = 0;
  $_search_default['showmoreresultstext'] = 'More results...';
  $_search_default['showmorefont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#055e94;font-size:12px;line-height:15px;';
  $_search_default['resultareaclickable'] = 0;
  $_search_default['showauthor'] = 1;
  $_search_default['showdate'] = 1;
  $_search_default['showdescription'] = 1;
  $_search_default['descriptionlength'] = 100;
  $_search_default['noresultstext'] = "No results!";
  $_search_default['didyoumeantext'] = "Did you mean:";
  $_search_default['highlight'] = 1;
  $_search_default['highlightwholewords'] = 1;
  $_search_default['highlightcolor'] = "#d9312b";
  $_search_default['highlightbgcolor'] = "#eee";
  
  /* Autocomplete options */
  $_search_default['autocomplete'] = 1;
  $_search_default['autocompleteexceptions'] = '';
  $_search_default['autocompletesource'] = "
     Search Statistics|0;
     Google Keywords|1
     ||1
  ";  
  
  /* Advanced Options */
  $_search_default['titlefield'] = array(
     array('option'=>'Post Title', 'value'=>0),
     array('option'=>'Post Excerpt', 'value'=>1)
  );
  $_search_default['titlefield_def'] = 0;

  $_search_default['descriptionfield'] = array(
     array('option'=>'Post Description', 'value'=>0),
     array('option'=>'Post Excerpt', 'value'=>1),
     array('option'=>'Post Title', 'value'=>2)
  );
  $_search_default['descriptionfield_def'] = 0;
    
  $_search_default['excludecategories'] = '';
  $_search_default['excludeposts'] = '';
  $_search_default['groupby'] = "
     No grouping|0;
     Group By Categories|1;
     Groub By Post Type|2
     ||0
  ";   
  $_search_default['groupbytext'] = "Posts from: %GROUP%";
  $_search_default['bbpressreplytext'] = "BBPress reply Results";
  $_search_default['bbpressgroupstext'] = "BBPress groups Results";
  $_search_default['bbpressuserstext'] = "BBPress  user Results";
  $_search_default['commentstext'] = "Comment Results";
  $_search_default['uncategorizedtext'] = "Other Results";
  $_search_default['showpostnumber'] = 1; 
  
  /* Theme options */
  $_search_default['boxbackground'] = '#c1ecf0';
  $_search_default['boxborder'] = 'border:0px none #000000;border-radius:5px 5px 5px 5px;';
  $_search_default['boxshadow'] = 'box-shadow:0px 0px 1px 1px #d2dbd9 ;';
  $_search_default['inputbackground'] = '#ffffff';
  $_search_default['inputborder'] = 'border:1px solid #ffffff;border-radius:3px 3px 3px 3px;';
  $_search_default['inputshadow'] = 'box-shadow:1px 0px 3px 0px #ccc inset;';
  $_search_default['inputfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#212121;font-size:12px;line-height:15px;';
  $_search_default['settingsbackground'] = '#ddd';
  $_search_default['settingsbackground'] = '#ddd';
  $_search_default['settingsdropbackground'] = '#ddd';
  $_search_default['settingsdropbackgroundfontcolor'] = '#333';
   
  $_search_default['resultsborder'] = 'border:0px none #000000;border-radius:3px 3px 3px 3px;';
  $_search_default['resultshadow'] = 'box-shadow:0px 0px 0px 0px #000000 ;'; 
  $_search_default['resultsbackground'] = '#c1ecf0';
  $_search_default['resultscontainerbackground'] = '#ebebeb';
  $_search_default['resultscontentbackground'] = '#ffffff';
  $_search_default['titlefont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#1454a9;font-size:14px;line-height:15px;';
  $_search_default['titlehoverfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#1454a9;font-size:14px;line-height:15px;';
  $_search_default['authorfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#a1a1a1;font-size:12px;line-height:15px;';
  $_search_default['datefont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#adadad;font-size:12px;line-height:15px;';
  $_search_default['descfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#4a4a4a;font-size:13px;line-height:15px;';
  $_search_default['groupfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#111111;font-size:13px;line-height:15px;';
  $_search_default['arrowcolor'] = '#0a3f4d';
  $_search_default['overflowcolor'] = '#ffffff';  
  
  $_search_default['magnifierimage'] = "
    /ajax-search-pro/img/magnifiers/magn1.png;
    /ajax-search-pro/img/magnifiers/magn2.png;
    /ajax-search-pro/img/magnifiers/magn3.png;
    /ajax-search-pro/img/magnifiers/magn4.png;
    /ajax-search-pro/img/magnifiers/magn5.png;
    /ajax-search-pro/img/magnifiers/magn6.png;
    /ajax-search-pro/img/magnifiers/magn7.png;
    /ajax-search-pro/img/magnifiers/magn8.png||
    /ajax-search-pro/img/magnifiers/magn3.png
  ";
  $_search_default['selected-magnifierimage'] = "/ajax-search-pro/img/magnifiers/magn3.png";
  $_search_default['settingsimage'] = "
    /ajax-search-pro/img/settings/settings1.png;
    /ajax-search-pro/img/settings/settings2.png;
    /ajax-search-pro/img/settings/settings3.png;
    /ajax-search-pro/img/settings/settings4.png;
    /ajax-search-pro/img/settings/settings5.png;
    /ajax-search-pro/img/settings/settings6.png;
    /ajax-search-pro/img/settings/settings7.png;
    /ajax-search-pro/img/settings/settings8.png;
    /ajax-search-pro/img/settings/settings9.png;
    /ajax-search-pro/img/settings/settings10.png||
    /ajax-search-pro/img/settings/settings1.png
  ";
  $_search_default['selected-settingsimage'] = "/ajax-search-pro/img/settings/settings1.png";
  $_search_default['loadingimage'] = "
    /ajax-search-pro/img/loading/loading1.gif;
    /ajax-search-pro/img/loading/loading2.gif;
    /ajax-search-pro/img/loading/loading3.gif;
    /ajax-search-pro/img/loading/loading4.gif;
    /ajax-search-pro/img/loading/loading5.gif;
    /ajax-search-pro/img/loading/loading6.gif;
    /ajax-search-pro/img/loading/loading7.gif;
    /ajax-search-pro/img/loading/loading8.gif;
    /ajax-search-pro/img/loading/loading9.gif;
    /ajax-search-pro/img/loading/loading10.gif;
    /ajax-search-pro/img/loading/loading11.gif||
    /ajax-search-pro/img/loading/loading3.gif
  ";
  $_search_default['selected-loadingimage'] = "/ajax-search-pro/img/loading/loading3.gif";
  $_search_default['magnifierbackground'] = "#eeeeee";   
  $_search_default['showauthor'] = 1;
  $_search_default['showdate'] = 1;
  $_search_default['showdescription'] = 1;
  $_search_default['descriptionlength'] = 130;
  $_search_default['groupbytextfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#055E94;font-size:11px;line-height:13px;';
  $_search_default['exsearchincategoriesboxcolor'] = "#EFEFEF";
  
  $_themes = file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'themes.json');
  
  if (isset($wpdb->base_prefix)) {
    $_prefix = $wpdb->base_prefix;
  } else {
    $_prefix = $wpdb->prefix;
  }
  
  $_comp = wpdreamsCompatibility::Instance();
  ?>
  
  <?php if ($_comp->has_errors()): ?>
  <div class="wpdreams-slider errorbox">
          <p class='errors'>Possible incompatibility! Please go to the <a href="<?php echo get_admin_url()."admin.php?page=ajax-search-pro/comp_check.php"; ?>">error check</a> page to see the details and solutions!</p> 
  </div>
  <?php endif; ?>  
  
  <div style="padding:20px;position:fixed;z-index:1000;background:#FFF;bottom:0;right:20px;border:1px solid #ccc;width:400px;height:500px;" id='preview'>
    Preview: <input type="button" value="Refresh" name='refresh' searchid='0' />
    <input type="button" value="Hide" name='hide' />
    <div class='big-loading hidden'></div>
    <div class="data hidden">
    </div>
  </div>
  <script>
  jQuery(document).ready(function() {
    (function( $ ){
       $('#wpdreams .settings').click(function(){
          $("#preview input[name=refresh]").attr('searchid', $(this).attr('searchid'));
       });
       $("select[id^=wpdreamsThemeChooser]").change(function(){
         $("#preview input[name=refresh]").click();
       });
       $("#preview input[name=refresh]").click(function(){
          $this =  $(this).parent();
          var id = $(this).attr('searchid');
          var loading = $('.big-loading', $this);
          if (id==0) {
             $('.data', $this).removeClass('hidden');
             $('.data', $this).html('Open one of the search forms settings to select the search form for preview!');
             return;
          }
          $('.data', $this).html("");
          $('.data', $this).addClass('hidden');
          loading.removeClass('hidden');
          var data = {
        	  action: 'ajaxsearchpro_preview',
            id: id,
            formdata: $('form[name="polaroid_slider_'+id+'"]').serialize()
        	};
        	jQuery.post(ajaxurl, data, function(response) {
              loading.addClass('hidden');
              $('.data', $this).html(response);
              $('.data', $this).removeClass('hidden');
              setTimeout(
                function(){
                  $(window).resize();
                  $(window).scroll();
                },
              1000); 		
        	}); 
       });
       $("#preview input[name=hide]").click(function(){
          $this = $(this.parentNode);
          if (parseInt($this.css('bottom'))<0) {
            $this.animate({
              bottom: "0px"
            });
            $(this).val('Hide');
            $("#preview input[name=refresh]").trigger('click');
          } else {
            $this.animate({
              bottom: "-470px"
            });
            $(this).val('Show');
          }
       });
      $("#preview input[name=hide]").trigger('click');
      
      var _self = new Object();
      
      $('.wpdreams-slider.moveable .settings').click(function(event, ignoreHeight){
         var moveable = this.parentNode.parentNode;
         var id = $(this).attr('searchid')
         if ($('.errorMsg', moveable).length) return;
         if (ignoreHeight!=null) {
            $(moveable).css({
              height: 'auto'
            });
            return; 
         }
         if (_self.sliderHeight==null) {
           _self.sliderHeight = $(moveable).height();
         }  
         if ($(moveable).height()<27) {
            var data = {
          	  action: 'ajaxsearchpro_searchsettings',
              searchid: id
          	};
            $('form[name="polaroid_slider_'+id+'"]', moveable).html('<div class="big-loading"></div>');
              $(moveable).css({
                height: 'auto'
              }); 
          	jQuery.post(ajaxsearchpro.ajaxurl, data, function(response) {
              $('form[name="polaroid_slider_'+id+'"]', moveable).html(response);
              $(moveable).css({
                height: 'auto'
              }); 
              $($('.tabs a')[0], moveable).trigger('click');
            });
         } else {
            $(moveable).css({
              height: "26px"
            });      
        }
      });
      $('.wpdreams-slider.moveable .settings').trigger("click");
    }(jQuery));
  });
  </script>

  <div class="wpdreams-slider">
     <form name="add-slider" action="" method="POST">
       <fieldset>
       <legend>Add a new Search!</legend>
           <?php
            $new_slider = new wpdreamsText("addsearch", "Search form name:", "",array( array("func"=>"isEmpty", "op"=>"eq", "val"=>false) ), "Please enter a valid form name!");
           ?>
       <input name="submit" type="submit" value="Add" /> 
          <?php
            if (isset($_POST['addsearch']) && !$new_slider->getError()) {
              $wpdb->query("INSERT INTO ".$_prefix."ajaxsearchpro
                (name, data) VALUES
                ('".$_POST['addsearch']."', '".mysql_real_escape_string(json_encode($_search_default))."')
              ");
              echo "<div class='successMsg'>Search Form Successfuly added!</div>";
            }          
          ?> 
       </fieldset>  
     </form>     
  </div>
 
  <?php
  
  if (isset($_POST['delete'])) {
    $wpdb->query("DELETE FROM ".$_prefix."ajaxsearchpro WHERE id=".$_POST['did']);
  }
    
 
  $searchforms = $wpdb->get_results("SELECT * FROM ".$_prefix."ajaxsearchpro", ARRAY_A);
  if (is_array($searchforms))
  foreach ($searchforms as $search) {
      $search['data'] = json_decode($search['data'], true);   
      
    ?>
      <div class="wpdreams-slider moveable">
        <div class="slider-info">
          <img title="Click on this icon for search settings!" src="<?php echo plugins_url('/types/icons/settings.png', __FILE__) ?>" class="settings" searchid="<?php echo $search['id']; ?>" />
          <img title="Click here if you want to delete this search!" src="<?php echo plugins_url('/types/icons/delete.png', __FILE__) ?>" class="delete" />

          <form name="polaroid_slider_del_<?php echo $search['id']; ?>" action="" style="display:none;" method="POST">
            <?php 
            new wpdreamsHidden("z", "z", time());
            new wpdreamsHidden("delete", "delete", "delete"); 
            new wpdreamsHidden("did", "did", $search['id']);
            ?>        
          </form>      
          <span><?php
             echo $search['name'];
          ?>
          </span>
          <span>
             <label class="shortcode">Search shortcode:</label>
             <input type="text" class="shortcode" value="[wpdreams_ajaxsearchpro id=<?php echo $search['id']; ?>]" readonly="readonly" />
             <label class="shortcode">Search shortcode for templates:</label>
             <input type="text" class="shortcode" value="echo do_shortcode('[wpdreams_ajaxsearchpro id=<?php echo $search['id']; ?>]');" readonly="readonly" />
          </span>
        </div>
        <hr />

        
        <form name="polaroid_slider_<?php echo $search['id']; ?>" action="" method="POST">

        </form>
        
      </div>
    <?php
    
    if (isset($_POST['submit_'.$search['id']])) {
      /* update data */
      foreach ($_POST as $k=>$v) {
        $_tmp = explode("_".$search['id'], $k);
        $params[$_tmp[0]] = $v;
        //unset($params[$k]);
      }
      foreach ($params as $k=>$v) {
        $_tmp = explode('classname-', $k);
        if ($_tmp!=null && count($_tmp)>1) {
          ob_start();
          $c = new $v('0', '0', $params[$_tmp[1]]);
          $out = ob_get_clean();
          $params['selected-'.$_tmp[1]] = $c->getSelected();
        }
      }
      //print_r($params);
      $data = mysql_real_escape_string(json_encode($params));
      //print_r($_POST);
      $wpdb->query("
        UPDATE ".$_prefix."ajaxsearchpro 
        SET data = '".$data."'
        WHERE id = ".$search['id']."
      ");
      echo "<div class='successMsg'>Search settings saved!</div>";
    }
  } 
?>

