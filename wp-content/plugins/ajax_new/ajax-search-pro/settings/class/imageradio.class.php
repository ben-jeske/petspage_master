<?php
if (!class_exists("wpdreamsImageRadio")) {
  class wpdreamsImageRadio extends wpdreamsType {
  	function getType() {
  		parent::getType();
      $this->processData();
      echo "<div class='wpdreamsImageRadio'>";
  		echo "<span class='radioimage'>" . $this->label . "</span>";
      
      foreach ($this->selects as $radio) {
        $radio = trim($radio);
        echo "
          <img src='".plugins_url().$radio."' class='radioimage".(($radio==trim($this->selected))?' selected':'')."'/>
        ";
      }
  		echo "<input type='hidden' class='realvalue' value='" . $this->data . "' name='" . $this->name . "'>";
  		echo "<input type='hidden' value='wpdreamsImageRadio' name='classname-" . $this->name . "'>";
      echo "<div class='triggerer'></div>
      </div>";
  	}
  	function processData() {
  		//$this->data = str_replace("\n","",$this->data); 
  		$_temp          = explode("||", $this->data);
  		$this->selects  = explode(";", $_temp[0]);
  		$this->selected = trim($_temp[1]);
  	}
  	final function getData() {
  		return $this->data;
  	}
  	final function getSelected() {
  		return $this->selected;
  	}
  }
}
?>