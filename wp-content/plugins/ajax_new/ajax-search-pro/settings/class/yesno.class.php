<?php
if (!class_exists("wpdreamsYesNo")) {
  class wpdreamsYesNo extends wpdreamsType {
  	function getType() {
  		parent::getType();
      echo "<div class='wpdreamsYesNo'>";
  		echo "<label for='wpdreamstext_" . self::$_instancenumber . "'>" . $this->label . "</label>";
  		echo "<a class='wpdreamsyesno" . (($this->data == 1) ? " yes" : " no") . "' id='wpdreamsyesno_" . self::$_instancenumber . "' name='" . $this->name . "_yesno'>" . (($this->data == 1) ? "YES" : "NO") . "</a>";
  		echo "<input type='hidden' value='" . $this->data . "' name='" . $this->name . "'>";
  	  echo "<div class='triggerer'></div>
      </div>";
  	}
  }
}
?>