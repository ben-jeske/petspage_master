<?php
  /* Default options */
  $options = array();
  $options['asp_caching'] = 1;
  $options['asp_cachinginterval'] = 1440;
  $options['asp_precacheimages'] = 1;
  
  /* Save the defaul options if not exist */
  foreach($options as $key=>$value) {
     if (get_option($key)===false)
      update_option($key, $value);
  }
?>