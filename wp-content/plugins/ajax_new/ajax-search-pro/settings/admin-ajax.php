<?php
  add_action('wp_ajax_ajaxsearchpro_deletekeyword', 'ajaxsearchpro_deletekeyword');
  function ajaxsearchpro_deletekeyword() {
    global $wpdb;
    if (isset($wpdb->base_prefix)) {
      $_prefix = $wpdb->base_prefix;
    } else {
      $_prefix = $wpdb->prefix;
    }
    if (isset($_POST['keywordid'])) {
      $wpdb->query("DELETE FROM ".$_prefix."ajaxsearchpro_statistics WHERE id=".$_POST['keywordid']);
      print 1;
    }
    die();
  } 
  

  add_action('wp_ajax_ajaxsearchpro_searchsettings', 'ajaxsearchpro_searchsettings');
  function ajaxsearchpro_searchsettings() {
    global $wpdb;
    $params = array();
    /* General Options */
    $_search_default['triggeronclick'] = 1;
    $_search_default['redirectonclick'] = 0;
    $_search_default['triggerontype'] = 1;
    $_search_default['searchinposts'] =  1;
    $_search_default['searchinpages'] =  1;
    $_search_default['searchinproducts'] =  1;
    $_search_default['searchintitle'] =  1;
    $_search_default['searchincontent'] =  1;
    $_search_default['searchinexcerpt'] =  1;
    $_search_default['customfields'] = "";
    
    /*Buddypress*/
    $_search_default['searchinbpusers'] =  1;
    $_search_default['searchinbpgroups'] =  1;
    $_search_default['searchinbpprivategroups'] =  1;
    $_search_default['searchinbpforums'] =  1;
    $_search_default['bpgroupstitle'] = "
       Topic title|bb_topics.topic_title as title;
       Post Content|bb_posts.post_text as title||
       bb_topics.topic_title as title
    ";
    /*End Buddypress*/
        
    $_search_default['searchindrafts'] =  0;
    $_search_default['searchinpending'] =  0;
    $_search_default['exactonly'] =  0;
    $_search_default['searchinterms'] =  0;
    $_search_default['keywordsuggestions'] = 1;
    $_search_default['keywordsuggestionslang'] = "en";
    $_search_default['charcount'] =  3;
    $_search_default['maxresults'] =  30;
    $_search_default['itemscount'] =  4;
    $_search_default['resultitemheight'] =  70;
    $_search_default['imagesettings'] = 'show:1;cache:1;featured:0;content:1;excerpt:2;imagenum:1;width:70;height:70;';
    $_search_default['settings-imagesettings'] = array();
    $_search_default['settings-imagesettings']['show'] = 1;
    $_search_default['settings-imagesettings']['cache'] = 1;
    $_search_default['settings-imagesettings']['width'] = 70;
    $_search_default['settings-imagesettings']['height'] = 70;
    $_search_default['settings-imagesettings']['imagenum'] = 1;
    $_search_default['settings-imagesettings']['from'] = array(
      0=>"featured",
      1=>"content",
      2=>"excerpt"
    ); 
    $_search_default['imagebg'] = "#ffffff";
    $_search_default['orderby'] = "
       Title descending|post_title DESC;
       Title ascending|post_title ASC;
       Date descending|post_date DESC;
       Date ascending|post_date ASC||
       post_date DESC
    ";
  
    /* Frontend search settings Options */
    $_search_default['showexactmatches'] = 1;
    $_search_default['showsearchinposts'] = 1;
    $_search_default['showsearchinpages'] = 1;
    $_search_default['showsearchintitle'] = 1;
    $_search_default['showsearchincontent'] = 1;
    $_search_default['showsearchincomments'] = 1;
    $_search_default['showsearchinexcerpt'] = 1;
    $_search_default['showsearchinbpusers'] = 0;
    $_search_default['showsearchinbpgroups'] = 0;
    $_search_default['showsearchinbpforums'] = 0;
    
    $_search_default['exactmatchestext'] = "Exact matches only";
    $_search_default['searchinpoststext'] = "Search in posts";
    $_search_default['searchinpagestext'] = "Search in pages";
    $_search_default['searchintitletext'] = "Search in title";
    $_search_default['searchincontenttext'] = "Search in content";
    $_search_default['searchincommentstext'] = "Search in comments";
    $_search_default['searchinexcerpttext'] = "Search in excerpt";
    $_search_default['searchinbpuserstext'] = "Search in users";
    $_search_default['searchinbpgroupstext'] = "Search in groups";
    $_search_default['searchinbpforumstext'] = "Search in forums";
    
    $_search_default['showsearchincategories'] = 1;
    $_search_default['exsearchincategories'] = "";
    $_search_default['exsearchincategoriesheight'] = 200;
    $_search_default['exsearchincategoriestext'] = "Filter by Categories";
    $_search_default['exsearchincategoriestextfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#055e94;font-size:12px;line-height:15px;';                    
    
    /* Layout Options */
    $_search_default['defaultsearchtext'] = '';
    $_search_default['showmoreresults'] = 0;
    $_search_default['showmoreresultstext'] = 'More results...';
    $_search_default['showmorefont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#055e94;font-size:12px;line-height:15px;';
    $_search_default['resultareaclickable'] = 0;
    $_search_default['showauthor'] = 1;
    $_search_default['showdate'] = 1;
    $_search_default['showdescription'] = 1;
    $_search_default['descriptionlength'] = 100;
    $_search_default['noresultstext'] = "No results!";
    $_search_default['didyoumeantext'] = "Did you mean:";
    $_search_default['highlight'] = 1;
    $_search_default['highlightwholewords'] = 1;
    $_search_default['highlightcolor'] = "#d9312b";
    $_search_default['highlightbgcolor'] = "#eee";
    
    /* Multisite Options */
    $_search_default['searchinblogtitles'] = 0;
    $_search_default['blogresultstext'] = "Blogs";
    $_search_default['blogtitleorderby'] = "
       Title descending|desc;
       Title ascending|asc
       ||asc
    ";
    
    /* Autocomplete options */
    $_search_default['autocomplete'] = 1;
    $_search_default['autocompleteexceptions'] = '';
    $_search_default['autocompletesource'] = "
       Search Statistics|0;
       Google Keywords|1
       ||1
    ";  
    
    /* Advanced Options */
    $_search_default['runshortcode'] = 1;
    $_search_default['striptagsexclude'] = "<a><span>";
    
    $_search_default['titlefield'] = array(
       array('option'=>'Post Title', 'value'=>0),
       array('option'=>'Post Excerpt', 'value'=>1)
    );
    $_search_default['titlefield_def'] = 0;

    $_search_default['descriptionfield'] = array(
       array('option'=>'Post Description', 'value'=>0),
       array('option'=>'Post Excerpt', 'value'=>1),
       array('option'=>'Post Title', 'value'=>2)
    );
    $_search_default['descriptionfield_def'] = 0;
    
    $_search_default['excludecategories'] = '';
    $_search_default['excludeposts'] = '';
    $_search_default['groupby'] = "
       No grouping|0;
       Group By Categories|1;
       Groub By Post Type|2
       ||0
    ";   
    $_search_default['groupbytext'] = "Posts from: %GROUP%";
    $_search_default['bbpressreplytext'] = "BBPress reply Results";
    $_search_default['bbpressgroupstext'] = "BBPress groups Results";
    $_search_default['bbpressuserstext'] = "BBPress  user Results";
    $_search_default['commentstext'] = "Comment Results";
    $_search_default['uncategorizedtext'] = "Other Results";
    $_search_default['showpostnumber'] = 1;    
    
    /* Theme options */
    $_search_default['boxbackground'] = '#c1ecf0';
    $_search_default['boxborder'] = 'border:0px none #000000;border-radius:5px 5px 5px 5px;';
    $_search_default['boxshadow'] = 'box-shadow:0px 0px 1px 1px #d2dbd9 ;';
    $_search_default['inputbackground'] = '#ffffff';
    $_search_default['inputborder'] = 'border:1px solid #ffffff;border-radius:3px 3px 3px 3px;';
    $_search_default['inputshadow'] = 'box-shadow:1px 0px 3px 0px #ccc inset;';
    $_search_default['inputfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#212121;font-size:12px;line-height:15px;';
    $_search_default['settingsbackground'] = '#ddd';
    $_search_default['settingsbackground'] = '#ddd';
    $_search_default['settingsdropbackground'] = '#ddd';
    $_search_default['settingsdropbackgroundfontcolor'] = '#333';
     
    $_search_default['resultsborder'] = 'border:0px none #000000;border-radius:3px 3px 3px 3px;';
    $_search_default['resultshadow'] = 'box-shadow:0px 0px 0px 0px #000000 ;'; 
    $_search_default['resultsbackground'] = '#c1ecf0';
    $_search_default['resultscontainerbackground'] = '#ebebeb';
    $_search_default['resultscontentbackground'] = '#ffffff';
    $_search_default['titlefont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#1454a9;font-size:14px;line-height:15px;';
    $_search_default['titlehoverfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#1454a9;font-size:14px;line-height:15px;';
    $_search_default['authorfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#a1a1a1;font-size:12px;line-height:15px;';
    $_search_default['datefont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#adadad;font-size:12px;line-height:15px;';
    $_search_default['descfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#4a4a4a;font-size:13px;line-height:15px;';
    $_search_default['groupfont'] = 'font-weight:normal;font-family:\'Arial\', Helvetica, sans-serif;color:#111111;font-size:13px;line-height:15px;';
    $_search_default['arrowcolor'] = '#0a3f4d';
    $_search_default['overflowcolor'] = '#ffffff';  
    
    $_search_default['magnifierimage'] = "
      /ajax-search-pro/img/magnifiers/magn1.png;
      /ajax-search-pro/img/magnifiers/magn2.png;
      /ajax-search-pro/img/magnifiers/magn3.png;
      /ajax-search-pro/img/magnifiers/magn4.png;
      /ajax-search-pro/img/magnifiers/magn5.png;
      /ajax-search-pro/img/magnifiers/magn6.png;
      /ajax-search-pro/img/magnifiers/magn7.png;
      /ajax-search-pro/img/magnifiers/magn8.png||
      /ajax-search-pro/img/magnifiers/magn3.png
    ";
    $_search_default['selected-magnifierimage'] = "/ajax-search-pro/img/magnifiers/magn3.png";
    $_search_default['settingsimage'] = "
      /ajax-search-pro/img/settings/settings1.png;
      /ajax-search-pro/img/settings/settings2.png;
      /ajax-search-pro/img/settings/settings3.png;
      /ajax-search-pro/img/settings/settings4.png;
      /ajax-search-pro/img/settings/settings5.png;
      /ajax-search-pro/img/settings/settings6.png;
      /ajax-search-pro/img/settings/settings7.png;
      /ajax-search-pro/img/settings/settings8.png;
      /ajax-search-pro/img/settings/settings9.png;
      /ajax-search-pro/img/settings/settings10.png||
      /ajax-search-pro/img/settings/settings1.png
    ";
    $_search_default['selected-settingsimage'] = "/ajax-search-pro/img/settings/settings1.png";
    $_search_default['loadingimage'] = "
      /ajax-search-pro/img/loading/loading1.gif;
      /ajax-search-pro/img/loading/loading2.gif;
      /ajax-search-pro/img/loading/loading3.gif;
      /ajax-search-pro/img/loading/loading4.gif;
      /ajax-search-pro/img/loading/loading5.gif;
      /ajax-search-pro/img/loading/loading6.gif;
      /ajax-search-pro/img/loading/loading7.gif;
      /ajax-search-pro/img/loading/loading8.gif;
      /ajax-search-pro/img/loading/loading9.gif;
      /ajax-search-pro/img/loading/loading10.gif;
      /ajax-search-pro/img/loading/loading11.gif||
      /ajax-search-pro/img/loading/loading3.gif
    ";
    $_search_default['selected-loadingimage'] = "/ajax-search-pro/img/loading/loading3.gif";
    $_search_default['magnifierbackground'] = "#eeeeee";   
    $_search_default['showauthor'] = 1;
    $_search_default['showdate'] = 1;
    $_search_default['showdescription'] = 1;
    $_search_default['descriptionlength'] = 130;
    $_search_default['groupbytextfont'] = 'font-weight:bold;font-family:\'Arial\', Helvetica, sans-serif;color:#055E94;font-size:11px;line-height:13px;';
    $_search_default['exsearchincategoriesboxcolor'] = "#EFEFEF";
    
    $_themes = file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'themes.json');
    require_once('types.inc.php');
    if (isset($wpdb->base_prefix)) {
      $_prefix = $wpdb->base_prefix;
    } else {
      $_prefix = $wpdb->prefix;
    }    
    ob_start();
    $search = $wpdb->get_row("SELECT * FROM ".$_prefix."ajaxsearchpro WHERE id=".$_POST['searchid'], ARRAY_A);
    $search['data'] = json_decode($search['data'], true);
?>
        <script type='text/javascript' src='<?php echo plugin_dir_url(__FILE__) . '/types/upload.js'; ?>'></script>
        <script type='text/javascript' src='<?php echo plugin_dir_url(__FILE__) . '/types/tabs.js'; ?>'></script>
        <script type='text/javascript' src='<?php echo plugin_dir_url(__FILE__) . '/types/others.js'; ?>'></script>
        <script type='text/javascript' src='<?php echo plugin_dir_url(__FILE__) . '/types/fonts.js'; ?>'></script>
        <ul id="tabs" class='tabs'>
            <li><a tabid="1" class='current'>General Options</a></li>
            <li><a tabid="2">Multisite Options</a></li>
            <li><a tabid="3">Frontend Search Settings</a></li>
            <li><a tabid="4">Layout options</a></li>
            <li><a tabid="5">Autocomplete options</a></li>
            <li><a tabid="6">Theme options</a></li>
            <li><a tabid="7">Advanced options</a></li>     
        </ul>
        <div id="content" class='tabscontent'> 
          <div tabid="1">
          <fieldset>
          <legend>Genearal Options</legend>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchinposts_".$search['id'], "Search in posts?", $search['data']['searchinposts']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchinpages_".$search['id'], "Search in pages?", $search['data']['searchinpages']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php
              if (!isset($search['data']['customtypes'])) $search['data']['customtypes'] = $_search_default['customtypes']; 
              $o = new wpdreamsCustomPostTypes("customtypes_".$search['id'], "Search in custom post types", $search['data']['customtypes']);
              $params[$o->getName()] = $o->getData();
              $params['selected-'.$o->getName()] = $o->getSelected();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchintitle_".$search['id'], "Search in title?", $search['data']['searchintitle']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchincontent_".$search['id'], "Search in content?", $search['data']['searchincontent']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
             <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchincomments_".$search['id'], "Search in comments?", $search['data']['searchincomments']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchinexcerpt_".$search['id'], "Search in post excerpts?", $search['data']['searchinexcerpt']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php
              if (!isset($search['data']['customfields'])) $search['data']['customfields'] = $_search_default['customfields']; 
              $o = new wpdreamsCustomFields("customfields_".$search['id'], "Search in custom fields", $search['data']['customfields']);
              $params[$o->getName()] = $o->getData();
              $params['selected-'.$o->getName()] = $o->getSelected();
              ?></div>
              <div class="item">
              <fieldset>
                 <legend>BuddyPress Options</legend>
                  <p class='extra'>You must have BuddyPress installed to use these functions!</p>
                  <?php
                  $o = new wpdreamsYesNo("searchinbpusers_".$search['id'], "Search in BuddyPress users?", $search['data']['searchinbpusers']);
                  $params[$o->getName()] = $o->getData();
                  $o = new wpdreamsYesNo("searchinbpgroups_".$search['id'], "Search in BuddyPress groups?", $search['data']['searchinbpgroups']);
                  $params[$o->getName()] = $o->getData();
                  $o = new wpdreamsYesNo("searchinbpprivategroups_".$search['id'], "Search in BuddyPress private groups?", (($search['data']['searchinbpprivategroups']!="")?$search['data']['searchinbpprivategroups']:$_search_default['searchinbpprivategroups'] ));
                  $params[$o->getName()] = $o->getData();
                  $o = new wpdreamsYesNo("searchinbpforums_".$search['id'], "Search in BuddyPress forums?", $search['data']['searchinbpforums']);
                  $params[$o->getName()] = $o->getData();
                  $o = new wpdreamsSelect("bpgroupstitle_".$search['id'], "Result title", (($search['data']['bpgroupstitle']!="")?$search['data']['bpgroupstitle']:$_search_default['bpgroupstitle'] ));
                  $params[$o->getName()] = $o->getData();
                  $params["selected-".$o->getName()] = $o->getSelected();
                  ?>
              </fieldset>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchindrafts_".$search['id'], "Search in draft posts?", $search['data']['searchindrafts']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchinpending_".$search['id'], "Search in pending posts?", $search['data']['searchinpending']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("exactonly_".$search['id'], "Show exact matches only?", $search['data']['exactonly']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("searchinterms_".$search['id'], "Search in terms? (categories, tags)", $search['data']['searchinterms']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("keywordsuggestions_".$search['id'], "Keyword suggestions on no results?", $search['data']['keywordsuggestions']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>                                       
              <div class="item"><?php
              $o = new wpdreamsLanguageSelect("keywordsuggestionslang_".$search['id'], "Keyword suggestions language", (($search['data']['keywordsuggestionslang']!="")?$search['data']['keywordsuggestionslang']:$_search_default['keywordsuggestionslang'] ));
              $params[$o->getName()] = $o->getData();
              ?></div> 
              <div class="item"><?php
              $o = new wpdreamsSelect("orderby_".$search['id'], "Result ordering", (($search['data']['orderby']!="")?$search['data']['orderby']:$_search_default['orderby'] ));
              $params[$o->getName()] = $o->getData();
              $params["selected-".$o->getName()] = $o->getSelected();
              ?></div> 
              <div class="item">
              <?php
              if ($search['data']['triggeronclick']=="") $search['data']['triggeronclick'] = $_search_default['triggeronclick'];
              $o = new wpdreamsYesNo("triggeronclick_".$search['id'], "Trigger search when clicking on search icon?", $search['data']['triggeronclick']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              if ($search['data']['redirectonclick']=="") $search['data']['redirectonclick'] = $_search_default['redirectonclick'];
              $o = new wpdreamsYesNo("redirectonclick_".$search['id'], "<b>Redirect</b> to search results page when clicking on search icon?", $search['data']['redirectonclick']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
              if ($search['data']['triggerontype']=="") $search['data']['triggerontype'] = $_search_default['triggerontype'];
              $o = new wpdreamsYesNo("triggerontype_".$search['id'], "Trigger search when typing?", $search['data']['triggerontype']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php
              $o = new wpdreamsTextSmall("charcount_".$search['id'], "Minimal character count to trigger search", $search['data']['charcount'], array( array("func"=>"ctype_digit", "op"=>"eq", "val"=>true) ));
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item"><?php
              $o = new wpdreamsTextSmall("maxresults_".$search['id'], "Max. results", $search['data']['maxresults'], array( array("func"=>"ctype_digit", "op"=>"eq", "val"=>true) ));
              $params[$o->getName()] = $o->getData();
              ?></div>  
              <div class="item"><?php
              $o = new wpdreamsTextSmall("itemscount_".$search['id'], "Results box viewport (in item numbers)", $search['data']['itemscount'], array( array("func"=>"ctype_digit", "op"=>"eq", "val"=>true) ));
              $params[$o->getName()] = $o->getData();
              ?></div>  
              <div class="item"><?php
              $o = new wpdreamsTextSmall("resultitemheight_".$search['id'], "One result item height", $search['data']['resultitemheight'], array( array("func"=>"ctype_digit", "op"=>"eq", "val"=>true) ));
              $params[$o->getName()] = $o->getData();
              ?></div>                
              <div class="item">
              <?php
              if ($search['data']['imagesettings']=="") $search['data']['imagesettings'] = $_search_default['imagesettings'];       
              $o = new wpdreamsImageSettings("imagesettings_".$search['id'], "Image Settings", $search['data']['imagesettings']);
              $params[$o->getName()] = $o->getData();
              $params["settings-".$o->getName()] = $o->getSettings();
              ?>
              </div>
              <div class="item"><?php
              if ($search['data']['imagebg']=="") $search['data']['imagebg'] = $_search_default['imagebg']; 
              $o = new wpdreamsColorPicker("imagebg_".$search['id'],"Transparent images background", $search['data']['imagebg']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
                <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save all tabs!" />
              </div> 
          </fieldset>
          </div>
          <div tabid="2">
          <fieldset>
            <legend>Multisite Options</legend>
            <div class='item'>
              <p>
                 If you not choose any site, then the <b>currently active</b> blog will be used!<br />
                 Also, you can use the same search on multiple blogs!
              </p>
            </div>
            <div class="item">
            <?php
            if (!isset($search['data']['searchinblogtitles'])) $search['data']['searchinblogtitles'] = 0;
            $o = new wpdreamsYesNo("searchinblogtitles_".$search['id'], "Search in blog titles?", $search['data']['searchinblogtitles']);
            $params[$o->getName()] = $o->getData();
            ?>
            </div>
            <div class="item"><?php
            $o = new wpdreamsSelect("blogtitleorderby_".$search['id'], "Result ordering", (($search['data']['blogtitleorderby']!="")?$search['data']['blogtitleorderby']:$_search_default['blogtitleorderby'] ));
            $params[$o->getName()] = $o->getData();
            $params["selected-".$o->getName()] = $o->getSelected();
            ?></div> 
            <div class="item">
            <?php
              $o = new wpdreamsText("blogresultstext_".$search['id'], "Blog results group default text", (($search['data']['blogresultstext']!="")?$search['data']['blogresultstext']:$_search_default['blogresultstext'] ));
              $params[$o->getName()] = $o->getData();
            ?>
            </div>
            <div class="item"><?php
            if (!isset($search['data']['blogs'])) $search['data']['blogs'] = ''; 
            $o = new wpdreamsBlogselect("blogs_".$search['id'], "Blogs", $search['data']['blogs']);
            $params[$o->getName()] = $o->getData();
            $params['selected-'.$o->getName()] = $o->getSelected();  
            ?></div>
            <div class="item">
              <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save all tabs!" />
            </div>
          </fieldset>
          </div>
          <div tabid="3">
          <fieldset>
          <legend>Frontend Search Settings options</legend>
              <div class="item" style="text-align:center;"> 
                The default values of the checkboxes on the frontend are the values set above.
              </div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showexactmatches_".$search['id'], "Show exact matches selector?", $search['data']['showexactmatches']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['exactmatchestext']=="") $search['data']['exactmatchestext'] = $_search_default['exactmatchestext'];
              $o = new wpdreamsText("exactmatchestext_".$search['id'], "Text", $search['data']['exactmatchestext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php 
              $o = new wpdreamsYesNo("showsearchinposts_".$search['id'], "Show search in posts selector?", $search['data']['showsearchinposts']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinpoststext']=="") $search['data']['searchinpoststext'] = $_search_default['searchinpoststext'];
              $o = new wpdreamsText("searchinpoststext_".$search['id'], "Text", $search['data']['searchinpoststext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchinpages_".$search['id'], "Show search in pages selector?", $search['data']['showsearchinpages']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinpagestext']=="") $search['data']['searchinpagestext'] = $_search_default['searchinpagestext'];
              $o = new wpdreamsText("searchinpagestext_".$search['id'], "Text", $search['data']['searchinpagestext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php 
              $o = new wpdreamsYesNo("showsearchintitle_".$search['id'], "Show search in title selector?", $search['data']['showsearchintitle']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchintitletext']=="") $search['data']['searchintitletext'] = $_search_default['searchintitletext'];
              $o = new wpdreamsText("searchintitletext_".$search['id'], "Text", $search['data']['searchintitletext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchincontent_".$search['id'], "Show search in content selector?", $search['data']['showsearchincontent']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchincontenttext']=="") $search['data']['searchincontenttext'] = $_search_default['searchincontenttext'];
              $o = new wpdreamsText("searchincontenttext_".$search['id'], "Text", $search['data']['searchincontenttext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchincomments_".$search['id'], "Show search in comments selector?", $search['data']['showsearchincomments']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchincommentstext']=="") $search['data']['searchincommentstext'] = $_search_default['searchincommentstext'];
              $o = new wpdreamsText("searchincommentstext_".$search['id'], "Text", $search['data']['searchincommentstext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchinexcerpt_".$search['id'], "Show search in excerpt selector?", $search['data']['showsearchinexcerpt']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinexcerpttext']=="") $search['data']['searchinexcerpttext'] = $_search_default['searchinexcerpttext'];
              $o = new wpdreamsText("searchinexcerpttext_".$search['id'], "Text", $search['data']['searchinexcerpttext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchinbpusers_".$search['id'], "Show search in BuddyPress users selector?", $search['data']['showsearchinbpusers']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinbpuserstext']=="") $search['data']['searchinbpuserstext'] = $_search_default['searchinbpuserstext'];
              $o = new wpdreamsText("searchinbpuserstext_".$search['id'], "Text", $search['data']['searchinbpuserstext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchinbpgroups_".$search['id'], "Show search in BuddyPress groups selector?", $search['data']['showsearchinbpgroups']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinbpgroupstext']=="") $search['data']['searchinbpgroupstext'] = $_search_default['searchinbpgroupstext'];
              $o = new wpdreamsText("searchinbpgroupstext_".$search['id'], "Search in BuddyPress groups text", $search['data']['searchinbpgroupstext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              $o = new wpdreamsYesNo("showsearchinbpforums_".$search['id'], "Show search in BuddyPress forums selector?", $search['data']['showsearchinbpforums']);
              $params[$o->getName()] = $o->getData();
              if ($search['data']['searchinbpforumstext']=="") $search['data']['searchinbpforumstext'] = $_search_default['searchinbpforumstext'];
              $o = new wpdreamsText("searchinbpforumstext_".$search['id'], "Text", $search['data']['searchinbpforumstext']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item"><?php
              if (!isset($search['data']['showcustomtypes'])) $search['data']['showcustomtypes'] = $_search_default['showcustomtypes']; 
              $o = new wpdreamsCustomPostTypesEditable("showcustomtypes_".$search['id'], "Show search in custom post types selectors", $search['data']['showcustomtypes']);
              $params[$o->getName()] = $o->getData();
              $params['selected-'.$o->getName()] = $o->getSelected();
              ?></div>
              <div class="item">
              <?php
              if ($search['data']['showsearchincategories']=="") $search['data']['showsearchincategories'] = $_search_default['showsearchincategories'];
              $o = new wpdreamsYesNo("showsearchincategories_".$search['id'], "Show the categories selectors?", $search['data']['showsearchincategories']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item"><?php
              if (!isset($search['data']['exsearchincategories'])) $search['data']['exsearchincategories'] = $_search_default['exsearchincategories']; 
              $o = new wpdreamsCategories("exsearchincategories_".$search['id'], "Select which categories exclude", $search['data']['exsearchincategories']);
              $params[$o->getName()] = $o->getData();
              $params['selected-'.$o->getName()] = $o->getSelected();
              ?></div>
              <div class="item">
              <?php
              if (!isset($search['data']['exsearchincategoriesheight'])) $search['data']['exsearchincategoriesheight'] = $_search_default['exsearchincategoriesheight']; 
              $o = new wpdreamsText("exsearchincategoriesheight_".$search['id'], "Categories box max-height (0 for auto, default 200)", $search['data']['exsearchincategoriesheight']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item">
              <?php
              if (!isset($search['data']['exsearchincategoriestext'])) $search['data']['exsearchincategoriestext'] = $_search_default['exsearchincategoriestext']; 
              $o = new wpdreamsText("exsearchincategoriestext_".$search['id'], "Categories box header text", $search['data']['exsearchincategoriestext']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php  
              if (!isset($search['data']['exsearchincategoriestextfont'])) $search['data']['exsearchincategoriestextfont'] = $_search_default['exsearchincategoriestextfont'];     
              $o = new wpdreamsFont("exsearchincategoriestextfont_".$search['id'], "Categories box header text font", $search['data']['exsearchincategoriestextfont']);
              $params[$o->getName()] = $o->getData();
              $params["script-".$o->getName()] = $o->getImport();
              ?>
              </div>  
            <div class="item">
              <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save all tabs!" />
            </div>       
          </fieldset>
          </div>
          <div tabid="4">
          <fieldset>
          <legend>Layout Options</legend>
              <div class="item">          
              <?php
              $o = new wpdreamsText("defaultsearchtext_".$search['id'], "Default search text", $search['data']['defaultsearchtext']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div> 
              <div class="item">          
              <?php
              if (!isset($search['data']['showmoreresults'])) $search['data']['showmoreresults'] = $_search_default['showmoreresults'];
              $o = new wpdreamsYesNo("showmoreresults_".$search['id'], "Show 'More results..' text in the bottom of the search box?", $search['data']['showmoreresults']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>  
              <div class="item">          
              <?php
              if (!isset($search['data']['showmoreresultstext'])) $search['data']['showmoreresultstext'] = $_search_default['showmoreresultstext'];
              $o = new wpdreamsText("showmoreresultstext_".$search['id'], "' Show more results..' text", $search['data']['showmoreresultstext']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php  
              if (!isset($search['data']['showmorefont'])) $search['data']['showmorefont'] = $_search_default['showmorefont'];     
              $o = new wpdreamsFont("showmorefont_".$search['id'], "Show more font", $search['data']['showmorefont']);
              $params[$o->getName()] = $o->getData();
              $params["script-".$o->getName()] = $o->getImport();
              ?>
              </div>     
              <div class="item">          
              <?php
              $o = new wpdreamsYesNo("resultareaclickable_".$search['id'], "Make the whole result area clickable?", $search['data']['resultareaclickable']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">          
              <?php
              $o = new wpdreamsYesNo("showauthor_".$search['id'], "Show author in results?", $search['data']['showauthor']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">               
              <?php
              $o = new wpdreamsYesNo("showdate_".$search['id'], "Show date in results?", $search['data']['showdate']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div> 
              <div class="item">               
              <?php
              $o = new wpdreamsYesNo("showdescription_".$search['id'], "Show description in results?", $search['data']['showdescription']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">               
              <?php
              $o = new wpdreamsTextSmall("descriptionlength_".$search['id'], "Description length", $search['data']['descriptionlength']);
              $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item"><?php
              $o = new wpdreamsText("noresultstext_".$search['id'], "No results text", $search['data']['noresultstext']);
              $params[$o->getName()] = $o->getData();
              ?></div>             
              <div class="item"><?php
              $o = new wpdreamsText("didyoumeantext_".$search['id'], "Did you mean text", $search['data']['didyoumeantext']);
              $params[$o->getName()] = $o->getData();
              ?></div> 
              <div class="item"><?php
              if (!isset($search['data']['highlight'])) $search['data']['highlight'] = $_search_default['highlight']; 
              $o = new wpdreamsYesNo("highlight_".$search['id'], "Highlight search text in results?", $search['data']['highlight']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item"><?php
              if (!isset($search['data']['highlightwholewords'])) $search['data']['highlightwholewords'] = $_search_default['highlightwholewords']; 
              $o = new wpdreamsYesNo("highlightwholewords_".$search['id'], "Highlight only whole words?", $search['data']['highlightwholewords']);
              $params[$o->getName()] = $o->getData();
              ?></div>
              <div class="item"><?php
              if (!isset($search['data']['highlightcolor'])) $search['data']['highlightcolor'] = $_search_default['highlightcolor']; 
              $o = new wpdreamsColorPicker("highlightcolor_".$search['id'], "Highlight text color", $search['data']['highlightcolor']);
              $params[$o->getName()] = $o->getData();
              ?></div> 
              <div class="item"><?php
              if (!isset($search['data']['highlightbgcolor'])) $search['data']['highlightbgcolor'] = $_search_default['highlightbgcolor']; 
              $o = new wpdreamsColorPicker("highlightbgcolor_".$search['id'], "Highlight-text background color", $search['data']['highlightbgcolor']);
              $params[$o->getName()] = $o->getData();
              ?></div>
            <div class="item">
              <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save all tabs!" />
            </div> 
          </fieldset>
          </div>
          <div tabid="5">
          <fieldset>
            <legend>Autocomplete Options</legend>
            <div class="item"><?php
            if (!isset($search['data']['autocomplete'])) $search['data']['autocomplete'] = $_search_default['autocomplete']; 
            $o = new wpdreamsYesNo("autocomplete_".$search['id'], "Turn on search autocomplete?", $search['data']['autocomplete']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php
            $o = new wpdreamsSelect("autocompletesource_".$search['id'], "Autocomplete source", (($search['data']['autocompletesource']!="")?$search['data']['autocompletesource']:$_search_default['autocompletesource'] ));
            $params[$o->getName()] = $o->getData();
            $params["selected-".$o->getName()] = $o->getSelected();
            ?></div> 
            <div class="item"><?php
            $o = new wpdreamsTextarea("autocompleteexceptions_".$search['id'], "Keyword exceptions (comma separated)", $search['data']['autocompleteexceptions']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item">
              <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save all tabs!" />
            </div>
          </fieldset>
          </div>
          <div tabid="6">
          <fieldset>
          <legend>Theme Options</legend>
            <div class="item">
            <?php
            $o = new wpdreamsThemeChooser("themes_".$search['id'], "Theme Chooser", $_themes);
            //$params[$o->getName()] = $o->getData();
            ?>
            </div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("boxbackground_".$search['id'],"Search box background color", $search['data']['boxbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>           
            <div class="item">
            <?php
            if ($search['data']['boxborder']=="") $search['data']['boxborder'] = $_search_default['boxborder'];        
            $o = new wpdreamsBorder("boxborder_".$search['id'], "Search box border", $search['data']['boxborder']);
            $params[$o->getName()] = $o->getData(); 
            ?>
            </div>
            <div class="item">
            <?php
            if ($search['data']['boxshadow']=="") $search['data']['boxshadow'] = $_search_default['boxshadow'];        
            $o = new wpdreamsBoxShadow("boxshadow_".$search['id'], "Search box Shadow", $search['data']['boxshadow']);
            $params[$o->getName()] = $o->getData();
            //print $o->getCss();
            ?>
            </div> 
            
            <div class="item"><?php
            $o = new wpdreamsColorPicker("inputbackground_".$search['id'],"Search input field background color", $search['data']['inputbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item">
            <?php
            if ($search['data']['inputborder']=="") $search['data']['inputborder'] = $_search_default['inputborder'];        
            $o = new wpdreamsBorder("inputborder_".$search['id'], "Search input field border", $search['data']['inputborder']);
            $params[$o->getName()] = $o->getData(); 
            ?>
            </div> 
            <div class="item">
            <?php
            if ($search['data']['inputshadow']=="") $search['data']['inputshadow'] = $_search_default['boxshadow'];        
            $o = new wpdreamsBoxShadow("inputshadow_".$search['id'], "Search input field Shadow", $search['data']['inputshadow']);
            $params[$o->getName()] = $o->getData();
            //print $o->getCss();
            ?>
            </div> 
            <div class="item"><?php       
            $o = new wpdreamsFont("inputfont_".$search['id'], "Search input font", $search['data']['inputfont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("settingsbackground_".$search['id'],"Settings-box background", $search['data']['settingsbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("settingsdropbackground_".$search['id'],"Settings drop-down background", $search['data']['settingsdropbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("settingsdropbackgroundfontcolor_".$search['id'],"Settings drop-down font-color", $search['data']['settingsdropbackgroundfontcolor']);
            $params[$o->getName()] = $o->getData();
            ?></div>  
            <div class="item">
            <?php
            if (!isset($search['data']['selected-settingsimage'])) $search['data']['settingsimage'] = $_search_default['settingsimage'];       
            $o = new wpdreamsImageRadio("settingsimage_".$search['id'], "Settimgs image", $search['data']['settingsimage']);
            $params[$o->getName()] = $o->getData();
            $params["selected-".$o->getName()] = $o->getSelected(); 
            ?>
            </div> 
            <div class="item"><?php
            $o = new wpdreamsColorPicker("magnifierbackground_".$search['id'],"Magnifier background", $search['data']['magnifierbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div> 
            <div class="item">
            <?php
            if (!isset($search['data']['selected-magnifierimage'])) $search['data']['magnifierimage'] = $_search_default['magnifierimage'];       
            $o = new wpdreamsImageRadio("magnifierimage_".$search['id'], "Magnifier image", $search['data']['magnifierimage']);
            $params[$o->getName()] = $o->getData();
            $params["selected-".$o->getName()] = $o->getSelected(); 
            ?>
            </div>
            <div class="item">
            <?php
            if (!isset($search['data']['selected-loadingimage'])) $search['data']['loadingimage'] = $_search_default['loadingimage'];       
            $o = new wpdreamsImageRadio("loadingimage_".$search['id'], "Loading image", $search['data']['loadingimage']);
            $params[$o->getName()] = $o->getData();
            $params["selected-".$o->getName()] = $o->getSelected(); 
            ?>
            </div> 
            <div class="item">
            <?php
            if ($search['data']['resultsborder']=="") $search['data']['resultsborder'] = $_search_default['resultsborder'];        
            $o = new wpdreamsBorder("resultsborder_".$search['id'], "Results box border", $search['data']['resultsborder']);
            $params[$o->getName()] = $o->getData(); 
            ?>
            </div>
            <div class="item">
            <?php
            if ($search['data']['resultshadow']=="") $search['data']['resultshadow'] = $_search_default['resultshadow'];        
            $o = new wpdreamsBoxShadow("resultshadow_".$search['id'], "Results box Shadow", $search['data']['resultshadow']);
            $params[$o->getName()] = $o->getData();
            //print $o->getCss();
            ?>
            </div> 
            <div class="item"><?php
            $o = new wpdreamsColorPicker("resultsbackground_".$search['id'],"Results box background color", $search['data']['resultsbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div> 
            <div class="item"><?php
            $o = new wpdreamsColorPicker("resultscontainerbackground_".$search['id'],"Result items container box background color", $search['data']['resultscontainerbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("resultscontentbackground_".$search['id'],"Result content background color", $search['data']['resultscontentbackground']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php       
            $o = new wpdreamsFont("titlefont_".$search['id'], "Results title link font", $search['data']['titlefont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php       
            $o = new wpdreamsFont("titlehoverfont_".$search['id'], "Results title hover link font", $search['data']['titlehoverfont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php       
            $o = new wpdreamsFont("authorfont_".$search['id'], "Author text font", $search['data']['authorfont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php       
            $o = new wpdreamsFont("datefont_".$search['id'], "Date text font", $search['data']['datefont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php       
            $o = new wpdreamsFont("descfont_".$search['id'], "Description text font", $search['data']['descfont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php
            if (!isset($search['data']['exsearchincategoriesboxcolor']))  $search['data']['exsearchincategoriesboxcolor'] = $_search_default['exsearchincategoriesboxcolor'];
            $o = new wpdreamsColorPicker("exsearchincategoriesboxcolor_".$search['id'],"Grouping box header background color", $search['data']['exsearchincategoriesboxcolor']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php 
            if (!isset($search['data']['groupbytextfont'])) $search['data']['groupbytextfont'] = $_search_default['groupbytextfont'];      
            $o = new wpdreamsFont("groupbytextfont_".$search['id'], "Grouping font color", $search['data']['groupbytextfont']);
            $params[$o->getName()] = $o->getData();
            $params["script-".$o->getName()] = $o->getImport();
            ?>
            </div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("arrowcolor_".$search['id'],"Resultbar arrow color", $search['data']['arrowcolor']);
            $params[$o->getName()] = $o->getData();
            ?></div>
            <div class="item"><?php
            $o = new wpdreamsColorPicker("overflowcolor_".$search['id'],"Resultbar overflow color", $search['data']['overflowcolor']);
            $params[$o->getName()] = $o->getData();
            ?></div>                
            <textarea class='previewtextarea' style='display:none;width:600px;'>  
            </textarea>
            <script>
            jQuery(document).ready(function() {
              (function( $ ){
                $(".previewtextarea").click(function(){
                   var parent = $(this).parent();
                   var content = "";
                    var v = "";
                   $("input[type=text], input[type=hidden]", parent).each(function(){
                      if ($(this).attr('type')=="hidden" || $(this).parent().parent().hasClass("item")==true) {
                        var name = $(this).attr("name").match(/(.*?)_/)[1];
                        var val = $(this).val().replace(/(\r\n|\n|\r)/gm,"");
                        content += '"'+name+'":"'+val+'",\n';
                      }
                   });
                   $(this).val(content+v);
                });
              }(jQuery))
            });
            </script>
          <div class="item">
            <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save this search!" />
          </div>
          </fieldset>
          </div>
          <div tabid="7">
          <fieldset>
          <legend>Advanced Options</legend>
          
              <div class="item">
              <?php
                $o = new wpdreamsYesNo("runshortcode_".$search['id'], "Run shortcodes found in post content", (($search['data']['runshortcode']!="")?$search['data']['runshortcode']:$_search_default['runshortcode'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div> 
              <div class="item">
              <?php
                $o = new wpdreamsText("striptagsexclude_".$search['id'], "HTML Tags exclude from stripping content", (($search['data']['striptagsexclude']!="")?$search['data']['striptagsexclude']:$_search_default['striptagsexclude'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div> 
              <div class="item">
              <?php
                $o = new wpdreamsCustomSelect("titlefield_".$search['id'], "Title Field", array('selects'=>$_search_default['titlefield'], 'selected'=>(($search['data']['titlefield']!="")?$search['data']['titlefield']:$_search_default['titlefield_def'] )) );
                $params[$o->getName()] = $o->getData();
              ?>
              </div> 
              <div class="item">
              <?php
                $o = new wpdreamsCustomSelect("descriptionfield_".$search['id'], "Description Field", array('selects'=>$_search_default['descriptionfield'], 'selected'=>(($search['data']['descriptionfield']!="")?$search['data']['descriptionfield']:$_search_default['descriptionfield_def'] )) );
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                if (!isset($search['data']['excludecategories'])) $search['data']['excludecategories'] = $_search_default['excludecategories']; 
                $o = new wpdreamsCategories("excludecategories_".$search['id'], "Exclude categories", $search['data']['excludecategories']);
                $params[$o->getName()] = $o->getData();
                $params['selected-'.$o->getName()] = $o->getSelected();
              ?>
              </div> 
              <div class="item">
              <?php
                $o = new wpdreamsTextarea("excludeposts_".$search['id'], "Exclude Posts by ID's (comma separated post ID-s)", $search['data']['excludeposts']);
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsSelect("groupby_".$search['id'], "Group results by", (($search['data']['groupby']!="")?$search['data']['groupby']:$_search_default['groupby'] ));
                $params[$o->getName()] = $o->getData();
                $params["selected-".$o->getName()] = $o->getSelected();
              ?>
              </div> 
              <div class="item">
              <?php
                $o = new wpdreamsText("groupbytext_".$search['id'], "Group by default text (%GROUP% is changed into the current cateogry/post type name)", (($search['data']['groupby']!="")?$search['data']['groupbytext']:$_search_default['groupbytext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsText("bbpressreplytext_".$search['id'], "BBPress replies results group default text", (($search['data']['bbpressreplytext']!="")?$search['data']['bbpressreplytext']:$_search_default['bbpressreplytext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsText("bbpressgroupstext_".$search['id'], "BBPress group results group default text", (($search['data']['bbpressgroupstext']!="")?$search['data']['bbpressgroupstext']:$_search_default['bbpressgroupstext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsText("bbpressuserstext_".$search['id'], "BBPress user results group default text", (($search['data']['bbpressuserstext']!="")?$search['data']['bbpressuserstext']:$_search_default['bbpressuserstext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsText("commentstext_".$search['id'], "Comments results group default text", (($search['data']['commentstext']!="")?$search['data']['commentstext']:$_search_default['commentstext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsText("uncategorizedtext_".$search['id'], "Uncategorized group text", (($search['data']['uncategorizedtext']!="")?$search['data']['uncategorizedtext']:$_search_default['uncategorizedtext'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>
              <div class="item">
              <?php
                $o = new wpdreamsYesNo("showpostnumber_".$search['id'], "Show Post Numbering", (($search['data']['showpostnumber']!="")?$search['data']['showpostnumber']:$_search_default['showpostnumber'] ));
                $params[$o->getName()] = $o->getData();
              ?>
              </div>              
              <div class="item">
                <input name="submit_<?php echo $search['id']; ?>" type="submit" value="Save this search!" />
              </div>
          </fieldset>
          </div>
        </div>

        
<?php
    $r = ob_get_clean();
    print $r;
    die;   
  }
?>