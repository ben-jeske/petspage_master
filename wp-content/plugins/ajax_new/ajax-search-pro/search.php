<?php
  add_action('wp_ajax_nopriv_ajaxsearchpro_autocomplete', 'ajaxsearchpro_autocomplete');
  add_action('wp_ajax_ajaxsearchpro_autocomplete', 'ajaxsearchpro_autocomplete');
  add_action('wp_ajax_nopriv_ajaxsearchpro_search', 'ajaxsearchpro_search');
  add_action('wp_ajax_ajaxsearchpro_search', 'ajaxsearchpro_search');
  add_action('wp_ajax_ajaxsearchpro_preview', 'ajaxsearchpro_preview');
  add_action('wp_ajax_ajaxsearchpro_precache', 'ajaxsearchpro_precache');
  add_action('wp_ajax_ajaxsearchpro_deletecache', 'ajaxsearchpro_deletecache');
  require_once(AJAXSEARCHPRO_PATH."/includes/suggest.class.php");
  require_once(AJAXSEARCHPRO_PATH."/includes/imagecache.class.php");
  require_once(AJAXSEARCHPRO_PATH."/includes/textcache.class.php");
  require_once(AJAXSEARCHPRO_PATH."/settings/types.inc.php");
  
  function ajaxsearchpro_search() {
  	global $wpdb;
    global $switched;
    $delimiter = "0xxb33pn";
    /*set_error_handler('handleError', E_ALL^E_NOTICE);
    try {*/ 
      $like = "";
      $s = strtolower(trim($_POST['aspp']));
      $s= preg_replace( '/\s+/', ' ', $s);                                           
      $stat = get_option("asp_stat");
      if (isset($wpdb->base_prefix)) {
        $_prefix = $wpdb->base_prefix;
      } else {
        $_prefix = $wpdb->prefix;
      }      
      if ($stat==1) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $in = $wpdb->query("UPDATE ".$_prefix."ajaxsearchpro_statistics SET num=num+1, last_date=".time()." WHERE (keyword='".$s."' AND search_id=".$_POST['asid'].")");	 
        if ($in==false) {
        	dbDelta("INSERT INTO ".$_prefix."ajaxsearchpro_statistics (search_id, keyword, num, last_date) VALUES (".$_POST['asid'].", '".$s."', 1, ".time().")");
        }
      }
  
      $search = $wpdb->get_row("SELECT * FROM ".$_prefix."ajaxsearchpro WHERE id=".$_POST['asid'], ARRAY_A);
      $search['data'] = json_decode($search['data'], true); 
      $search['data']['settings-imagesettings'] = $search['data']['selected-imagesettings'];
      if (isset($search) && $search['data']['exactonly']!=1) {
        $_s = explode(" ", $s);
      }                                   
      if (isset($_POST['options'])) {
        parse_str($_POST['options'], $options);   
      } 
      $limit = $search['data']['maxresults'];
      $not_exactonly = (isset($options['set_exactonly'])?false:true);
      $searchintitle = (isset($options['set_intitle'])?true:false);
      $searchincontent = (isset($options['set_incontent'])?true:false);
      $searchincomments = (isset($options['set_incomments'])?true:false);
      $searchinexcerpt = (isset($options['set_inexcerpt'])?true:false);
      $searchinposts = (isset($options['set_inposts'])?true:false);
      $searchinpages = (isset($options['set_inpages'])?true:false);
      $searchinterms = (($search['data']['searchinterms']==1)?true:false);
      $searchinbpusers = (isset($options['set_inbpusers'])?true:false);
      $searchinbpgroups = (isset($options['set_inbpgroups'])?true:false);
      $searchinbpforums = (isset($options['set_inbpforums'])?true:false);
      
      $searchin = (($search['data']['searchindrafts']==1)?" OR $wpdb->posts.post_status='draft'":"").(($search['data']['searchinpending']==1)?" OR $wpdb->posts.post_status='searchinpending'":"");
      
      $blogresults = array();
      $forumresults = array();
      $userresults = array();
      $groupresults = array();
      $allpageposts = array();
      $pageposts = array();
      $repliesresults = array();
      $allcommentsresults = array();
      $commentsresults = array();
      
      if (!isset($search['data']['selected-blogs']) || $search['data']['selected-blogs']==null || count($search['data']['selected-blogs'])<=0) {
        $search['data']['selected-blogs'] = array(0=>1);
      }   
      if (get_option('asp_caching')) {
        $filename = AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR.md5(json_encode($options).$s).".wp";
        $textcache = new wpdreamsTextCache($filename, get_option('asp_cachinginterval')*60);
        $cache_content = $textcache->getCache();
        if ($cache_content!=false) {
          print_r($cache_content);
          die;
        }
      }
      foreach ($search['data']['selected-blogs'] as $blog) {
        $like = "";
        if (is_multisite()) switch_to_blog($blog); 
           
        if ($searchintitle) {
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->posts.post_title) like '%",$_s);
          $sr =  " lower($wpdb->posts.post_title) like '%".$sr."%'";
         } else {
          $sr =  " lower($wpdb->posts.post_title) like '%".$s."%'";
         }
         $like .= $sr;
        }
      
        if ($searchincontent) {
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->posts.post_content) like '%",$_s);
          if ($like!="") {
            $sr =  " OR lower($wpdb->posts.post_content) like '%".$sr."%'";
          } else {
            $sr =  " lower($wpdb->posts.post_content) like '%".$sr."%'";
          }
         } else {
          if ($like!="") {
            $sr =  " OR lower($wpdb->posts.post_content) like '%".$s."%'";
          } else {
            $sr =  " lower($wpdb->posts.post_content) like '%".$s."%'";
          }
         }
         $like .= $sr;
        }
        

      
        if ($searchinexcerpt) {
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->posts.post_excerpt) like '%",$_s);
          if ($like!="") {
            $sr =  " OR lower($wpdb->posts.post_excerpt) like '%".$sr."%'";
          } else {
            $sr =  " lower($wpdb->posts.post_excerpt) like '%".$sr."%'";
          }
         } else {
          if ($like!="") {
            $sr =  " OR lower($wpdb->posts.post_excerpt) like '%".$s."%'";
          } else {
            $sr =  " lower($wpdb->posts.post_excerpt) like '%".$s."%'";
          }
         }
         $like .= $sr;
        }
      
        if ($searchinterms) {
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->terms.name) like '%",$_s);
          if ($like!="") {
            $sr =  " OR lower($wpdb->terms.name) like '%".$sr."%'";
          } else {
            $sr =  " lower($wpdb->terms.name) like '%".$sr."%'";
          }
         } else {
          if ($like!="") {
            $sr =  " OR lower($wpdb->terms.name) like '%".$s."%'";
          } else {
            $sr =  " lower($wpdb->terms.name) like '%".$s."%'";
          }
         }
         $like .= $sr;
        }
      
        if ($searchinposts) {
          $where = " $wpdb->posts.post_type='post'"; 
        }
        
        if ($searchinpages) {
          if ($where!="")
            $where.= " OR $wpdb->posts.post_type='page'";
          else
            $where.= "$wpdb->posts.post_type='page'"; 
        }
      
        $selected_customs = array();
        if (isset($options['customset']))
          $selected_customs = $options['customset'];
        if (is_array($selected_customs)) {
          foreach($selected_customs as $k=>$v){
              if ($where!="") {
                $where.= " OR $wpdb->posts.post_type='".$v."'";
              } else {
                $where.= "$wpdb->posts.post_type='".$v."'";
              } 
          }
        }
        
      
        $selected_customfields = $search['data']['selected-customfields'];
        if (is_array($selected_customfields) && count($selected_customfields)>0) {
           if ($not_exactonly) {
            $sr = implode("%' OR lower($wpdb->postmeta.meta_value) like '%",$_s);
            $sr =  "lower($wpdb->postmeta.meta_value) like '%".$sr."%'";
           } else {
            $sr =  "lower($wpdb->postmeta.meta_value) like '%".$s."%'";
           }
          $ws = "";
          foreach($selected_customfields as $k=>$v){
              if ($ws!="") {
                $ws.= " OR $wpdb->postmeta.meta_key='".$v."'";
              } else {
                $ws.= "$wpdb->postmeta.meta_key='".$v."'";
              } 
          }
          if ($like!="") {
            $like .= " OR ((".$sr.") AND (".$ws."))";
          } else {
            $like .= "((".$sr.") AND (".$ws."))";
          }
        }
        
        /*$_selected_categories = array();
        if (isset($options['categoryset']))
           $_selected_categories = $options['categoryset'];
        $_all_categories = get_all_category_ids();
        if (count($_selected_categories)>0 && (count($_all_categories)!=count($_selected_categories))) {
          $_ll = "";
          foreach($_selected_categories as $k=>$v) {
             if ($ll=="") {
               $ll = "term_id LIKE %(".$v.",% OR term_id LIKE %,".$v.",% OR term_id LIKE %,".$v." ,%";
             } else {
               $ll = " OR term_id LIKE %(".$v.",% OR term_id LIKE %,".$v.",% OR term_id LIKE %,".$v." ,%";
             }
          }
          if ($like!="") {
            $sr =  " AND (".$ll.")";
          } else {
            $sr =  " ";
          }
          $like .= $sr;
        }*/         
         
        if (isset($search['data']['excludeposts']) && $search['data']['excludeposts']!="") {
          $exclude_posts = "$wpdb->posts.ID NOT IN (".$search['data']['excludeposts'].")";
        } else {
          $exclude_posts = "$wpdb->posts.ID NOT IN (-55)";
        }
          
        $orderby = ((isset($search['data']['selected-orderby']) && $search['data']['selected-orderby']!='')?$search['data']['selected-orderby']:"post_date DESC");   
      	$s=strtolower(addslashes($_POST['aspp']));
      	$querystr = "
      		SELECT 
            $wpdb->posts.post_title as title,
            $wpdb->posts.ID as id,
            $wpdb->posts.post_date as date,
            $wpdb->posts.post_content as content,
            $wpdb->posts.post_excerpt as excerpt,
            $wpdb->users.user_nicename as author,
            GROUP_CONCAT(DISTINCT $wpdb->terms.term_id) as term_id,
            $wpdb->posts.post_type as post_type
      		FROM $wpdb->posts
          LEFT JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID
          LEFT JOIN $wpdb->users ON $wpdb->users.ID = $wpdb->posts.post_author
          LEFT JOIN $wpdb->term_relationships ON $wpdb->posts.ID = $wpdb->term_relationships.object_id
          LEFT JOIN $wpdb->term_taxonomy ON $wpdb->term_taxonomy.term_taxonomy_id = $wpdb->term_relationships.term_taxonomy_id
          LEFT JOIN $wpdb->terms ON $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id
      		WHERE
          ($wpdb->posts.post_status='publish' $searchin) AND
          (".$where.")			    
          AND (".$like.")
          AND (".$exclude_posts.")
          GROUP BY
            $wpdb->posts.ID
      		ORDER BY ".$wpdb->posts.".".$orderby."
      		LIMIT $limit;
      	 ";
        
    	 	$pageposts = $wpdb->get_results($querystr, OBJECT);
        
        
       $_selected_categories = array();
        if (isset($options['categoryset']))
           $_selected_categories = $options['categoryset'];
        $_all_categories = get_all_category_ids();
        if (count($_selected_categories)>0 && (count($_all_categories)!=count($_selected_categories))) {
          foreach ($pageposts as $k=>$v) {
            if ($v->term_id==null) {
              $v->term_id = "";
              continue;
            }
            if ($v->post_type!='post') {
              continue;
            }  
            $_term_ids = explode(',', $v->term_id);
            if (count($_term_ids)<=0) $_term_ids = array($v->term_id);
            $match = false; 
            $_new_terms = array();
            foreach ($_term_ids as $_term_id) {
              if (in_array($_term_id, $_selected_categories)) {
                $match = true;
                $_new_terms[] = $_term_id;
              }
            }
            if (!$match) {
              unset($pageposts[$k]);
            } else {
              $v->term_id = implode(",", $_new_terms);
            }
          }
        } 
        
        
        
        foreach ($pageposts as $k=>$v) {
           $pageposts[$k]->link = get_permalink($v->id);
           
           if ($search['data']['settings-imagesettings']['show']==1) {
             $imgs = $search['data']['settings-imagesettings'];
             ksort($imgs['from']);
             foreach($imgs['from'] as $kk=>$source) {
              if ($source=='featured') {
                $im = get_the_post_thumbnail($v->id);  
                $img = new wpdreamsImageCache($im, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], 1, $search['data']['imagebg']);
              } else if($source=='content') {
                $img = new wpdreamsImageCache($v->content, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], $imgs['imagenum'], $search['data']['imagebg']);
              } else if($source=='excerpt') {
                $img = new wpdreamsImageCache($v->excerpt, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], $imgs['imagenum'], $search['data']['imagebg']);
              }
              $res = $img->get_image();
              if ($res!='') {
                $pageposts[$k]->image = plugins_url('/cache/'.$res , __FILE__);
                break;
              } 
             } 
           }
           
           if (!isset($search['data']['titlefield']) || $search['data']['titlefield']=="0"){
             $pageposts[$k]->title = get_the_title($pageposts[$k]->id);
           } else {
             if ($search['data']['titlefield']=="1") {
               if (strlen($pageposts[$k]->excerpt)>=200)
                $pageposts[$k]->title = substr($pageposts[$k]->excerpt, 0, 200);
               else
                $pageposts[$k]->title = $pageposts[$k]->excerpt;
             } else {
               $mykey_values = get_post_custom_values($search['data']['titlefield'],  $pageposts[$k]->id);
               if (isset($mykey_values[0])) {
                 $pageposts[$k]->title = $mykey_values[0];
               } else {
                 $pageposts[$k]->title = get_the_title($pageposts[$k]->id);
               }
             }
           }

           if (!isset($search['data']['striptagsexclude'])) $search['data']['striptagsexclude'] = "<a><span>";

           if (!isset($search['data']['descriptionfield']) || $search['data']['descriptionfield']=="0"){
              $_content = strip_tags($pageposts[$k]->content, $search['data']['striptagsexclude']);
           } else {
             if ($search['data']['descriptionfield']=="1") {
               $_content = strip_tags($pageposts[$k]->excerpt, $search['data']['striptagsexclude']);
             } else if($search['data']['descriptionfield']=="2") {
               $_content = strip_tags(get_the_title($pageposts[$k]->id), $search['data']['striptagsexclude']);
             } else {
               $mykey_values = get_post_custom_values($search['data']['descriptionfield'],  $pageposts[$k]->id);
               if (isset($mykey_values[0])) {
                 $_content = strip_tags($mykey_values[0], $search['data']['striptagsexclude']);
               } else {
                 $_content = strip_tags(get_content_w($pageposts[$k]->content), $search['data']['striptagsexclude']);
               }
             }
           }                       
           if ($_content=="") $_content = $pageposts[$k]->content;
           if (isset($search['data']['runshortcode']) && $search['data']['runshortcode']==1) {
             if ($_content!="") $_content = apply_filters('the_content', $_content);
             if ($_content!="") $_content = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $_content);
             
           } 
           $_content = strip_tags($_content, $search['data']['striptagsexclude']);
           
           if ($_content!='' && (strlen($_content) > $search['data']['descriptionlength']))
            $pageposts[$k]->content = substr($_content, 0, $search['data']['descriptionlength'])."..."; 
           else
            $pageposts[$k]->content = $_content."..."; 
        }
        
        // Need a suffix to separate blogs 
        if (isset($blog)) {
          $_key_suff = "_".$blog;
        } else {
          $_key_suff = "";
        }
        /* Regrouping */
        // By category
        if ($search['data']['selected-groupby']==1 && count($pageposts)>0) {
           $_pageposts = array();
           foreach ($pageposts as $k=>$v) {
              if ($v->term_id=="" || $v->post_type!='post') {
                $_pageposts['99999']['data'][] = $v;
                continue;
              }
              $_term_ids = explode(',', $v->term_id);
              if (count($_term_ids)<=0) $_term_ids = array($v->term_id);
              foreach ($_term_ids as $_term_id) {
                $cat = get_category($_term_id);
                if (!is_object($cat) || trim($cat->name)=="") {
                  $_pageposts['99999']['data'][] = $v;
                } else {
                  $_pageposts[$_term_id]['data'][] = $v;
                }
              }
           }

           foreach($_pageposts as $k=>$v) {
              if ($search['data']['showpostnumber']==1) {
                $num = " (".count($_pageposts[$k]['data']).")";
              } else {
                $num = "";
              }
              if ($k!=99999) {
                $cat = get_category($k);
                $_pageposts[$k]['name'] = str_replace('%GROUP%', $cat->name, $search['data']['groupbytext']).$num;
              } else {
                $_pageposts[$k]['name'] = $search['data']['uncategorizedtext'].$num;
              }
           }

           $pageposts = null;
           $pageposts['grouped'] = 1;
           $pageposts['items'] = $_pageposts;
           ksort($pageposts['items']);
           if ($_key_suff!="") {
             foreach($pageposts['items'] as $k=>$v) {
              $pageposts['items'][$k.$_key_suff] = $v;
              unset($pageposts['items'][$k]);
             }
           }  
        // By post type
        } else if ($search['data']['selected-groupby']==2 && count($pageposts)>0) {
           foreach ($pageposts as $k=>$v) {
              $_pageposts[$v->post_type]['data'][] = $v;
           }
           foreach($_pageposts as $k=>$v) {
              if ($search['data']['showpostnumber']==1) {
                $num = " (".count($_pageposts[$k]['data']).")";
              } else {
                $num = "";
              }
              $obj = get_post_type_object($k);
              $_pageposts[$k]['name'] = str_replace('%GROUP%', $obj->labels->singular_name, $search['data']['groupbytext']).$num;
           }
           $pageposts = null;
           $pageposts['grouped'] = 1;
           $pageposts['items'] = $_pageposts;
           ksort($pageposts['items']);        
        } 
        
        if (($search['data']['selected-groupby']==1 || $search['data']['selected-groupby']==2) && count($pageposts)>0 && count($allpageposts)>0)
            $allpageposts['items'] = array_merge($allpageposts['items'], $pageposts['items']);
        else 
            $allpageposts = array_merge($allpageposts, $pageposts); 
        
        if ($searchincomments) {
         $like = "";
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->comments.comment_content) like '%",$_s);
          if ($like!="") {
            $sr =  " OR lower($wpdb->comments.comment_content) like '%".$sr."%'";
          } else {
            $sr =  " lower($wpdb->comments.comment_content) like '%".$sr."%'";
          }
         } else {
          if ($like!="") {
            $sr =  " OR lower($wpdb->comments.comment_content) like '%".$s."%'";
          } else {
            $sr =  " lower($wpdb->comments.comment_content) like '%".$s."%'";
          }
         }
         $like .= $sr;
        	$querystr = "
        		SELECT 
              $wpdb->comments.comment_ID as id,
              $wpdb->comments.comment_post_ID as post_id,
              $wpdb->comments.user_id as user_id,
              $wpdb->comments.comment_content as content,
              $wpdb->comments.comment_date as date
        		FROM $wpdb->comments
        		WHERE
            ($wpdb->comments.comment_approved=1)
            AND
            (".$like.")
        		ORDER BY $wpdb->comments.comment_ID DESC
        		LIMIT $limit;
        	 ";
           //print_r($querystr);
      	 	$commentsresults = $wpdb->get_results($querystr, OBJECT); 
          if (is_array($commentsresults)) {
            foreach ($commentsresults as $k=>$v) {
               $commentsresults[$k]->link = get_comment_link($v->id);
               $commentsresults[$k]->author = get_comment_author($v->id);
               if ($search['data']['settings-imagesettings']['show']==1) {
                 $imgs = $search['data']['settings-imagesettings'];
                 ksort($imgs['from']);
                 foreach($imgs['from'] as $kk=>$source) {
                  $img = new wpdreamsImageCache($v->content, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], $imgs['imagenum'], $search['data']['imagebg']);
                  $res = $img->get_image();
                  if ($res!='') {
                    $commentsresults[$k]->image = plugins_url('/cache/'.$res , __FILE__);
                    break;
                  }
                 } 
               }
               $commentsresults[$k]->title = substr($commentsresults[$k]->content, 0, 15)."...";
                   
            }
          }
          $allcommentsresults = array_merge($allcommentsresults, $commentsresults);
        } 
      }
      
      if (is_multisite()) restore_current_blog();    
      
      /*
        BuddyPress Section START
      */              
      if (function_exists('bp_core_get_user_domain')) {
        /* BP users */
        $like = "";
        if ($searchinbpusers) {
         if ($not_exactonly) {
          $sr = implode("%' OR lower($wpdb->users.display_name) like '%",$_s);
          if ($like!="") {
            $sr =  " OR lower($wpdb->users.display_name) like '%".$sr."%'";
          } else {
            $sr =  " lower($wpdb->users.display_name) like '%".$sr."%'";
          }
         } else {
          if ($like!="") {
            $sr =  " OR lower($wpdb->users.display_name) like '%".$s."%'";
          } else {
            $sr =  " lower($wpdb->users.display_name) like '%".$s."%'";
          }
         }
         $like .= $sr;
        }
        $querystr = "
           SELECT
             $wpdb->users.ID as id,
             $wpdb->users.display_name as title,
             '' as date,
             '' as author
           FROM
             $wpdb->users
           WHERE 
            (".$like.") 
        ";
        $userresults = $wpdb->get_results($querystr, OBJECT);
        foreach ($userresults as $k=>$v) {
          $userresults[$k]->link = bp_core_get_user_domain($v->id);
          $imgs = $search['data']['settings-imagesettings'];
          if ($search['data']['settings-imagesettings']['show']==1) {
            $im = bp_core_fetch_avatar( 'item_id='.$userresults[$k]->id);
            $img = new wpdreamsImageCache($im, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], 1, $search['data']['imagebg']);
            $res = $img->get_image();
            if ($res!='') {
              $userresults[$k]->image = plugins_url('/cache/'.$res , __FILE__);
            }
          }
          $update=get_usermeta( $v->id, 'bp_latest_update' );
          $userresults[$k]->content = $update['content'];
          if ($userresults[$k]->content!='') {
           $userresults[$k]->content = substr(strip_tags($userresults[$k]->content), 0, $search['data']['descriptionlength'])."...";
          } else {
           $userresults[$k]->content = "";
          }
            
        }
        
        /* BP groups */
        $like = "";
        if ($searchinbpgroups) {
          if ($not_exactonly) {
            $sr = implode("%' OR lower(".$wpdb->prefix."bp_groups.name) like '%",$_s);
            if ($like!="") {
              $sr =  " OR lower(".$wpdb->prefix."bp_groups.name) like '%".$sr."%'";
            } else {
              $sr =  " lower(".$wpdb->prefix."bp_groups.name) like '%".$sr."%'";
            }
          } else {
          if ($like!="") {
              $sr =  " OR lower(".$wpdb->prefix."bp_groups.name) like '%".$s."%'";
            } else {
              $sr =  " lower(".$wpdb->prefix."bp_groups.name) like '%".$s."%'";
            }
          }
          $like .= $sr;
          if ($not_exactonly) {
            $sr = implode("%' OR lower(".$wpdb->prefix."bp_groups.description) like '%",$_s);
            if ($like!="") {
              $sr =  " OR lower(".$wpdb->prefix."bp_groups.description) like '%".$sr."%'";
            } else {
              $sr =  " lower(".$wpdb->prefix."bp_groups.description) like '%".$sr."%'";
            }
          } else {
          if ($like!="") {
              $sr =  " OR lower(".$wpdb->prefix."bp_groups.description) like '%".$s."%'";
            } else {
              $sr =  " lower(".$wpdb->prefix."bp_groups.description) like '%".$s."%'";
            }
          }
          $like .= $sr;
          if (isset($search['data']['searchinbpprivategroups']) && $search['data']['searchinbpprivategroups']==0) {
              $_and = "AND ".$wpdb->prefix."bp_groups.status = 'public'";
          } else {
              $_and = "";
          }        
          $querystr = "
             SELECT
               ".$wpdb->prefix."bp_groups.id as id,
               ".$wpdb->prefix."bp_groups.name as title,
               ".$wpdb->prefix."bp_groups.description as content,
               ".$wpdb->prefix."bp_groups.date_created as date,
               $wpdb->users.user_nicename as author             
             FROM
               ".$wpdb->prefix."bp_groups
             LEFT JOIN $wpdb->users ON $wpdb->users.ID = ".$wpdb->prefix."bp_groups.creator_id
             WHERE 
              (".$like.")
              ".$_and;    //AND ".$wpdb->prefix."bp_groups.status = 'public'
  
          $groupresults = $wpdb->get_results($querystr, OBJECT);
          foreach ($groupresults as $k=>$v) {
            $group = new BP_Groups_Group($v->id);
            $groupresults[$k]->link = bp_get_group_permalink($group);
            $imgs = $search['data']['settings-imagesettings'];
            if ($search['data']['settings-imagesettings']['show']==1) {
              $avatar_options = array ( 'item_id' => $v->id, 'object' => 'group', 'type' => 'full', 'html' => true );
              $im =   bp_core_fetch_avatar($avatar_options);
              $img = new wpdreamsImageCache($im, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], 1, $search['data']['imagebg']);
              $res = $img->get_image();
              if ($res!='') {
                $groupresults[$k]->image = plugins_url('/cache/'.$res , __FILE__);
              }
            }
           if ($groupresults[$k]->content!='')
            $groupresults[$k]->content = substr(strip_tags($groupresults[$k]->content), 0, $search['data']['descriptionlength'])."...";
          }
        }      
  
          do_action( 'bbpress_init' );

          if ($searchinbpforums && class_exists('BB_Query_Form')) {
            $like = "";
            
            if ($not_exactonly) {
              $sr = implode("%' OR lower(".$wpdb->prefix."bb_forums.forum_name) like '%",$_s);
              if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_forums.forum_name) like '%".$sr."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_forums.forum_name) like '%".$sr."%'";
              }
            } else {
            if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_forums.forum_name) like '%".$s."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_forums.forum_name) like '%".$s."%'";
              }
            }
            $like .= $sr; 
            
            if ($not_exactonly) {
              $sr = implode("%' OR lower(".$wpdb->prefix."bb_topics.topic_title) like '%",$_s);
              if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_topics.topic_title) like '%".$sr."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_topics.topic_title) like '%".$sr."%'";
              }
            } else {
            if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_topics.topic_title) like '%".$s."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_topics.topic_title) like '%".$s."%'";
              }
            }
            $like .= $sr; 
            
            if ($not_exactonly) {
              $sr = implode("%' OR lower(".$wpdb->prefix."bb_posts.post_text) like '%",$_s);
              if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_posts.post_text) like '%".$sr."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_posts.post_text) like '%".$sr."%'";
              }
            } else {
            if ($like!="") {
                $sr =  " OR lower(".$wpdb->prefix."bb_posts.post_text) like '%".$s."%'";
              } else {
                $sr =  " lower(".$wpdb->prefix."bb_posts.post_text) like '%".$s."%'";
              }
            }
            $like .= $sr;
            
            if (!isset($search['data']['selected-bpgroupstitle'])) $search['data']['selected-bpgroupstitle'] = "bb_topics.topic_title as title";
            
            $querystr = "
               SELECT
                 ".$wpdb->prefix."bb_posts.post_id as id,
                 ".$wpdb->prefix.$search['data']['selected-bpgroupstitle'].",
                 ".$wpdb->prefix."bb_posts.post_text as content,
                 ".$wpdb->prefix."bb_posts.post_time as date,
                 ".$wpdb->prefix."bb_topics.topic_id as topic_id,
                 $wpdb->users.user_nicename as author             
               FROM
                 ".$wpdb->prefix."bb_posts
               LEFT JOIN $wpdb->users ON $wpdb->users.ID = ".$wpdb->prefix."bb_posts.poster_id
               LEFT JOIN ".$wpdb->prefix."bb_topics ON ".$wpdb->prefix."bb_posts.topic_id = ".$wpdb->prefix."bb_topics.topic_id 
               LEFT JOIN ".$wpdb->prefix."bb_forums ON ".$wpdb->prefix."bb_posts.forum_id = ".$wpdb->prefix."bb_forums.forum_id
               WHERE 
                (".$like.")
            ";
             
            $repliesresults = $wpdb->get_results($querystr, OBJECT);
            foreach ($repliesresults as $k=>$v) {
              $topic_query_vars = array( 'topic_id'=> $v->topic_id, 'topic_status' => 'normal', 'open' => 'open');
              $topic_query = new BB_Query_Form( 'topic', $topic_query_vars );
              $topics = $topic_query->results;
              if (is_array($topics)) {
                foreach ( $topics as $topic ) : $first_post = bb_get_first_post( $topic );
                  /* pagination fix */
                  $_qs = "SELECT post_id as id FROM ".$wpdb->prefix."bb_posts WHERE topic_id=".$v->topic_id;
                  $_posts = $wpdb->get_results($_qs, OBJECT);
                  $_count = 0; 
                  foreach ($_posts as $_post) {
                    $_count++;
                    if ($_post->id==$v->id) break;
                  }
                  $_bbp_post_count = get_option("_bbp_replies_per_page", 15);
                  $_page = intval(($_count-1) / $_bbp_post_count);
                  if ($_page>0) {
                    $_page = "?topic_page=".($_page+1);
                  } else {
                    $_page = "";
                  }
                  $repliesresults[$k]->link = get_bloginfo('url')."/groups/".$topic->object_slug."/forum/topic/".$topic->topic_slug."/".$_page."#post-".$v->id;
                endforeach;
              }           
              if ($search['data']['settings-imagesettings']['show']==1) {
                $im =   $repliesresults[$k]->content;
                $img = new wpdreamsImageCache($im, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], 1, $search['data']['imagebg']);
                $res = $img->get_image();
                if ($res!='') {
                  $repliesresults[$k]->image = plugins_url('/cache/'.$res , __FILE__);
                }
              }
             if ($repliesresults[$k]->content!='')
              $repliesresults[$k]->content = substr(strip_tags($repliesresults[$k]->content), 0, $search['data']['descriptionlength'])."...";
             $repliesresults[$k]->title = substr(strip_tags($repliesresults[$k]->title), 0, 30)."...";
            }
          }
      }
      /*
        BuddyPress Section END
      */
      
      if (isset($search['data']['searchinblogtitles']) && $search['data']['searchinblogtitles']==1 && is_multisite()) {
        $blog_list = wpdreams_get_blog_list(0, 'all');
        foreach($blog_list as $bk=>$blog) {
          $_det = get_blog_details( $blog['blog_id']);
          $blog_list[$bk]['name'] = $_det->blogname;
          $blog_list[$bk]['siteurl'] = $_det->siteurl;
          $blog_list[$bk]['match'] = 0;
        }

        if (isset($search) && $search['data']['exactonly']!=1) {
          $_s = explode(" ", $s);
          foreach($_s as $keyword) {
            foreach($blog_list as $bk=>$blog) {
              if ($blog_list[$bk]['match']==1) continue;
              $pos = strpos(strtolower($blog['name']), $keyword);
              if ($pos!==false) $blog_list[$bk]['match'] = 1; 
            }
          } 
        }
        foreach($blog_list as $bk=>$blog) {
          if ($blog_list[$bk]['match']==1) continue;
          $pos = strpos(strtolower($blog['name']), $s);
          if ($pos!==false) $blog_list[$bk]['match'] = 1;
        }
        foreach($blog_list as $bk=>$blog) {          
          if ( $blog_list[$bk]['match']==1) {
            $_blogres = new stdClass();
            switch_to_blog($blog['blog_id']);
            $_blogres->title = $blog['name'];
            $_blogres->link = get_bloginfo('siteurl');
            $_blogres->content = get_bloginfo('description');
            $_blogres->author = "";
            $_blogres->date = "";
            $blogresults[] = $_blogres;
          }
        }
        if ($search['data']['selected-blogtitleorderby']=='asc') {
           $blogresults = array_reverse($blogresults);
        }
        restore_current_blog(); 
      }
      //print_r($blogresults);
      // Grouping again
      if ($search['data']['selected-groupby']==1 || $search['data']['selected-groupby']==2) {
      
         $results = $allpageposts;

         if (!isset($results['items']) && count($allpageposts)>0) {
            $results['items'] = array();
            $results['grouped'] = 1;
         }
         
         if (count($blogresults)>0) {
           if ($search['data']['showpostnumber']==1) {
            $num = " (".count($blogresults).")";
           } else {
            $num = "";
           }
           $results['items'][90000] = array();
           $results['items'][90000]['name'] = $search['data']['blogresultstext'].$num;
           $results['items'][90000]['data'] = $blogresults;
           $results['grouped'] = 1;
         }
         
         if (count($repliesresults)>0) {
           if ($search['data']['showpostnumber']==1) {
            $num = " (".count($repliesresults).")";
           } else {
            $num = "";
           }
           $results['items'][90001] = array();
           $results['items'][90001]['name'] = $search['data']['bbpressreplytext'].$num;
           $results['items'][90001]['data'] = $repliesresults;
           $results['grouped'] = 1;
         }
         
         if (count($allcommentsresults)>0) {
           if ($search['data']['showpostnumber']==1) {
            $num = " (".count($allcommentsresults).")";
           } else {
            $num = "";
           }
           $results['items'][90002] = array();
           $results['items'][90002]['name'] = $search['data']['commentstext'].$num;
           $results['items'][90002]['data'] = $allcommentsresults;
           $results['grouped'] = 1;
         }
         
         if (count($groupresults)>0) {
           if ($search['data']['showpostnumber']==1) {
            $num = " (".count($groupresults).")";
           } else {
            $num = "";
           }
           $results['items'][90003] = array();
           $results['items'][90003]['name'] = $search['data']['bbpressgroupstext'].$num;
           $results['items'][90003]['data'] = $groupresults;
           $results['grouped'] = 1;
         }
         
         if (count($userresults)>0) {
           if ($search['data']['showpostnumber']==1) {
            $num = " (".count($userresults).")";
           } else {
            $num = "";
           }
           $results['items'][90004] = array();
           $results['items'][90004]['name'] = $search['data']['bbpressuserstext'].$num;
           $results['items'][90004]['data'] = $userresults;
           $results['grouped'] = 1;
         }
         
      } else {
         $results = array_merge($blogresults, $repliesresults, $allcommentsresults, $groupresults, $userresults, $allpageposts); 
      }

      if (count($results)<=0 && $search['data']['keywordsuggestions']) {
        $t = new keywordSuggest($search['data']['keywordsuggestionslang']);
        $keywords = $t->getKeywords($s);
        if ($keywords!=false) {
          $results['keywords'] = $keywords;
          $results['nores'] = 1;    
        }
      }
      if (get_option('asp_caching'))
        $cache_content = $textcache->setCache(json_encode($results)); 
      print_r(json_encode($results));
      //restore_error_handler();
      die();
    /*} catch (ErrorException $e) {
      $results['error'] = "There is an error: ".$e->getMessage()." in ".$e->getFile()." on line ".$e->getLine().". ";
      $results['error'].= 'Please contact the developer for support!';
      print_r(json_encode($results));
      die();
    }*/
  }

  function ajaxsearchpro_autocomplete() {
      global $wpdb;
      $s = trim($_POST['sauto']);
      $s= preg_replace( '/\s+/', ' ', $s);
      //$s = wpdreams_utf8safeencode($s, $delimiter);
      $search = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."ajaxsearchpro WHERE id=".$_POST['asid'], ARRAY_A);
      $search['data'] = json_decode($search['data'], true);
      if (!isset($search['data']['selected-autocompletesource'])) $search['data']['selected-autocompletesource'] = 0; 
      if ($search['data']['selected-autocompletesource']==0) {
        $keywords = array();
        $_keywords = $wpdb->get_results("SELECT keyword FROM ".$wpdb->prefix."ajaxsearchpro_statistics WHERE keyword LIKE '".$s."%' ORDER BY num desc", ARRAY_A);
        foreach($_keywords as $k=>$v) {
          $keywords[] = $v['keyword'];
        }
        $banned = explode(',', $search['data']['autocompleteexceptions']);
        if (is_array($banned)) {
          foreach ($banned as $k=>$v) {
            $banned[$k] = trim(preg_replace( '/\s+/', ' ', $v));
          }
        }
        if (!is_array($keywords)) $keywords = array();
        if (!is_array($banned)) $keywords = array();
        $possible = array_diff($keywords, $banned);
        if (is_array($possible)) {
          print reset($possible);
          die();
        } else {
          print "";
          die();
        }
      } else if ($search['data']['selected-autocompletesource']==1) {
        $t = new keywordSuggest();
        $keywords = $t->getKeywords($s);
        if ($keywords==false) {
          print '';
          die();   
        } else {
          print $keywords[0];
          die();
        }      
      }
  }
  
  function ajaxsearchpro_preview() {
    $out = add_ajaxsearchpro(array("id"=>$_POST['asid']));
    print $out;
    die();
  }
  
  
  /*
  * Precaching images, with max 10 sec of execution time
  */
  function ajaxsearchpro_precache() {
      global $wpdb;
      if (get_option('asp_precacheimages')!=1) return;
      if (isset($wpdb->base_prefix)) {
        $_prefix = $wpdb->base_prefix;
      } else {
        $_prefix = $wpdb->prefix;
      }   
      $start = microtime(true);
    	$querystr = "
    		SELECT 
          $wpdb->posts.post_title as title,
          $wpdb->posts.ID as id,
          $wpdb->posts.post_date as date,
          $wpdb->posts.post_content as content,
          $wpdb->posts.post_excerpt as excerpt,
          $wpdb->posts.post_type as post_type
    		FROM $wpdb->posts
    		WHERE
        $wpdb->posts.post_status='publish'
    	 ";
      $pageposts = $wpdb->get_results($querystr, OBJECT);
      $searches = $wpdb->get_results("SELECT * FROM ".$_prefix."ajaxsearchpro", ARRAY_A);
      if (is_array($searches)) {
        foreach ($searches as $search) {        
          $search['data'] = json_decode($search['data'], true); 
          if ($search['data']['selected-imagesettings']['show']==1) {
            $imgs = $search['data']['selected-imagesettings'];
            if (is_array($pageposts)) {
              foreach( $pageposts as $_post ) {
                  $im = get_the_post_thumbnail($post->id);  
                  $img = new wpdreamsImageCache($im, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], 1, $search['data']['imagebg']);
                  $img = new wpdreamsImageCache($_post->content, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], $imgs['imagenum'], $search['data']['imagebg']);
                  $img = new wpdreamsImageCache($_post->excerpt, AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR, $imgs['width'], $imgs['height'], $imgs['imagenum'], $search['data']['imagebg']);
                  $current = microtime(true);
                  $runtime = $current-$start;
                  if ($runtime>9) {
                    echo "Ran: ".$runtime." - Interrupted.";
                    exit;
                  }
              }
            }
          }
        }
        $current = microtime(true);
        $runtime = $current-$start;
        echo "Ran: ".$runtime;
        exit;
      }
  }
  
  /* Delete the cache */
  function ajaxsearchpro_deletecache() {
    $count = 0;
    function delTree($dir) {
        $files = @glob( $dir . '*', GLOB_MARK );
        foreach( $files as $file ){
            if( substr( $file, -1 ) == '/' )
                @delTree( $file );
            else
                @unlink( $file );
            $count++;
        }
        return $count;
    }
    print delTree(AJAXSEARCHPRO_PATH.DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR);
    exit;
  }
  
  /**
   * This function will supress the warnings and throw an exception instead.
   */
  if (!function_exists('handleError')) {
    function handleError($errno, $errstr, $errfile, $errline, array $errcontext) {
          if (0 === error_reporting()) {
              return false;
          }
          throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    } 
  }
?>