=== AdRotate ===
Contributors: adegans
Donate link: http://meandmymac.net/donate/
Tags: ads, banner, commercial, admin, plugin, random, rotator, ad, advertising, advertisement, block, blocks, advertiser, publisher, adsense, geo, geolocation, chitika, clickbank, broadstreet, export, import, freegeoip, geoplugin, maxmind, referrer, email, menu, statistics, stats
Requires at least: 3.7
Tested up to: 3.9.1
Stable tag: 3.9.14
 
Make it easy on yourself and use AdRotate to place your adverts on your site... Make making money easy!

== Description ==

Get the most extensive ad manager and start making money with your website.
Add and manage the ads from the dashboard and show a random banner, or multiple, on your site. 
Easy management from the dashboard allows you to quickly oversee, add and edit banner code or renew/delete and add new ads.
Preview banners when editing them.

AdRotate supports unlimited groups for banners allowing you to tie certain banners to different areas of the website.
On top of that with GeoLocation you can tie adverts to certain areas of the world.

AdRotate makes managing your adverts easy with it's easy to use dashboard. AdRotate looks and feels similar to the dashboard you already know, so you're already familiar with AdRotate before you've even started. The well thought out menu structures and features make advert management very easy and straightforward.

AdRotate is also available as a premium plugin for a low price. Offering extra and more extensive features and faster support! 
Check out the benefits of AdRotate Pro! [Get AdRotate Pro today](http://www.adrotateplugin.com/adrotate-pro/)!

**Important links**

* [Details](http://www.adrotateplugin.com) - Lots of Additional plugin information
* [Manuals & Support](http://www.adrotateplugin.com/support/) - Setup instructions and support
* [AdRotate Store](http://www.adrotateplugin.com/shop/) - Buy AdRotate Pro or hire me to install or troubleshoot AdRotate

**Some Features**

* Super easy management of ads and groups of ads
* Automated Javascript cycles of ads with Dynamic Groups
* Have your advertisers add/edit/manage their own ads
* Geo location for adverts
* Detect ad blockers in browser and nag visitors about it
* Works with Google Ads and most other referrer/ad servers
* Get push notifications right on your iOS or Android device about adverts
* Get email notifications when your ads need you
* Perfectly suited for any size of advertisement, including 125x125, 468x60, 729x90, 160x600 and much more
* Simple to use stats so you can follow which ad is working the best, worst
* Daily and monthly stats
* Couple ads to users so that user can follow his personalized stats
* Advertisers can easily contact you from their dashboard
* Track how many times a banner is clicked and show it's Cick-Through-Ratio
* Put random, or selected, banners in pages or posts
* Preview banners on edit
* Advanced time schedules and restrictions
* CSV Exports of statistics
* Multiple groups per banner location
* Unlimited widgets
* Adblocks (blocks of banners)
* Auto disable ads after X views / clicks / period of time
* Warns you on the dashboard when ads are about to expire
* Use shortcodes, widgets or PHP to put ads on your site

NOTE: Certain features are exclusive to AdRotate PRO. Learn more about what AdRotate Pro will do for you in the [features list](http://www.adrotateplugin.com/features/).

== Installation ==

Find out how AdRotate works, more options and various ways to implement your ads.

* [Manual Installation](http://www.adrotateplugin.com/support/knowledgebase/installing-adrotate-on-your-website/)
* [Upgrade AdRotate Free to Pro](http://www.adrotateplugin.com/support/knowledgebase/upgrade-adrotate-to-adrotate-pro/)
* [Manuals & Support](http://www.adrotateplugin.com/support/)
* [AdRotate Store](http://www.adrotateplugin.com/shop/)

== Frequently Asked Questions ==

= I'm getting errors about duplicate entries in the stats or the tracker table! =
**Answer:** This means your database is full and needs to be "upgraded". More on the how and why in [this article](href="http://www.adrotateplugin.com/support/knowledgebase/duplicate-entry-errors/)!

There is a [knowledge base](http://www.adrotateplugin.com/support/knowledgebase/) with every feature explained.
If that's not enough you can post on the [forum](http://www.adrotateplugin.com/support/forums/) and ask your question there!

== Changelog ==

= 3.9.14 FREE =
* AdRotate 3.7.x no longer supported for upgrades
* Blocks of ads no longer supported - Use Groups in block mode instead
* [change] Increased cleanup rate for *_adrotate_tracker table
* [change] Cleaned up the AdRotate Pro notifications a bit
* [change] Improved activation routine for new installs and updates
* [change] Improved status reporting when editing ads with clicktracking
* [new] Menus now have an icon
* [fix] Start/end date saved incorrect for new ads
* [fix] Correct roles shown in AdRotate Settings for new installs
* [fix] Incorrect CSS when adverts size is auto in dynamic or block mode
* [fix] Almost expired adverts not always visible when editing groups
* [fix] Correct bot filter keywords for new installs
* [fix] No longer "updates" AdRotate when activating a new installation
* [fix] Clicks timer can now be 0
* [fix] Clicktracking inaccurate if click timer is less than 12 hours
* [fix] Ambiguous check for saving post injection setting
* [fix] Incorrect paragraph detection with post injection in some cases

NOTE: Certain features are exclusive to AdRotate PRO. If you need these features please consider upgrading to [AdRotate PRO](http://www.adrotateplugin.com/features/).

= 3.9.14 PRO =
* AdRotate 3.7.x no longer supported for upgrades
* Blocks of ads no longer supported - Use Groups in block mode instead
* [change] Removed unreliable 'last 50 clicks' from Global report
* [change] Increased cleanup rate for *_adrotate_tracker table
* [change] Redesigned group editing page for a better workflow
* [change] Improved activation routine for new installs and updates
* [change] Improved status reporting when editing ads with clicktracking
* [new] Menus now have an icon
* [new] Select adverts when creating/editing schedules
* [fix] Almost expired adverts not always visible when editing groups
* [fix] Incorrect CSS when adverts size is auto in dynamic or block mode
* [fix] Correct bot filter keywords for new installs
* [fix] No longer "updates" AdRotate when activating a new installation
* [fix] Clicks timer can now be 0
* [fix] Clicktracking inaccurate if click timer is less than 12 hours
* [fix] Ambiguous check for saving post injection setting
* [fix] Incorrect paragraph detection with post injection in some cases

The full changelog can be found on the [AdRotate website](http://www.adrotateplugin.com/development/).
Get more AdRotate! [Get AdRotate Pro today](http://www.adrotateplugin.com/adrotate-pro/)!

== Upgrade Notice ==

= 3.9.14 FREE =
* AdRotate 3.7.x no longer supported for upgrades
* Blocks of ads no longer supported - Use Groups in block mode instead
* [change] Increased cleanup rate for *_adrotate_tracker table
* [change] Cleaned up the AdRotate Pro notifications a bit
* [change] Improved activation routine for new installs and updates
* [change] Improved status reporting when editing ads with clicktracking
* [new] Menus now have an icon
* [fix] Start/end date saved incorrect for new ads
* [fix] Correct roles shown in AdRotate Settings for new installs
* [fix] Incorrect CSS when adverts size is auto in dynamic or block mode
* [fix] Almost expired adverts not always visible when editing groups
* [fix] Correct bot filter keywords for new installs
* [fix] No longer "updates" AdRotate when activating a new installation
* [fix] Clicks timer can now be 0
* [fix] Clicktracking inaccurate if click timer is less than 12 hours
* [fix] Ambiguous check for saving post injection setting
* [fix] Incorrect paragraph detection with post injection in some cases

The full changelog can be found on the [AdRotate website](http://www.adrotateplugin.com/development/).
Get more AdRotate! [Get AdRotate Pro today](http://www.adrotateplugin.com/adrotate-pro/)!

NOTE: Certain features are exclusive to AdRotate PRO. If you need these features please consider upgrading to [AdRotate PRO](http://www.adrotateplugin.com/features/).

== Screenshots ==

Visit [adrotateplugin.com](http://www.adrotateplugin.com/)