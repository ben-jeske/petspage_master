<?php
/**
 * Plugin Name: myCRED Notifications Plus
 * Plugin URI: http://mycred.me/add-ons/notifications-plus/
 * Description: Overrides the default myCRED Notifications add-on and adds further customization options. Remember to disable the built-in notifications in myCRED before enabling this plugin!
 * Version: 1.2.1
 * Author: Gabriel Sebastian Merovingi
 * Author URI: http://www.merovingi.com
 * Text Domain: mycred_notice
 * Domain Path: /lang
 * License: Copyrighted
 *
 * Copyright © 2013 Gabriel S Merovingi
 * 
 * Permission is hereby granted, to the licensed domain to install and run this
 * software and associated documentation files (the "Software") for an unlimited
 * time with the followning restrictions:
 *
 * - This software is only used under the domain name registered with the purchased
 *   license though the myCRED website (mycred.me). Exception is given for localhost
 *   installations or test enviroments.
 *
 * - This software can not be copied and installed on a website not licensed.
 *
 * - This software is supported only if no changes are made to the software files
 *   or documentation. All support is voided as soon as any changes are made.
 *
 * - This software is not copied and re-sold under the current brand or any other
 *   branding in any medium or format.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
define( 'MYCRED_NOTICE_VERSION',     '1.2.1' );
define( 'MYCRED_NOTICE_JS_VERSION',  MYCRED_NOTICE_VERSION . '.1' );
define( 'MYCRED_NOTICE_CSS_VERSION', MYCRED_NOTICE_VERSION . '.1' );

define( 'MYCRED_NOTICE_SLUG',       'mycred-notice-plus' );
define( 'myCRED_NOTICE',             __FILE__ );
define( 'MYCRED_NOTICE_ROOT_DIR',    plugin_dir_path( myCRED_NOTICE ) );
define( 'MYCRED_NOTICE_ASSETS_DIR',  MYCRED_NOTICE_ROOT_DIR . 'assets/' );

/**
 * myCRED_Notice_Plus_Plugin class
 * @since 1.0
 * @version 1.1
 */
if ( ! class_exists( 'myCRED_Notice_Plus_Plugin' ) ) {
	class myCRED_Notice_Plus_Plugin {

		/**
		 * Construct
		 */
		function __construct() {
			add_action( 'mycred_pre_init',                       array( $this, 'mycred_pre_init' ) );
			register_activation_hook( myCRED_NOTICE,             array( $this, 'activate_mycred_notifications' ) );
			add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_for_plugin_update' ), 14 );
			add_filter( 'plugins_api',                           array( $this, 'plugin_api_call' ), 14, 3 );
		}

		/**
		 * Load Translation
		 * @since 1.0
		 * @version 1.1.2
		 */
		function mycred_pre_init() {
			// Load Translation
			load_plugin_textdomain( 'mycred_notice', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
		}

		/**
		 * Activate
		 * @since 1.0
		 * @version 1.0.1
		 */
		function activate_mycred_notifications() {
			global $wpdb;

			$message = array();
			// WordPress check
			$wp_version = $GLOBALS['wp_version'];
			if ( version_compare( $wp_version, '3.1', '<' ) )
				$message[] = __( 'This myCRED Add-on requires WordPress 3.1 or higher. Version detected:', 'mycred_notice' ) . ' ' . $wp_version;

			// PHP check
			$php_version = phpversion();
			if ( version_compare( $php_version, '5.2.4', '<' ) )
				$message[] = __( 'This myCRED Add-on requires PHP 5.2.4 or higher. Version detected: ', 'mycred_notice' ) . ' ' . $php_version;

			// SQL check
			$sql_version = $wpdb->db_version();
			if ( version_compare( $sql_version, '5.0', '<' ) )
				$message[] = __( 'This myCRED Add-on requires SQL 5.0 or higher. Version detected: ', 'mycred_notice' ) . ' ' . $sql_version;

			// Not empty $message means there are issues
			if ( ! empty( $message ) ) {
				$error_message = implode( "\n", $message );
				die( __( 'Sorry but your WordPress installation does not reach the minimum requirements for running this add-on. The following errors were given:', 'mycred_notice' ) . "\n" . $error_message );
			}
		}
		
		/**
		 * Plugin Update Check
		 * @since 1.0
		 * @version 1.0
		 */
		function check_for_plugin_update( $checked_data ) {
			global $wp_version;

			if ( empty( $checked_data->checked ) )
				return $checked_data;

			$args = array(
				'slug'    => MYCRED_NOTICE_SLUG,
				'version' => $checked_data->checked[ MYCRED_NOTICE_SLUG . '/' . MYCRED_NOTICE_SLUG . '.php' ],
				'site'    => site_url()
			);
			$request_string = array(
				'body'       => array(
					'action'  => 'basic_check', 
					'request' => serialize( $args ),
					'api-key' => md5( get_bloginfo( 'url' ) )
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
			);

			// Start checking for an update
			$raw_response = wp_remote_post( 'http://mycred.me/api/plugins/', $request_string );

			$response = '';
			if ( ! is_wp_error( $raw_response ) && ( $raw_response['response']['code'] == 200 ) )
				$response = maybe_unserialize( $raw_response['body'] );

			if ( is_object( $response ) && ! empty( $response ) )
				$checked_data->response[ MYCRED_NOTICE_SLUG . '/' . MYCRED_NOTICE_SLUG . '.php' ] = $response;

			return $checked_data;
		}

		/**
		 * Plugin New Version Update
		 * @since 1.0
		 * @version 1.0
		 */
		function plugin_api_call( $def, $action, $args ) {
			global $wp_version;

			if ( ! isset( $args->slug ) || ( $args->slug != MYCRED_NOTICE_SLUG ) )
				return false;

			// Get the current version
			$plugin_info = get_site_transient( 'update_plugins' );
			$args = array(
				'slug'    => MYCRED_NOTICE_SLUG,
				'version' => $plugin_info->checked[ MYCRED_NOTICE_SLUG . '/' . MYCRED_NOTICE_SLUG . '.php' ],
				'site'    => site_url()
			);
			$request_string = array(
				'body'       => array(
					'action'  => 'basic_check', 
					'request' => serialize( $args ),
					'api-key' => md5( get_bloginfo( 'url' ) )
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
			);

			$request = wp_remote_post( 'http://mycred.me/api/plugins/', $request_string );

			if ( is_wp_error( $request ) ) {
				$res = new WP_Error( 'plugins_api_failed', __( 'An Unexpected HTTP Error occurred during the API request. <a href="?" onclick="document.location.reload(); return false;">Try again</a>', 'mycred_notice' ), $request->get_error_message() );
			}
			else {
				$res = maybe_unserialize( $request['body'] );
				if ( $res === false )
					$res = new WP_Error( 'plugins_api_failed', __( 'An unknown error occurred', 'mycred_notice' ), $request['body'] );
			}

			return $res;
		}
	}
	new myCRED_Notice_Plus_Plugin();
}

/**
 * myCRED_Notifications class
 *
 * @since 1.0
 * @version 1.0.1
 */
add_action( 'plugins_loaded', 'mycred_notice_plus', 25 );
function mycred_notice_plus() {
	if ( ! class_exists( 'myCRED_Module' ) ) return;

	if ( ! class_exists( 'myCRED_Notifications' ) ) {
		class myCRED_Notifications extends myCRED_Module {

			/**
			 * Construct
			 */
			function __construct() {
				parent::__construct( 'myCRED_Notifications', array(
					'module_name' => 'notifications',
					'defaults'    => array(
						'life'      => 7,
						'template'  => '<p>%entry%</p><h1>%cred_f%</h1>',
						'use_css'   => 1,
						'duration'  => 3000,
						'position'  => 'top-left',
						'padding'   => 50,
						'colors'    => array(
							'bg'        => '#dedede',
							'border'    => '#dedede',
							'text'      => '#333333',
							'negative'  => 0,
							'nbg'       => '#333333',
							'nborder'   => '#333333',
							'ntext'     => '#dedede'
						),
						'border'    => array(
							'width'     => 1,
							'radius'    => 5
						),
						'css'       => '.notice-item { padding: 12px; line-height: 22px; font-size: 12px; border-style: solid; }
.notice-item p { display: block; float: right; margin: 0; padding: 0; }
.notice-item h1 { margin: 0 !important; padding: 0; }',
						'instant'   => array(
							'use'       => 0,
							'check'     => 15
						),
						'types'     => array( 'mycred_default' )
					),
					'register'    => false,
					'add_to_core' => true
				) );
				add_filter( 'mycred_add', array( $this, 'mycred_add' ), 99, 2 );
			}
		
			/**
			 * Module Init
			 * @since 1.0
			 * @version 1.1
			 */
			public function module_init() {
				if ( ! is_user_logged_in() ) return;
			
				add_action( 'mycred_front_enqueue', array( $this, 'register_assets' ) );
				add_action( 'wp_footer',            array( $this, 'get_notices' ), 1 );
				add_action( 'wp_footer',            array( $this, 'wp_footer' ), 99 );
				add_action( 'mycred_admin_enqueue', array( $this, 'admin_enqueue' ) );
				
				if ( (bool) $this->notifications['instant']['use'] )
					add_action( 'wp_head', array( $this, 'wp_header' ) );

				add_action( 'wp_ajax_mycred-inotify', array( $this, 'instant_notification' ) );
			}

			/**
			 * Load Instnat Notifications Script
			 * @since 1.1
			 * @version 1.1
			 */
			public function wp_header() {
				$freq = abs( $this->notifications['instant']['check']*1000 );

				if ( (bool) $this->notifications['use_css'] )
					wp_enqueue_style( 'mycred-notifications' );

				$this->load_style();
				
				wp_enqueue_script( 'mycred-notifications' );
				wp_localize_script(
					'mycred-notifications',
					'myCRED_Notice',
					array(
						'ajaxurl'   => admin_url( 'admin-ajax.php' ),
						'duration'  => $this->notifications['duration'],
						'frequency' => $freq,
						'token'     => wp_create_nonce( 'mycred-instant-notice' ),
						'user_id'   => get_current_user_id()
					)
				);
				wp_enqueue_script( 'mycred-instant-notifications' );
			}

			/**
			 * Load Notice in Footer
			 * @since 1.0
			 * @version 1.2
			 */
			public function wp_footer() {
				$notices = apply_filters( 'mycred_notifications', array() );
				if ( empty( $notices ) ) return;

				if ( (bool) $this->notifications['use_css'] )
					$this->load_style();

				if ( $this->notifications['duration'] == 0 )
					$stay = 'true';
				else
					$stay = 'false';

				do_action_ref_array( 'mycred_before_notifications', array( &$notices ) );
				foreach ( (array) $notices as $notice ) {
					// Get color first
					$color = substr( $notice, 0, 1 );
					if ( $color == '<' )
						$type = 'negative';
					else
						$type = 'positive';
					
					// Parse The actual message
					$notice = substr( $notice, 1 );
					$notice = $this->core->template_tags_general( $notice );
					$notice = str_replace( array( "\r", "\n", "\t" ), '', $notice );
					echo '<script type="text/javascript">(function(jQuery){jQuery.noticeAdd({ text: "' . $notice . '", stay: ' . $stay . ', type: "' . $type . '"});})(jQuery);</script>';
				}
				do_action_ref_array( 'mycred_after_notifications', array( &$notices ) );
			}
		
			/**
			 * Load Styling
			 * @since 1.0
			 * @version 1.2
			 */
			public function load_style() {
				echo '
<style type="text/css">
.notice-item {
background-color: ' . $this->notifications['colors']['bg'] . ';
color: ' . $this->notifications['colors']['text'] . ';
border-color: ' . $this->notifications['colors']['border'] . ';
border-width: ' . $this->notifications['border']['width'] . 'px;
border-radius: ' . $this->notifications['border']['radius'] . 'px;';

				if ( in_array( $this->notifications['position'], array( 'top-left', 'top-right' ) ) )
					echo 'margin: 0 0 24px 0;';
				else
					echo 'margin: 24px 0 0 0;';

				echo '
}
.negative.notice-item {
background-color: ' . $this->notifications['colors']['nbg'] . ';
color: ' . $this->notifications['colors']['ntext'] . ';
border-color: ' . $this->notifications['colors']['nborder'] . ';
}
.notice-wrap {';
			
				$padding = $this->notifications['padding'];
				if ( $this->notifications['position'] == 'top-left' )
					echo 'top: ' . $padding . 'px;left: ' . $padding . 'px;';

				elseif ( $this->notifications['position'] == 'top-right' )
					echo 'top: ' . $padding . 'px;right: ' . $padding . 'px;';

				elseif ( $this->notifications['position'] == 'bottom-left' )
					echo 'bottom: ' . $padding . 'px;left: ' . $padding . 'px;';

				elseif ( $this->notifications['position'] == 'bottom-right' )
					echo 'bottom: ' . $padding . 'px;right: ' . $padding . 'px;';

				echo '} ' . $this->notifications['css'] . '
</style>';
			}
		
			/**
			 * Register Assets
			 * @since 1.0
			 * @version 1.1
			 */
			public function register_assets() {
				// Register Styling
				if ( (bool) $this->notifications['use_css'] )
					wp_register_style(
						'mycred-notifications',
						plugins_url( 'assets/css/notify.css', myCRED_NOTICE ),
						false,
						MYCRED_NOTICE_CSS_VERSION . '.2',
						'all',
						true
					);
				
				// Register script
				wp_register_script(
					'mycred-notifications',
					plugins_url( 'assets/js/notify.js', myCRED_NOTICE ),
					array( 'jquery' ),
					MYCRED_NOTICE_JS_VERSION . '.1',
					true
				);
				
				// Register instant script
				if ( is_user_logged_in() )
					wp_register_script(
						'mycred-instant-notifications',
						plugins_url( 'assets/js/inotify.js', myCRED_NOTICE ),
						array( 'jquery' ),
						MYCRED_NOTICE_JS_VERSION . '.1'
					);
			}
		
			/**
			 * myCRED Add
			 * @since 1.0
			 * @version 1.2.1
			 */
			public function mycred_add( $reply, $request ) {
				if ( $reply === false ) return $reply;

				if ( $request['type'] == '' )
					$request['type'] = 'mycred_default';

				if ( isset( $this->notifications['types'] ) && ! in_array( $request['type'], $this->notifications['types'] ) )
					return $reply;

				$mycred = mycred( $request['type'] );

				$template = $this->notifications['template'];
				$template = str_replace( '%entry%', $request['entry'], $template );
				$template = str_replace( '%amount%', $request['amount'], $template );
				$template = $mycred->template_tags_amount( $template, $request['amount'] );
				$template = $mycred->parse_template_tags( $template, (object) $request );
				$template = apply_filters( 'mycred_notifications_note', $template, $request, $mycred );
			
				$color = '>';
				if ( $request['amount'] < 0 )
					$color = '<';
			
				if ( ! empty( $template ) )
					mycred_add_new_notice( array(
						'user_id' => $request['user_id'],
						'message' => $template,
						'color'   => $color
					), $this->notifications['life'] );

				return $reply;
			}
			
			/**
			 * AJAX: Instant Notification Check
			 * @since 1.1
			 * @version 1.2
			 */
			public function instant_notification() {
				// Security
				check_ajax_referer( 'mycred-instant-notice', 'token' );
				
				// Get notices
				$this->get_notices();
				
				$notices = apply_filters( 'mycred_notifications', array() );
				if ( empty( $notices ) ) die( 0 );

				if ( $this->notifications['duration'] == 0 )
					$stay = 'true';
				else
					$stay = 'false';

				$_notices = array();
				
				do_action_ref_array( 'mycred_before_notifications', array( &$notices ) );
				foreach ( (array) $notices as $notice ) {
					// Get color first
					$color = substr( $notice, 0, 1 );
					if ( $color == '<' )
						$type = 'negative';
					else
						$type = 'positive';
					
					// Parse The actual message
					$notice = substr( $notice, 1 );
					$notice = $this->core->template_tags_general( $notice );
					$notice = str_replace( array( "\r", "\n", "\t" ), '', $notice );
					$_notices[] = array(
						'text' => $notice,
						'stay' => $stay,
						'type' => $type
					);
				}
				do_action_ref_array( 'mycred_after_notifications', array( &$notices ) );
				
				die( json_encode( $_notices ) );
			}
		
			/**
			 * Get Notices
			 * @since 1.0
			 * @version 1.0
			 */
			public function get_notices() {
				$user_id = get_current_user_id();
				$data = get_transient( 'mycred_notice_' . $user_id );
			
				if ( $data === false || ! is_array( $data ) ) return;
			
				if ( (bool) $this->notifications['use_css'] )
					wp_enqueue_style( 'mycred-notifications' );

				if ( ! (bool) $this->notifications['instant']['use'] ) {
					wp_enqueue_script( 'mycred-notifications' );
					wp_localize_script(
						'mycred-notifications',
						'myCRED_Notice',
						array(
							'ajaxurl'  => admin_url( 'admin-ajax.php' ),
							'duration' => $this->notifications['duration']
						)
					);
				}
			
				foreach ( $data as $notice ) {
					add_filter( 'mycred_notifications', create_function( '$query', '$query[]=\'' . $notice . '\'; return $query;' ) );
				}
			
				delete_transient( 'mycred_notice_' . $user_id );
			}

			/**
			 * Add Color Picker
			 * @since 1.0
			 * @version 1.0
			 */
			public function admin_enqueue() {
				$screen = get_current_screen();
				if ( $screen->id != 'mycred_page_myCRED_page_settings' ) return;
				wp_enqueue_style( 'wp-color-picker' );
				wp_enqueue_script(
					'mycred-notifications-admin',
					plugins_url( 'assets/js/admin.js', myCRED_NOTICE ),
					array( 'wp-color-picker' ),
					false,
					true
				);
			}
		
			/**
			 * Settings Page
			 * @since 1.0
			 * @version 1.1
			 */
			public function after_general_settings() {
				global $mycred_types;
				$settings = $this->notifications;
				if ( ! isset( $settings['types'] ) )
					$settings['types'] = array( 'mycred_default' ); ?>

<h4><div class="icon icon-active"></div><?php _e( 'Notifications Plus', 'mycred_notice' ); ?></h4>
<div class="body" style="display:none;">
	<label class="subheader"><?php _e( 'Styling', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<input type="checkbox" name="mycred_pref_core[notifications][use_css]" id="myCRED-notifications-use-css" <?php checked( $settings['use_css'], 1 ); ?> value="1" />
			<label for="myCRED-notifications-use-css"><?php _e( 'Use the included CSS Styling for notifications.', 'mycred_notice' ); ?></label>
		</li>
	</ol>
	<?php if ( count( $mycred_types ) > 1 ) : ?>
	<label class="subheader"><?php _e( 'Point Types', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<?php foreach ( $mycred_types as $type => $label ) { if ( $type == 'mycred_default' ) $label = $this->core->plural(); ?>
			<label><input type="checkbox" name="mycred_pref_core[notifications][types][]" id="myCRED-notifications-type-<?php echo $type; ?>" value="<?php echo $type; ?>"<?php if ( in_array( $type, $settings['types'] ) ) echo ' checked="checked"'; ?> /> <?php echo $label; ?></label><br />
			<?php } ?>
		</li>
	</ol>
	<?php else : ?>
	<input type="hidden" name="mycred_pref_core[notifications][types][]" value="mycred_default" />
	<?php endif; ?>
	<label class="subheader"><?php _e( 'Position', 'mycred_notice' ); ?></label>
	<ol class="inline">
		<li>
			<img src="<?php echo plugins_url( 'assets/images/top-left.png', myCRED_NOTICE ); ?>" alt="" /><br />
			<input type="radio" name="mycred_pref_core[notifications][position]" id="myCRED-notifications-position-top-left" <?php checked( $settings['position'], 'top-left' ); ?> value="top-left" />
			<label for="myCRED-notifications-position-top-left"><?php _e( 'Top Left', 'mycred_notice' ); ?></label>
		</li>
		<li>
			<img src="<?php echo plugins_url( 'assets/images/top-right.png', myCRED_NOTICE ); ?>" alt="" /><br />
			<input type="radio" name="mycred_pref_core[notifications][position]" id="myCRED-notifications-position-top-right" <?php checked( $settings['position'], 'top-right' ); ?> value="top-right" />
			<label for="myCRED-notifications-position-top-right"><?php _e( 'Top Right', 'mycred_notice' ); ?></label>
		</li>
		<li>
			<img src="<?php echo plugins_url( 'assets/images/bottom-left.png', myCRED_NOTICE ); ?>" alt="" /><br />
			<input type="radio" name="mycred_pref_core[notifications][position]" id="myCRED-notifications-position-bottom-left" <?php checked( $settings['position'], 'bottom-left' ); ?> value="bottom-left" />
			<label for="myCRED-notifications-position-bottom-left"><?php _e( 'Bottom Left', 'mycred_notice' ); ?></label>
		</li>
		<li>
			<img src="<?php echo plugins_url( 'assets/images/bottom-right.png', myCRED_NOTICE ); ?>" alt="" /><br />
			<input type="radio" name="mycred_pref_core[notifications][position]" id="myCRED-notifications-position-bottom-right" <?php checked( $settings['position'], 'bottom-right' ); ?> value="bottom-right" />
			<label for="myCRED-notifications-position-bottom-right"><?php _e( 'Bottom Right', 'mycred_notice' ); ?></label>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Padding', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<div class="h2"><input type="text" name="mycred_pref_core[notifications][padding]" id="myCRED-notifications-padding" value="<?php echo $settings['padding']; ?>" class="short" /> px</div>
			<span class="description"><?php _e( 'Number of pixels between the edge of the screen and the notification box.<br />Applied both horizontally and vertically depending on the set position.', 'mycred_notice' ); ?></span>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Border', 'mycred_notice' ); ?></label>
	<ol class="inline">
		<li>
			<label><?php _e( 'Width', 'mycred_notice' ); ?></label>
			<div class="h2"><input type="text" name="mycred_pref_core[notifications][border][width]" id="myCRED-notifications-border-width" value="<?php echo $settings['border']['width']; ?>" class="short" /> px</div>
		</li>
		<li>
			<label><?php _e( 'Radius', 'mycred' ); ?></label>
			<div class="h2"><input type="text" name="mycred_pref_core[notifications][border][radius]" id="myCRED-notifications-border-radius" value="<?php echo $settings['border']['radius']; ?>" class="short" /> px</div>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Colors', 'mycred_notice' ); ?></label>
	<ol class="inline">
		<li>
			<label for="myCRED-notifications-colors-bg"><?php _e( 'Background Color', 'mycred_notice' ); ?></label><br />
			<input type="text" name="mycred_pref_core[notifications][colors][bg]" id="myCRED-notifications-colors-bg" value="<?php echo $settings['colors']['bg']; ?>" class="wp-color-picker-field" data-default-color="#dedede" />
		</li>
		<li>
			<label for="myCRED-notifications-colors-border"><?php _e( 'Border Color', 'mycred_notice' ); ?></label><br />
			<input type="text" name="mycred_pref_core[notifications][colors][border]" id="myCRED-notifications-colors-border" value="<?php echo $settings['colors']['border']; ?>" class="wp-color-picker-field" data-default-color="#dedede" />
		</li>
		<li>
			<label for="myCRED-notifications-colors-text"><?php _e( 'Text Color', 'mycred_notice' ); ?></label><br />
			<input type="text" name="mycred_pref_core[notifications][colors][text]" id="myCRED-notifications-colors-text" value="<?php echo $settings['colors']['text']; ?>" class="wp-color-picker-field" data-default-color="#333333" />
		</li>
	</ol>
	<label class="subheader">&nbsp;</label>
	<ol>
		<li>
			<input type="checkbox" name="mycred_pref_core[notifications][colors][negative]" id="myCRED-notifications-colors-negative-use" <?php checked( $settings['colors']['negative'], 1 ); ?> value="1" /> <label for="myCRED-notifications-colors-negative-use"><?php echo $this->core->template_tags_general( __( 'Use different colors when users loose %plural%.', 'mycred_notice' ) ); ?></label>
		</li>
	</ol>
	<div id="negative-colors" style="display:<?php if ( (bool) $settings['colors']['negative'] ) echo 'block'; else echo 'none'; ?>;">
		<label class="subheader"><?php _e( 'Negative Colors', 'mycred_notice' ); ?></label>
		<ol class="inline">
			<li>
				<label for="myCRED-notifications-neg-colors-bg"><?php _e( 'Background Color', 'mycred_notice' ); ?></label><br />
				<input type="text" name="mycred_pref_core[notifications][colors][nbg]" id="myCRED-notifications-neg-colors-bg" value="<?php echo $settings['colors']['nbg']; ?>" class="wp-color-picker-field" data-default-color="#333333" />
			</li>
			<li>
				<label for="myCRED-notifications-neg-colors-border"><?php _e( 'Border Color', 'mycred_notice' ); ?></label><br />
				<input type="text" name="mycred_pref_core[notifications][colors][nborder]" id="myCRED-notifications-neg-colors-border" value="<?php echo $settings['colors']['nborder']; ?>" class="wp-color-picker-field" data-default-color="#333333" />
			</li>
			<li>
				<label for="myCRED-notifications-neg-colors-text"><?php _e( 'Text Color', 'mycred_notice' ); ?></label><br />
				<input type="text" name="mycred_pref_core[notifications][colors][ntext]" id="myCRED-notifications-neg-colors-text" value="<?php echo $settings['colors']['ntext']; ?>" class="wp-color-picker-field" data-default-color="#dedede" />
			</li>
		</ol>
	</div>
	<label class="subheader"><?php _e( 'Custom CSS', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<textarea name="mycred_pref_core[notifications][css]" id="myCRED-notifications-css" rows="5" style="width: 50%;"><?php echo $settings['css']; ?></textarea>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Template', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<textarea name="mycred_pref_core[notifications][template]" id="myCRED-notifications-template" rows="5" style="width: 50%;" /><?php echo $settings['template']; ?></textarea><br />
			<span class="description"><?php _e( 'Use %entry% to show the log entry in the notice and %amount% for the amount.', 'mycred_notice' ); ?></span>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Transient Lifespan', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<div class="h2"><input type="text" name="mycred_pref_core[notifications][life]" id="myCRED-notifications-life" value="<?php echo $settings['life']; ?>" class="short" /></div>
			<span class="description"><?php _e( 'The number of days a users notification is saved before being automatically deleted.', 'mycred_notice' ); ?></span>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Duration', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<div class="h2"><input type="text" name="mycred_pref_core[notifications][duration]" id="myCRED-notifications-duration" value="<?php echo $settings['duration']; ?>" class="short" /></div>
			<span class="description"><?php _e( 'The number of milliseconds a notice should be visible.<br />Use zero to require that the user closes the notice manually. 1000 milliseconds = 1 second.', 'mycred_notice' ); ?></span>
		</li>
	</ol>
	<label class="subheader"><?php _e( 'Instant Notifications', 'mycred_notice' ); ?></label>
	<ol>
		<li>
			<input type="checkbox" name="mycred_pref_core[notifications][instant][use]" id="myCRED-notifications-instant-use" <?php checked( $settings['instant']['use'], 1 ); ?> value="1" /> <label for="myCRED-notifications-instant-use"><?php _e( 'Enable Instant Notifications', 'mycred_notice' ); ?></label>
		</li>
	</ol>
	<div id="instant-notifications" style="display:<?php if ( (bool) $settings['instant']['use'] ) echo 'block'; else echo 'none'; ?>;">
		<label class="subheader" for="myCRED-notifications-instant-check"><?php _e( 'Update Frequency', 'mycred_notice' ); ?></label>
		<ol>
			<li>
				<div class="h2"><input type="text" name="mycred_pref_core[notifications][instant][check]" id="myCRED-notifications-instant-check" value="<?php echo $settings['instant']['check']; ?>" class="short" /></div>
				<span class="description"><?php _e( 'How often should this add-on check for new notifications in seconds? While it is possible to update every second, it is not recommended for large sites!', 'mycred_notice' ); ?></span>
			</li>
		</ol>
	</div>
</div>
<?php
			}
		
			/**
			 * Sanitize & Save Settings
			 * @since 1.0
			 * @version 1.1
			 */
			public function sanitize_extra_settings( $new_data, $data, $general ) {
				$new_data['notifications']['use_css'] = ( isset( $data['notifications']['use_css'] ) ) ? 1: 0;
			
				if ( ! isset( $data['notifications']['position'] ) )
					$new_data['notifications']['position'] = 'top-right';
				else
					$new_data['notifications']['position'] = $data['notifications']['position'];

				if ( ! isset( $data['notifications']['types'] ) )
					$new_data['notifications']['types'] = array( 'mycred_default' );
				else
					$new_data['notifications']['types'] = $data['notifications']['types'];

				$new_data['notifications']['padding'] = abs( $data['notifications']['padding'] );
				if ( empty( $new_data['notifications']['padding'] ) )
					$new_data['notifications']['padding'] = 50;

				$new_data['notifications']['border']['width'] = sanitize_text_field( $data['notifications']['border']['width'] );
				$new_data['notifications']['border']['radius'] = sanitize_text_field( $data['notifications']['border']['radius'] );
			
				$new_data['notifications']['colors']['bg'] = sanitize_text_field( $data['notifications']['colors']['bg'] );
				$new_data['notifications']['colors']['border'] = sanitize_text_field( $data['notifications']['colors']['border'] );
				$new_data['notifications']['colors']['text'] = sanitize_text_field( $data['notifications']['colors']['text'] );
				
				$new_data['notifications']['colors']['negative'] = ( isset( $data['notifications']['colors']['negative'] ) ) ? 1 : 0;
				$new_data['notifications']['colors']['nbg'] = sanitize_text_field( $data['notifications']['colors']['nbg'] );
				$new_data['notifications']['colors']['nborder'] = sanitize_text_field( $data['notifications']['colors']['nborder'] );
				$new_data['notifications']['colors']['ntext'] = sanitize_text_field( $data['notifications']['colors']['ntext'] );
				
				$new_data['notifications']['css'] = sanitize_text_field( $data['notifications']['css'] );
			
				$new_data['notifications']['template'] = trim( $data['notifications']['template'] );
				$new_data['notifications']['life'] = abs( $data['notifications']['life'] );
				$new_data['notifications']['duration'] = abs( $data['notifications']['duration'] );
				
				$new_data['notifications']['instant']['use'] = ( isset( $data['notifications']['instant']['use'] ) ) ? 1 : 0;
				$new_data['notifications']['instant']['check'] = abs( $data['notifications']['instant']['check'] );
				
				return $new_data;
			}
		}
		$notice = new myCRED_Notifications();
		$notice->load();
	}

	/**
	 * Add Notice
	 * @since 1.0.3
	 * @version 1.0.1
	 */
	if ( ! function_exists( 'mycred_add_new_notice' ) ) {
		function mycred_add_new_notice( $notice = array(), $life = 1 ) {
			if ( ! isset( $notice['user_id'] ) || ! isset( $notice['message'] ) ) return false;

			// Get transient
			$data = get_transient( 'mycred_notice_' . $notice['user_id'] );

			// If none exists create a new array
			if ( $data === false || ! is_array( $data ) )
				$notices = array();
			else
				$notices = $data;
			
			// Add new notice
			$notices[] = $notice['color'] . addslashes( $notice['message'] );
			// Save as a transient
			set_transient( 'mycred_notice_' . $notice['user_id'], $notices, 86400*$life );
		}
	}
}
?>