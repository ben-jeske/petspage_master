<?php
/**
 * Plugin Name: myCRED Hook - BuddyBoss
 * Plugin URI: http://mycred.me/about/supported-plugins/buddyboss-theme/
 * Description: Adds a custom hook on your myCRED > Hooks page allowing you to award points for Wall Updates, Wall Likes and Photo Uploads.
 * Version: 1.0
 * Tags: points, tokens, credit, management, reward, charge, buddyboss
 * Author: Gabriel S Merovingi
 * Author URI: http://www.merovingi.com
 * Author Email: support@mycred.me
 * Requires at least: WP 3.1
 * Tested up to: WP 3.8
 * Text Domain: mycred_bboss
 * Domain Path: /lang
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Forum URI: http://mycred.me/support/forums/
 */

/**
 * Register Custom Hook
 * @since 1.0
 * @version 1.0
 */
add_filter( 'mycred_setup_hooks', 'mycred_register_buddyboss_hook' );
function mycred_register_buddyboss_hook( $installed ) {
	$installed['buddyboss'] = array(
		'title'       => __( 'BuddyBoss Theme Actions', 'mycred_bboss' ),
		'description' => __( 'Award %_plural% for BuddyBoss theme actions.', 'mycred_bboss' ),
		'callback'    => array( 'myCRED_Hook_BuddyBoss' )
	);
	return $installed;
}

/**
 * Add Hook Class
 * @since 1.0
 * @version 1.0
 */
add_action( 'mycred_pre_init', 'mycred_load_buddyboss_hook' );
function mycred_load_buddyboss_hook() {
	class myCRED_Hook_BuddyBoss extends myCRED_Hook {
		
		/**
		 * Construct
		 */
		function __construct( $hook_prefs ) {
			parent::__construct( array(
				'id'       => 'buddyboss',
				'defaults' => array(
					'image_upload' => array(
						'creds'   => 0,
						'log'     => '%plural% for uploading an image.'
					),
					'like_wall'    => array(
						'creds'   => 0,
						'log'     => '%plural% for wall like',
						'limit'   => 1
					)
				)
			), $hook_prefs );
		}

		/**
		 * Run
		 * @since 1.0
		 * @version 1.0
		 */
		public function run() {
			if ( $this->prefs['image_upload']['creds'] != 0 && get_option( 'buddyboss_pics_on', 0 ) )
				add_action( 'buddyboss_add_attachment', array( $this, 'add_attachment' ) );
				
			if ( $this->prefs['like_wall']['creds'] != 0 && get_option( 'buddyboss_wall_on', 0 ) )
				add_action( 'bp_activity_add_user_favorite', array( $this, 'activity_fav' ), 10, 2 );
		}

		/**
		 * Image Upload Hook
		 * @since 1.0
		 * @version 1.0
		 */
		public function add_attachment( $attachment_id ) {
			if ( ! is_user_logged_in() ) return;
				
			$user_id = get_current_user_id();

			// Make sure user is not excluded
			if ( $this->core->exclude_user( $user_id ) === true ) return;

			// Make sure this is unique
			if ( $this->has_entry( 'photo_upload', $attachment_id, $user_id ) ) return;

			// Execute
			$this->core->add_creds(
				'photo_upload',
				$user_id,
				$this->prefs['image_upload']['creds'],
				$this->prefs['image_upload']['log'],
				$attachment_id,
				array( 'ref_type' => 'post' )
			);
		}

		/**
		 * Activity "Like"
		 * @since 1.0
		 * @version 1.0
		 */
		public function activity_fav( $activity_id, $user_id ) {
			// Make sure user is not excluded
			if ( $this->core->exclude_user( $user_id ) === true ) return;
				
			// No likes of our own
			$activity_obj = new BP_Activity_Activity( $activity_id );
			if ( $activity_obj->user_id == $user_id ) return;
				
			// Make sure we have not reached limit
			if ( $this->reached_limit( $user_id ) ) return;
				
			// Make sure this is unique
			if ( $this->has_entry( 'wall_like', $activity_id, $user_id ) ) return;
				
			// Execute
			$this->core->add_creds(
				'wall_like',
				$user_id,
				$this->prefs['like_wall']['creds'],
				$this->prefs['like_wall']['log'],
				$activity_id
			);
				
			$this->increment_limit( $user_id );
		}

		/**
		 * Reached Limit Check
		 * @since 1.0
		 * @version 1.0
		 */
		public function reached_limit( $user_id = NULL ) {
			if ( $user_id === NULL || empty( $this->prefs['like_wall']['limit'] ) || $this->prefs['like_wall']['limit'] == 0 ) return true;
				
			$today = date_i18n( 'Y-m-d' );
			$limit = get_user_meta( $user_id, 'activity_fav_limit', true );

			if ( empty( $limit ) || ! isset( $limit[ $today ] ) ) {
				$limit = array( $today => 0 );
				update_user_meta( $user_id, 'activity_fav_limit', $limit );
			}
				
			if ( $limit[ $today ] < $this->prefs['like_wall']['limit'] )
				return false;
				
			return true;
		}

		/**
		 * Incrememnt Limit
		 * @since 1.0
		 * @version 1.0
		 */
		public function increment_limit( $user_id = NULL ) {
			if ( $user_id === NULL ) return true;
				
			$today = date_i18n( 'Y-m-d' );
			$limit = get_user_meta( $user_id, 'activity_fav_limit', true );
				
			if ( empty( $limit ) )
				$limit = array( $today => 0 );
			
			$limit[ $today ]++;

			update_user_meta( $user_id, 'activity_fav_limit', $limit );
		}

		/**
		 * Preference for BuddyBoss Hook
		 * @since 1.0
		 * @version 1.0
		 */
		public function preferences() {
			$prefs = $this->prefs;
			$pic_upload = get_option( 'buddyboss_pics_on', 0 );
			$wall = get_option( 'buddyboss_wall_on', 0 ); ?>

<?php if ( $pic_upload ) : ?>
<label class="subheader"><?php _e( 'User Photo Uploading', 'mycred_bboss' ); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( array( 'image_upload' => 'creds' ) ); ?>" id="<?php echo $this->field_id( array( 'image_upload' => 'creds' ) ); ?>" value="<?php echo $this->core->number( $prefs['image_upload']['creds'] ); ?>" size="8" /></div>
	</li>
</ol>
<label class="subheader"><?php _e( 'Log template', 'mycred_bboss' ); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( array( 'image_upload' => 'log' ) ); ?>" id="<?php echo $this->field_id( array( 'image_upload' => 'log' ) ); ?>" value="<?php echo $prefs['image_upload']['log']; ?>" class="long" /></div>
		<span class="description"><?php _e( 'Available template tags: General and Post related.', 'mycred_bboss' ); ?></span>
	</li>
</ol>
<?php else : ?>

<label class="subheader"><?php _e( 'Image Upload', 'mycred_bboss' ); ?></label>
<ol>
	<li>
		<p><?php _e( 'User Photo Uploading is not enabled.', 'mycred_bboss' ); ?></p>
	</li>
</ol>
<?php endif; ?>

<?php if ( $wall ) : ?>

<label class="subheader"><?php _e( 'Wall Update', 'mycred_bboss' ); ?></label>
<ol>
	<li><?php echo $this->core->template_tags_general( __( 'To award points for wall updates, use the BuddyPress: Members "%plural% for Profile Updates" hook.', 'mycred_bboss' ) ); ?></li>
</ol>
<label class="subheader"><?php _e( 'Wall Like', 'mycred_bboss' ); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( array( 'like_wall' => 'creds' ) ); ?>" id="<?php echo $this->field_id( array( 'like_wall' => 'creds' ) ); ?>" value="<?php echo $this->core->number( $prefs['like_wall']['creds'] ); ?>" size="8" /></div>
	</li>
</ol>
<label class="subheader"><?php _e( 'Log template', 'mycred_bboss' ); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( array( 'like_wall' => 'log' ) ); ?>" id="<?php echo $this->field_id( array( 'like_wall' => 'log' ) ); ?>" value="<?php echo $prefs['like_wall']['log']; ?>" class="long" /></div>
		<span class="description"><?php _e( 'Available template tags: General and Post related.', 'mycred_bboss' ); ?></span>
	</li>
	<li class="empty">&nbsp;</li>
	<li>
		<label><?php _e( 'Daily Limit', 'mycred_bboss' ); ?></label>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( array( 'like_wall' => 'limit' ) ); ?>" id="<?php echo $this->field_id( array( 'like_wall' => 'limit' ) ); ?>" value="<?php echo $prefs['like_wall']['limit']; ?>" class="short" /> <?php _e( '/ day', 'mycred' ); ?></div>
		<span class="description"><?php _e( 'Optional daily limit for Wall Likes. Use zero for unlimited. Note that authors liking their own updates are not awarded!', 'mycred_bboss' ); ?></span>
	</li>
</ol>
<?php endif; ?>

<?php
		}
	}
}

/**
 * Apply Update to myCRED Tool Bar
 * Replaced the tool bar handler for version before 1.4
 * @since 1.0
 * @version 1.0
 */
add_action( 'mycred_init', 'mycred_adjust_tool_bar_pre_update' );
function mycred_adjust_tool_bar_pre_update() {
	if ( version_compare( myCRED_VERSION, '1.4', '<' ) ) {
		remove_action( 'admin_bar_menu', 'mycred_hook_into_toolbar' );
		add_action( 'admin_bar_menu', 'mycred_hook_into_toolbar_updated' );
	}
}

/**
 * myCRED Tool Bar
 * @since 1.0
 * @version 1.0
 */
if ( ! function_exists( 'mycred_hook_into_toolbar_updated' ) ) :
	function mycred_hook_into_toolbar_updated( $wp_admin_bar ) {
		if ( ! is_user_logged_in() ) return;

		$mycred = mycred_get_settings();
		$user_id = get_current_user_id();
		if ( $mycred->exclude_user( $user_id ) ) return;

		$balance = $mycred->get_users_cred( $user_id );

		$my_balance_label = apply_filters( 'mycred_label_my_balance', __( 'My Balance: %cred_f%', 'mycred_bboss' ), $user_id, $mycred );
		if ( isset( $mycred->buddypress['balance_template'] ) )
			$my_balance_label = $mycred->buddypress['balance_template'] . ' %cred_f%';
	
		$mycred_history_label = __( 'History', 'mycred_bboss' );
		if ( isset( $mycred->buddypress['history_menu_title']['me'] ) && function_exists( 'bp_loggedin_user_domain' ) )
			$mycred_history_label = $mycred->buddypress['history_menu_title']['me'];
		
		$mycred_history_url = apply_filters( 'mycred_my_history_url', '#' );
		if ( isset( $mycred->buddypress['history_url'] ) && function_exists( 'bp_loggedin_user_domain' ) )
			$mycred_history_url = bp_loggedin_user_domain() . $mycred->buddypress['history_url'] . '/';

		global $bp;
	
		// BuddyPress
		if ( is_object( $bp ) )
			$wp_admin_bar->add_menu( array(
				'parent' => 'my-account-buddypress',
				'id'     => 'mycred-account',
				'title'  => $mycred->plural(),
				'href'   => '#'
			) );

		// Default
		else
			$wp_admin_bar->add_menu( array(
				'parent' => 'my-account',
				'id'     => 'mycred-account',
				'title'  => $mycred->plural(),
				'meta'   => array(
					'class' => 'ab-sub-secondary'
				)
			) );

		$wp_admin_bar->add_menu( array(
			'parent' => 'mycred-account',
			'id'     => 'mycred-account-balance',
			'title'  => $mycred->template_tags_amount( $my_balance_label, $balance ),
			'href'   => '#'
		) );

		$wp_admin_bar->add_menu( array(
			'parent' => 'mycred-account',
			'id'     => 'mycred-account-history',
			'title'  => $mycred_history_label,
			'href'   => $mycred_history_url
		) );

		// Let others play
		do_action( 'mycred_tool_bar', $wp_admin_bar, $mycred );
	}
endif;
?>