<div id="design5-advanced-design-settings">
    <div class="dh_ptp_accordion">
        <!-- General Settings -->
        <h3><?php _e('General Settings', PTP_LOC); ?></h3>
        <div>
            <table>
                <tr>
                    <td class="settings-title"><?php _e('Featured Label Text', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-most-popular-label-text'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'Most Popular'; ?>
                        <input type="text" name="<?php $metabox->the_name(); ?>" id="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>" />
                    </td>
                </tr>
                <tr>
                    <?php $mb->the_field('design5-shake-buttons-on-hover'); ?>
                    <td class="settings-title">
                        <label for="design5-shake-buttons-on-hover" style="margin: 0; font-weight: normal;"><?php _e('Shake Button on Hover', PTP_LOC); ?></label>
                    </td>
                    <td>
                        <input type="checkbox" name="<?php $metabox->the_name(); ?>" id="design5-shake-buttons-on-hover" value="1" <?php if ($metabox->get_the_value()) echo 'checked="checked"'; ?>/>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Font Sizes -->
        <h3><?php _e('Font Sizes', PTP_LOC); ?></h3>
        <div>
            <table>
                <tr>
                    <td class="settings-title"><?php _e('Featured Label Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-most-popular-font-size'); ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php if(!is_null($mb->get_the_value())) echo $metabox->the_value(); else echo "18"; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-most-popular-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'em') {
                                    echo 'selected';
                                }
                            }
                            ?> >em</option>
                            <option value="px" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'px') {
                                    echo 'selected';
                                }
                            } else {
                                echo 'selected';
                            }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Plan Name Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-plan-name-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():"25"; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-plan-name-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>                    
                <tr>
                    <td class="settings-title"><?php _e('Price Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-price-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():"90"; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-price-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Currency Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-currency-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():"40"; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-currency-font-size-type'); ?>
                        <select name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Billing Timeframe Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-billing-timeframe-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'16'; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-billing-timeframe-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Bullet Item Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-bullet-item-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'16'; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-bullet-item-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Button Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-button-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'14'; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-button-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Raw Description Font Size', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-raw-description-font-size'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'14'; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-raw-description-font-size-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'em') {
                                        echo 'selected';
                                    }
                                }
                            ?> >em</option>
                            <option value="px" <?php
                                if(!is_null($mb->get_the_value())) {
                                    if($mb->get_the_value() == 'px') {
                                        echo 'selected';
                                    }
                                } else {
                                    echo 'selected';
                                }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Row Line Height', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-row-line-height'); ?>
                        <?php $value = (!is_null($mb->get_the_value()))?$metabox->get_the_value():'20'; ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $value; ?>"/>
                    </td>
                    <td>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="px" selected>px</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Column Margins -->
        <h3><?php _e('Column Margins', PTP_LOC); ?></h3>
        <div>
            <table>
                <tr>
                    <td class="settings-title"><?php _e('Description Column (Additional) Top Margin', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-description-column-margin'); ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php if(!is_null($mb->get_the_value())) echo $metabox->the_value(); else echo "0"; ?>"/>
                    </td>
                    <td>
                        px
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Margin Between Plan Name and Price', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-margin-between-plan-name-and-price'); ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php if(!is_null($mb->get_the_value())) echo $metabox->the_value(); else echo "10"; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-margin-between-plan-name-and-price-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'em') {
                                    echo 'selected';
                                }
                            }
                            ?> >em</option>
                            <option value="px" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'px') {
                                    echo 'selected';
                                }
                            } else {
                                echo 'selected';
                            }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="settings-title"><?php _e('Margin Between Price and Billing Timeframe', PTP_LOC); ?></td>
                    <td>
                        <?php $mb->the_field('design5-margin-between-price-and-billing-timeframe'); ?>
                        <input class="form-control float-input" type="text" name="<?php $metabox->the_name(); ?>" value="<?php if(!is_null($mb->get_the_value())) echo $metabox->the_value(); else echo "10"; ?>"/>
                    </td>
                    <td>
                        <?php $mb->the_field('design5-margin-between-price-and-billing-timeframe-type'); ?>
                        <select  name="<?php $metabox->the_name(); ?>">
                            <option value="em" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'em') {
                                    echo 'selected';
                                }
                            }
                            ?> >em</option>
                            <option value="px" <?php
                            if(!is_null($mb->get_the_value())) {
                                if($mb->get_the_value() == 'px') {
                                    echo 'selected';
                                }
                            } else {
                                echo 'selected';
                            }
                            ?>>px</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Colors -->
        <h3><?php _e('Colors', PTP_LOC); ?></h3>
        <div>
            <table>
                <tr>
                    <td class="settings-title"><?php _e('How would you like to choose your colors?', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-choose-your-colors'); ?>
                    <td>
                        <?php $design5_choose_your_colors = (!is_null($mb->get_the_value()))?$metabox->get_the_value():1; ?>
                        <label style="margin: 0; font-weight: normal;"><input type="radio" name="<?php $mb->the_name(); ?>" class="design5-choose-your-colors" value="1" <?php echo ($design5_choose_your_colors == 1)?'checked':''; ?>/><?php _e('Use a pre-built color scheme', PTP_LOC); ?></label>
                        <label style="margin: 4px 0 5px 0; font-weight: normal;"><input type="radio" name="<?php $mb->the_name(); ?>" class="design5-choose-your-colors" value="2" <?php echo ($design5_choose_your_colors == 2)?'checked':''; ?>/><?php _e('Build your own color scheme', PTP_LOC); ?></label>
                    </td>
                </tr>
                
                <!-- description column -->
                <tr class="table-headline design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td><br/><?php _e('Description Column', PTP_LOC); ?></td>
                </tr>
                
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Even Row Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-description-dark-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#2c3e50"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#2c3e50" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Uneven Row Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-description-light-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#34495e"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#34495e" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Hover Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-description-hover-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#18232e"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#18232e" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Border Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-description-border-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#2c3e50"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#2c3e50" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Description Text Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-description-text-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#ffffff"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#ffffff" />
                    </td>
                </tr>
                
                <!-- buttons -->
                <tr class="table-headline design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td><br/><?php _e('Button Colors', PTP_LOC); ?></td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Button Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-button-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#ffffff"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#ffffff" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Button Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-button-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#e74c3c"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#e74c3c" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Button Hover Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-button-hover-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#ffffff"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#ffffff" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Button Hover Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-button-hover-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#c0392b"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#c0392b" />
                    </td>
                </tr>
                
                <!-- columns -->
                <tr class="table-headline design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td><br/><?php _e('Row Colors', PTP_LOC); ?></td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Uneven Row Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-column-light-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#f4fafb"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#f4fafb" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Even Row Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-column-dark-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#e8f4f7"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#e8f4f7" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Row Hover Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-column-hover-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#ffffff"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#ffffff" />
                    </td>
                </tr>
                
                <!-- unfeatured -->
                <tr class="table-headline design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td><br/><?php _e('Unfeatured Columns', PTP_LOC); ?></td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-plan-title-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#537597"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#537597" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-plan-title-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#34495e"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#34495e" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Hover Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-plan-title-hover-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#2c3e50"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#2c3e50" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Border Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-plan-border-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#2c3e50"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#2c3e50" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Pay Duration Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-pay-duration-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#2c3e50"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#2c3e50" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Price Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-unfeatured-plan-price-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#f1f1f1"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#f1f1f1" />
                    </td>
                </tr>
                
                <!-- featured -->
                <tr class="table-headline design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td><br/><?php _e('Featured Columns', PTP_LOC); ?></td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-plan-title-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#952e22"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#952e22" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-plan-title-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#e74c3c"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#e74c3c" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Hover Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-plan-title-hover-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#c0392b"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#c0392b" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Plan Title Border Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-plan-border-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#c0392b"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#c0392b" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Pay Duration Background Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-pay-duration-background-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#c0392b"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#c0392b" />
                    </td>
                </tr>
                <tr class="design5-color-scheme-2 <?php echo ($design5_choose_your_colors != 2) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Price Font Color', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-featured-plan-price-font-color'); ?>
                    <td>
                        <?php $value = (!is_null($mb->get_the_value()))?$mb->get_the_value():"#f1f1f1"; ?>
                        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="colorpicker-no-palettes" data-default-color="#f1f1f1" />
                    </td>
                </tr>
                
                <!-- pre-build -->
                <tr class="design5-color-scheme-1 <?php echo ($design5_choose_your_colors != 1) ? 'hide' : ''; ?>">
                    <td class="settings-title"><?php _e('Choose a theme', PTP_LOC); ?></td>
                    <?php $mb->the_field('design5-choose-a-theme'); ?>
                    <td>
                        <?php
                            $value = (!is_null($mb->get_the_value()) && substr($mb->get_the_value(), 0, 1) == '#')?$mb->get_the_value():"#34495e/#c0392b";
                            if (strpos($value, '/') === false) {
                                $value = "#34495e/#c0392b";
                            }
                            list($color1, $color2) = explode('/', $value);
                        ?>
                        <div class="dh_ptp_color_palettes_container">
                            <a tabindex="0" class="dh_ptp_color_palettes_result" title="<?php _e('Select Theme', PTP_LOC); ?>" data-current="<?php _e('Current Theme', PTP_LOC); ?>" style="background-color: <?php echo $color1; ?>;">
                                <span style="position: absolute; height: 22px; width: 15px; margin-left: -15px;background-color:<?php echo $color2; ?>"></span>
                            </a>
                            <span class="dh_ptp_color_palettes_input_wrap">
                                <input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $value; ?>" class="palette"/>
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Advanced Settings -->
        <h3><?php _e('Advanced Settings', PTP_LOC); ?></h3>
        <div>
            <table>
                <tr>
                    <?php $mb->the_field('design5-hide-empty-rows'); ?>
                    <td class="settings-title">
                        <label for="design5-hide-empty-rows" style="margin: 0; font-weight: normal;"><?php _e('Hide Empty Rows', PTP_LOC); ?></label>
                    </td>
                    <td>
                        <input type="checkbox" name="<?php $metabox->the_name(); ?>" id="design5-hide-empty-rows" value="1" <?php if ($metabox->get_the_value()) echo 'checked="checked"'; ?>/>
                    </td>
                </tr>
                <tr>
                    <?php $mb->the_field('design5-hide-call-to-action-buttons'); ?>
                    <td class="settings-title">
                        <label for="design5-hide-call-to-action-buttons" style="margin: 0; font-weight: normal;"><?php _e('Hide Call To Action Buttons', PTP_LOC); ?></label>
                    </td>
                    <td>  
                        <input type="checkbox" name="<?php $metabox->the_name(); ?>" id="design5-hide-call-to-action-buttons" value="1"<?php if ($metabox->get_the_value()) echo ' checked="checked"'; ?>/>
                    </td>
                </tr>
                <tr>
                    <?php $mb->the_field('design5-open-link-in-new-tab'); ?>
                    <td class="settings-title">
                        <label for="design5-open-link-in-new-tab" style="margin: 0; font-weight: normal;"><?php _e('Open Links in New Tab', PTP_LOC); ?></label>
                    </td>
                    <td>
                        <input type="checkbox" name="<?php $metabox->the_name(); ?>" id="design5-open-link-in-new-tab" value="1" <?php if ($metabox->get_the_value()) echo 'checked="checked"'; ?>/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
