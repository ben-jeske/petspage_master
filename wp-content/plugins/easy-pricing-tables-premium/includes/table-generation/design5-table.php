<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 11/15/13
 * Time: 15:28
 */


/**
 * Read all custom styling from our database
 */
function dh_ptp_design5_css($id, $meta)
{
    $content = '';
    $themes = array('#34495e/#c0392b', '#0e9577/#ce3306', '#66317d/#0a8bb0', '#08a7bd/#a81818', '#138f6b/#46b527', '#417d83/#ee4c7e', '#27b18f/#ecce44', '#b97a68/#f37e5d', '#114d68/#9e0165', '#444588/#ee1367');
    
    // Custom theme templates
    $design5_choose_a_theme = isset($meta['design5-choose-a-theme'])?$meta['design5-choose-a-theme']:'#34495e/#c0392b';
    // Fetch theme styles
    $key = array_search($design5_choose_a_theme, $themes);
    if ($key || $key == 0) {
        $current_path = plugin_dir_path(__FILE__);
        $theme_path = $current_path.'../../assets/pricing-tables/design5/css/theme'.($key+1).'.css'; 
        if (file_exists($theme_path)) {
            $content = file_get_contents($theme_path);
        }
    }
    
    // Column Margins
    $design5_description_column_margin = isset($meta['design5-description-column-margin'])?$meta['design5-description-column-margin']:'20';
    $design5_margin_between_plan_name_and_price = isset($meta['margin-between-plan-name-and-price'])?$meta['margin-between-plan-name-and-price']:'10';
    $design5_margin_between_plan_name_and_price_type = isset($meta['margin-between-plan-name-and-price-type'])?$meta['margin-between-plan-name-and-price-type']:'px';
    $design5_margin_between_price_and_billing_timeframe = isset($meta['design5-margin-between-price-and-billing-timeframe'])?$meta['design5-margin-between-price-and-billing-timeframe']:'10';
    $design5_margin_between_price_and_billing_timeframe_type = isset($meta['design5-margin-between-price-and-billing-timeframe-type'])?$meta['design5-margin-between-price-and-billing-timeframe-type']:'px';
    
    // Other variables
    $design5_shake_buttons_on_hover = isset($meta['design5-shake-buttons-on-hover'])?$meta['design5-shake-buttons-on-hover']:0;
    $design5_most_popular_font_size = isset($meta['design5-most-popular-font-size'])?$meta['design5-most-popular-font-size']:'18';
    $design5_most_popular_font_size_type = isset($meta['design5-most-popular-font-size-type'])?$meta['design5-most-popular-font-size-type']:'px';
    $design5_plan_name_font_size = isset($meta['design5-plan-name-font-size'])?$meta['design5-plan-name-font-size']:'25';
    $design5_plan_name_font_size_type = isset($meta['design5-plan-name-font-size-type'])?$meta['design5-plan-name-font-size-type']:'px';
    $design5_price_font_size = isset($meta['design5-price-font-size'])?$meta['design5-price-font-size']:'90';
    $design5_price_font_size_type = isset($meta['design5-price-font-size-type'])?$meta['design5-price-font-size-type']:'px';
    $design5_currency_font_size = isset($meta['design5-currency-font-size'])?$meta['design5-currency-font-size']:'40';
    $design5_currency_font_size_type = isset($meta['design5-currency-font-size-type'])?$meta['design5-currency-font-size-type']:'px';
    $design5_billing_timeframe_font_size = isset($meta['design5-billing-timeframe-font-size'])?$meta['design5-billing-timeframe-font-size']:'16';
    $design5_billing_timeframe_font_size_type = isset($meta['design5-billing-timeframe-font-size-type'])?$meta['design5-billing-timeframe-font-size-type']:'px';
    $design5_bullet_item_font_size = isset($meta['design5-bullet-item-font-size'])?$meta['design5-bullet-item-font-size']:'16';
    $design5_bullet_item_font_size_type = isset($meta['design5-bullet-item-font-size-type'])?$meta['design5-bullet-item-font-size-type']:'px';
    $design5_button_font_size = isset($meta['design5-button-font-size'])?$meta['design5-button-font-size']:'14';
    $design5_button_font_size_type = isset($meta['design5-button-font-size-type'])?$meta['design5-button-font-size-type']:'px';
    $design5_raw_description_font_size = isset($meta['design5-raw-description-font-size'])?$meta['design5-raw-description-font-size']:'14';
    $design5_raw_description_font_size_type = isset($meta['design5-raw-description-font-size-type'])?$meta['design5-raw-description-font-size-type']:'px';
    $design5_row_line_height = isset($meta['design5-row-line-height'])?$meta['design5-row-line-height']:'20';
    
    // Colors, fetch only when option selected
    $design5_choose_your_colors = isset($meta['design5-choose-your-colors'])?$meta['design5-choose-your-colors']:1;
    if ($design5_choose_your_colors == 2) {
        // Description Column
        $design5_description_dark_background_color = isset($meta['design5-description-dark-background-color'])?$meta['design5-description-dark-background-color']:'#2c3e50';
        $design5_description_light_background_color = isset($meta['design5-description-light-background-color'])?$meta['design5-description-light-background-color']:'#34495e';
        $design5_description_hover_background_color = isset($meta['design5-description-hover-background-color'])?$meta['design5-description-hover-background-color']:'#18232e';
        $design5_description_border_color = isset($meta['design5-description-border-color'])?$meta['design5-description-border-color']:'#2c3e50';
        $design5_description_text_font_color = isset($meta['design5-description-text-font-color'])?$meta['design5-description-text-font-color']:'#ffffff';
        
        // Buttons
        $design5_button_font_color = isset($meta['design5-button-font-color'])?$meta['design5-button-font-color']:'#ffffff';
        $design5_button_background_color = isset($meta['design5-button-background-color'])?$meta['design5-button-background-color']:'#e74c3c';
        $design5_button_hover_font_color = isset($meta['design5-button-hover-font-color'])?$meta['design5-button-hover-font-color']:'#ffffff';
        $design5_button_hover_background_color = isset($meta['design5-button-hover-background-color'])?$meta['design5-button-hover-background-color']:'#c0392b';
        
        // Column Colors
        $design5_column_light_color = isset($meta['design5-column-light-color'])?$meta['design5-column-light-color']:'#f4fafb';
        $design5_column_dark_color = isset($meta['design5-column-dark-color'])?$meta['design5-column-dark-color']:'#e8f4f7';
        $design5_column_hover_color = isset($meta['design5-column-hover-color'])?$meta['design5-column-hover-color']:'#ffffff';
        
        // Unfeatured columns
        $design5_unfeatured_plan_title_font_color = isset($meta['design5-unfeatured-plan-title-font-color'])?$meta['design5-unfeatured-plan-title-font-color']:'#537597';
        $design5_unfeatured_plan_title_background_color = isset($meta['design5-unfeatured-plan-title-background-color'])?$meta['design5-unfeatured-plan-title-background-color']:'#34495e';
        $design5_unfeatured_plan_title_hover_background_color = isset($meta['design5-unfeatured-plan-title-hover-background-color'])?$meta['design5-unfeatured-plan-title-hover-background-color']:'#2c3e50';
        $design5_unfeatured_plan_border_background_color = isset($meta['design5-unfeatured-plan-border-background-color'])?$meta['design5-unfeatured-plan-border-background-color']:'#2c3e50';
        $design5_unfeatured_plan_price_font_color = isset($meta['design5-unfeatured-plan-price-font-color'])?$meta['design5-unfeatured-plan-price-font-color']:'#f1f1f1';
        $design5_unfeatured_pay_duration_background_color = isset($meta['design5-unfeatured-pay-duration-background-color'])?$meta['design5-unfeatured-pay-duration-background-color']:'#2c3e50';
        
        // Featured
        $design5_featured_plan_title_font_color = isset($meta['design5-featured-plan-title-font-color'])?$meta['design5-featured-plan-title-font-color']:'#952e22';
        $design5_featured_plan_title_background_color = isset($meta['design5-featured-plan-title-background-color'])?$meta['design5-featured-plan-title-background-color']:'#e74c3c';
        $design5_featured_plan_border_background_color = isset($meta['design5-featured-plan-border-background-color'])?$meta['design5-featured-plan-border-background-color']:'#c0392b';
        $design5_featured_plan_price_font_color = isset($meta['design5-featured-plan-price-font-color'])?$meta['design5-featured-plan-price-font-color']:'#f1f1f1';
        $design5_featured_plan_title_hover_background_color = isset($meta['design5-featured-plan-title-hover-background-color'])?$meta['design5-featured-plan-title-hover-background-color']:'#c0392b';
        $design5_featured_pay_duration_background_color = isset($meta['design5-featured-pay-duration-background-color'])?$meta['design5-featured-pay-duration-background-color']:'#c0392b';
    }
    
    ?>
    
    /* design5 #<?php echo $id;?> */
    
    <?php
    $lines = explode("\n", $content);
    if (count($lines) > 0) :
        foreach ($lines as $line) :
            echo "#ptp-".$id.' '.$line."\n    "; 
        endforeach;
    endif;
    ?>

    <?php if ($design5_shake_buttons_on_hover) : ?>
        #ptp-<?php echo $id; ?> .ptp-data-holder .btn:hover{
            margin-top: 3px;
            border-bottom-width: 2px;
        }
    <?php endif; ?>
    
    <?php // !important replacements  ?>
    #ptp-<?php echo $id;?> a {outline: none; }
    #ptp-<?php echo $id;?> .ptp-price-table-holder [class*="span"] { margin-left:0px; }
    #ptp-<?php echo $id;?> .ptp-price-table .ptp-data-holder:hover{ background-color:#ffffff;}
    #ptp-<?php echo $id;?> .row-fluid [class*="span"] { margin-left: -1.5px; margin-right: -1.5px;}
    
    #ptp-<?php echo $id;?> .head-tooltip {
        font-size: <?php echo $design5_most_popular_font_size.$design5_most_popular_font_size_type;?>;
    }
    #ptp-<?php echo $id;?> .ptp-plan-title h2 {
        font-size: <?php echo $design5_plan_name_font_size.$design5_plan_name_font_size_type;?>;
        font-weight: bold;
        margin: 0px;
    }

    #ptp-<?php echo $id;?> .ptp-price-holder .dg5-ptp-price {

        font-size: <?php echo $design5_price_font_size.$design5_price_font_size_type;?>;
        line-height: 1;
        padding-top: <?php echo $design5_margin_between_plan_name_and_price.$design5_margin_between_plan_name_and_price_type;?>;
        padding-bottom: <?php echo $design5_margin_between_price_and_billing_timeframe.$design5_margin_between_price_and_billing_timeframe_type;?>;
    }
    #ptp-<?php echo $id;?> .ptp-price-holder .sign {
        font-size: <?php echo $design5_currency_font_size.$design5_currency_font_size_type;?>;
    }
    #ptp-<?php echo $id;?> .ptp-pay-duration {
        font-size: <?php echo $design5_billing_timeframe_font_size.$design5_billing_timeframe_font_size_type;?>;
    }
    #ptp-<?php echo $id;?> .ptp-data-holder {
        line-height: <?php echo $design5_row_line_height;?>px;
    }
    #ptp-<?php echo $id;?> .ptp-data-holder, #ptp-<?php echo $id;?> .ptp-data-holder .fa-times-circle, #ptp-<?php echo $id;?> .ptp-data-holder .fa-chevron-circle-down {
        font-size: <?php echo $design5_bullet_item_font_size.$design5_bullet_item_font_size_type;?>;
    }
    #ptp-<?php echo $id;?> .ptp-design5-row .has-tip, #ptp-<?php echo $id;?> .ptp-design5-row .has-tip:hover {
        color: #333;
        border-bottom: dotted 1px #333;
    }
    #ptp-<?php echo $id;?> .ptp-design5-row .has-tip:hover {
        border-bottom: dotted 1px #333;
    }
    #ptp-<?php echo $id;?> .ptp-data-holder .btn {
        font-size: <?php echo $design5_button_font_size.$design5_button_font_size_type;?>;
    }
    #ptp-<?php echo $id;?> .desc-table {
        margin-top: <?php echo $design5_description_column_margin;?>px;
    }
    #ptp-<?php echo $id;?> .desc-table .ptp-data-holder {
        font-size: <?php echo $design5_raw_description_font_size.$design5_raw_description_font_size_type;?>;
    }
    
    <?php if ($design5_choose_your_colors == 2) : ?>
        /* Description column */
        #ptp-<?php echo $id;?> .desc-table {
            border: 1px solid <?php echo $design5_description_border_color; ?>;
        }
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:nth-child(2n+1) {
            background: <?php echo $design5_description_dark_background_color;?>;
            color: <?php echo $design5_description_text_font_color; ?>;
        }
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:nth-child(2n) {
            background: <?php echo $design5_description_light_background_color; ?>;
            color: <?php echo $design5_description_text_font_color; ?>;
        }
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:hover {
            background: <?php echo $design5_description_hover_background_color; ?>;
            color: <?php echo $design5_description_text_font_color; ?>;
        }
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:nth-child(2n+1) .has-tip,
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:nth-child(2n) .has-tip,
        #ptp-<?php echo $id;?> .desc-table .ptp-data-holder:hover .has-tip
        {
            color: <?php echo $design5_description_text_font_color; ?>;
            border-bottom: dotted 1px <?php echo $design5_description_text_font_color; ?>;
        }
        
        /* Buttons */
        #ptp-<?php echo $id; ?> .ptp-data-holder .btn {
            background: <?php echo $design5_button_background_color; ?>;
            color: <?php echo $design5_button_font_color;?>;
            border-color: rgba(0, 0, 0, 0.3); 
        }
        #ptp-<?php echo $id; ?> .ptp-data-holder .btn .has-tip {
            color: <?php echo $design5_button_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_button_font_color; ?>;
        }
        #ptp-<?php echo $id; ?> .ptp-data-holder .btn:hover {
            background: <?php echo $design5_button_hover_background_color; ?>;
            color: <?php echo $design5_button_hover_font_color;?>;
            border-color: rgba(0, 0, 0, 0.5); 
        }
        #ptp-<?php echo $id; ?> .ptp-data-holder .btn:hover .has-tip {
            color: <?php echo $design5_button_hover_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_button_hover_font_color; ?>;
        }
        
        /* Unfeatured Column */
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured .ptp-plan-title h2 {
            color: <?php echo $design5_unfeatured_plan_title_font_color;?>;
            text-shadow: 0 2px 0 rgba(0, 0, 0, 0.7);
        }
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured .ptp-plan-title h2 .has-tip{
            color: <?php echo $design5_unfeatured_plan_title_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_unfeatured_plan_title_font_color; ?>;
        }
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured .ptp-price-holder {
            background: <?php echo $design5_unfeatured_plan_title_background_color; ?>;
            border: 1px solid <?php echo $design5_unfeatured_plan_border_background_color; ?>;
            color: <?php echo $design5_unfeatured_plan_price_font_color;?>;
        }
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured .ptp-price-holder .has-tip{
            color: <?php echo $design5_unfeatured_plan_price_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_unfeatured_plan_price_font_color;?>;
        }
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured:hover .ptp-price-holder {
            background: <?php echo $design5_unfeatured_plan_title_hover_background_color; ?> !important;
        }
        
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured .ptp-pay-duration,
        #ptp-<?php echo $id; ?> .ptp-design5-unfeatured:hover .ptp-pay-duration {
            background: <?php echo $design5_unfeatured_pay_duration_background_color; ?>;
        }
        
        /* Featured Column */
        #ptp-<?php echo $id; ?> .special.ptp-price-table .ptp-plan-title h2 {
            color: <?php echo $design5_featured_plan_title_font_color;?>;
            text-shadow: 0 2px 0 rgba(0, 0, 0, 0.7);
        }
        #ptp-<?php echo $id; ?> .special.ptp-price-table .ptp-plan-title h2 .has-tip{
            color: <?php echo $design5_featured_plan_title_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_featured_plan_title_font_color; ?>;
        }
        #ptp-<?php echo $id; ?> .special.ptp-price-table .ptp-price-holder {
            background: <?php echo $design5_featured_plan_title_background_color; ?>;
            border: 1px solid <?php echo $design5_featured_plan_border_background_color; ?>;
            color: <?php echo $design5_featured_plan_price_font_color;?>;
        }
        #ptp-<?php echo $id; ?> .special.ptp-price-table .ptp-price-holder .has-tip{
            color: <?php echo $design5_featured_plan_price_font_color;?>;
            border-bottom: dotted 1px <?php echo $design5_featured_plan_price_font_color; ?>;
        }
        #ptp-<?php echo $id; ?> .special.ptp-price-table:hover .ptp-price-holder {
            background: <?php echo $design5_featured_plan_title_hover_background_color; ?> !important;
        }
        #ptp-<?php echo $id; ?> .special.ptp-price-table .ptp-pay-duration,
        #ptp-<?php echo $id; ?> .special.ptp-price-table:hover .ptp-pay-duration {
            background: <?php echo $design5_featured_pay_duration_background_color; ?>;
        }
        
        /* Column Colors */
        #ptp-<?php echo $id; ?> .ptp-data-holder:nth-child(2n+1) {
            background: <?php echo $design5_column_light_color;?>;
        }
        #ptp-<?php echo $id; ?> .ptp-data-holder:nth-child(2n) {
            background: <?php echo $design5_column_dark_color; ?>;
        }
        #ptp-<?php echo $id; ?> .ptp-data-holder:hover {
            background: <?php echo $design5_column_hover_color; ?>;
        }
    <?php endif; ?>
    /* end of design5 #<?php echo $id;?> */
    
    <?php
}

/**
 * Generate our simple flat pricing table HTML
 * @return [type]
 */
function dh_ptp_generate_design5_pricing_table_html ($id, $hide = false) {
    global $features_metabox;
    global $meta;

    $meta = get_post_meta($id, $features_metabox->get_the_id(), TRUE);
    $post = get_post($id);
    
    $loop_index = 0;
    $col_cnt = (int)(count($meta['column'])+1);
    $col_cnt = ($col_cnt == 2) ? 3 : $col_cnt;
    $columns = floor(12/((int)$col_cnt));  // responsive column count
    $hide_table = ($hide)?'style="display:none"':'';
    $pricing_table_html =
        '<div id="ptp-'. $id .'" class="ptp-design5-pricingtable" '.$hide_table.'>' .
            '<div class="ptp-price-table-holder ptp-columns-'.$col_cnt.'">' .
                '<div class="row-fluid">';

    // Row descriptions
    $design5_table_features = isset($meta['design5-table-features'])?$meta['design5-table-features']:'';
    if ($design5_table_features) {
        $pricing_table_html .= '<div class="span'.$columns.'">'.
                                    '<div class="head">'.                        
                                        '<div class="ptp-price-holder ptp-price-holder-invisible">'.
                                            '<div class="ptp-plan-title"><h2>&nbsp;</h2></div>'.

                                            '<div class="dg5-ptp-price">&nbsp;<span class="sign">&nbsp;</span><div style="clear:both"></div></div>'.

                                            '<div class="ptp-pay-duration ptp-pay-duration-invisible">&nbsp;</div>'.
                                        '</div>'.
                                    '</div>'.
                                    '<div class="desc-table">';
        
        $feature_list = explode("\n", $design5_table_features);
        foreach($feature_list as $item) {
            $pricing_table_html .= '<div class="ptp-data-holder">' . dh_ptp_fa_icons($item) . '</div>';
        }
        
        $pricing_table_html .= '</div></div>'; 
    }
    
    // Data columns
    foreach ($meta['column'] as $column)
    {        
        // Note: beneath ifs are to prevent 'undefined variable notice'. It wasn't possible to put this code into a function since the passing argument might be undefined.      
        $planname = isset($column['planname'])?$column['planname']:'';
        $planprice = isset($column['planprice'])?$column['planprice']:'';
        $planfeatures = isset($column['planfeatures'])?$column['planfeatures']:'';
        $buttontext = isset($column['buttontext'])?$column['buttontext']:'';
        $buttonurl = isset($column['buttonurl'])?$column['buttonurl']:'';
        $billingtimeframe = isset($column['billingtimeframe'])?$column['billingtimeframe']:'';
        
        // featured column
        if (isset($column['feature'])){
            $feature_text = (isset($meta['design5-most-popular-label-text']))?$meta['design5-most-popular-label-text']:__('Most Popular', PTP_LOC);
            $feature = ($column['feature'] == 'featured')?'<div class="head-tooltip">' . dh_ptp_fa_icons($feature_text) . '</div>':'';
            $feature_class = ($column['feature'] == 'featured')?'special':'ptp-design5-unfeatured';
        } else {
            $feature = '';
            $feature_class = '';
        }

        /**
         * Extract price information
         * 
         * Patterns of prices supported:
         * - Currency then amount ($30, USD 30; €30) and possible text before and after
         * - Amount then currency (30 euros) and possible text before and after
         * - Amount only (30)
         */
        $price_formatted = '';
        $price_patterns = array(
            array(
                'id' => 'PTP_TEXT',
                'format' => ':price',
                'pattern' => "/^(?P<price>\D+)$/",
            ),
            array(
                'id' => 'PTP_CURR_PRICE',
                'format' => ':text_before:currency:price:text_after',
                'pattern' => "/^((?P<text_before>\D+)\s+)?(?P<currency>[^\d\s]+)\s*(?P<price>[\d.,']+)(\s+(?P<text_after>.+))?$/",
            ),
            array(
                'id' => 'PTP_PRICE_CURR',
                'format' => ':text_before:price:currency:text_after',
                'pattern' => "/^((?P<text_before>\D+)\s+)?(?P<price>\d[\d.,']*)\s*(?P<currency>[^\d\s]+)(\s+(?P<text_after>.+))?$/",
            ),
            array(
                'id' => 'PTP_PRICE',
                'format' => ':price:currency',
                'pattern' => '/^(?P<price>\d+)$/',
            ),
        );
        if (strlen($planprice) > 0) {
            /**
             * If we find a match, set $price and possibly $currency and break.
             * $price_pattern['format'] will help us build the price after the loop
             */
            foreach ($price_patterns as $price_pattern) {
                if ( preg_match( $price_pattern['pattern'], trim( $planprice ), $matches) ) {
                    $price = $matches[ 'price' ];
                    break;
                }
            }
            /**
             * Prepare HTML
             */
            $currency = empty ( $matches[ 'currency' ] ) ? '$' : $matches[ 'currency' ];
            $currency = '<span class="sign">' . $currency . '</span>';
            $price = empty ( $matches[ 'price' ] ) ? '...' : $matches[ 'price' ];
            $text_before = empty ( $matches[ 'text_before' ] ) ? '' : '<div class="ptp-pricing-text">' . $matches[ 'text_before' ] . '</div>' ;
            $text_after = empty ( $matches[ 'text_after' ] ) ? '' : '<div class="ptp-pricing-text">' . $matches[ 'text_after' ] . '</div>' ;
            /**
             * Replace value and produce formatted price
             */
            $price_formatted = str_replace( 
                array( ':price', ':currency', ':text_before', ':text_after' ),
                array( $price, $currency, $text_before, $text_after ),
                $price_pattern['format']
            );
        }
    
        // Get button text
        $button_text = isset($column['buttontext'])?$column['buttontext']:'';

        // Get button url
        $button_url = isset($column['buttonurl'])?$column['buttonurl']:'';

        // Get custom shortcode button if any
        $custom_button = false;
        $shortcode_pattern = '|^\[shortcode\](?P<custom_button>.*)\[/shortcode\]$|';
        if ( 
            preg_match( $shortcode_pattern, $button_text, $matches) 
            ||
            preg_match( $shortcode_pattern, $button_url, $matches) 
        ) {
            $custom_button = $matches[ 'custom_button' ];
        }
    
        // Hide call to action button
        $tracking = get_option('dh_ptp_google_analytics');
        $table_name = addslashes($post->post_title)?addslashes($post->post_title):'untitled pricing table';
        $onclick = ($tracking == 1)?"onclick=\"if (_gaq) {_gaq.push(['_trackEvent', '".$table_name."', 'Button clicked', '".addslashes($column['planname'])."']);}\"":"";
        $open_in_new_tab = (isset($meta['design5-open-link-in-new-tab']) && $meta['design5-open-link-in-new-tab'])?'target="_blank"':'';
        $call_to_action_button =
            '<div class="ptp-data-holder">'.
                (($custom_button)?$custom_button:'<a href="'.$column['buttonurl'].'" class="btn sign-up" ' . $open_in_new_tab . ' '.$onclick.'>' . dh_ptp_fa_icons($column['buttontext']) . '</a>') .
            '</div>';
        
        if (isset($meta['design5-hide-call-to-action-buttons']) && $meta['design5-hide-call-to-action-buttons']) {
            $call_to_action_button = '';
        }
    
        // Hide empty rows
        $hide_empty_rows = isset($meta['design5-hide-empty-rows'])?true:false;
    
        // create the html code
        $pricing_table_html .= '
            <div class="span'.$columns.'">
                <div class="ptp-price-table '.$feature_class.'">
                    <div class="head">
                        '.$feature.'
                        <div class="ptp-price-holder">
                            <div class="ptp-plan-title"><h2>'.dh_ptp_fa_icons($planname).'</h2></div>

                            <div class="dg5-ptp-price">' . $price_formatted . '<div style="clear:both"></div></div>

                            <div class="ptp-pay-duration">' . dh_ptp_fa_icons($billingtimeframe) . '</div>
                        </div>
                    </div>
                    <div class="ptp-tabled-data">' .
                        dh_ptp_features_to_html_design5($planfeatures, dh_ptp_design5_max_number_of_features(), $hide_empty_rows) .
                        $call_to_action_button .
                    '</div>
                </div>
            </div>
        ';
    
        $loop_index++;
    }
    
    $pricing_table_html .=
                '</div>'.
            '</div>'.
        '</div>';

    return $pricing_table_html;
}

function dh_ptp_design5_max_number_of_features()
{
    global $meta;

    $max = 0;
    foreach ($meta['column'] as $column) {
        if(isset($column['planfeatures'])) {
            // get number of features
            $col_number_of_features = count( explode( "\n", $column['planfeatures'] ) );

            if ($col_number_of_features > $max) {
                $max = $col_number_of_features;
            }
        }
    }

    return $max;
}

function dh_ptp_features_to_html_design5($features, $max_number_of_features, $hide_empty_rows)
{
    // the string to be returned
    $output = '';

    // explode string into a useable array
    $dh_ptp_features = explode("\n", $features);
    
    // how many features does this column have?
    $total_features = count($dh_ptp_features);
    
    // add each feature to $dh_ptp_feature_html
    for ($i=0; $i<$max_number_of_features; $i++) {
        if ($i < $total_features && $dh_ptp_features[$i] != '') {
            $output .= '<div class="ptp-data-holder ptp-design5-row ptp-row-id-'.$i.'">' . dh_ptp_fa_icons($dh_ptp_features[$i], true) . '</div>';
        } else {
            if (!$hide_empty_rows) {
                $output .= '<div class="ptp-data-holder ptp-design5-row ptp-row-id-'.$i.'">&nbsp;</div>';
            }
        }
    }

    // return the features html
    return $output;
}

