<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 11/15/13
 * Time: 15:48
 */


// include our HTML generators for each table
include ( PTP_PLUGIN_PATH . '/includes/table-generation/design1-table.php');
include ( PTP_PLUGIN_PATH . '/includes/table-generation/design2-table.php');
include ( PTP_PLUGIN_PATH . '/includes/table-generation/design3-table.php');
include ( PTP_PLUGIN_PATH . '/includes/table-generation/design4-table.php');
include ( PTP_PLUGIN_PATH . '/includes/table-generation/design5-table.php');


/**
 * Output the CSS of the pricing table
 * 
 * @param int $it the id of the easy pricing table
 * @todo track output CSS to prevent outputting it twice
 */
function dh_ptp_easy_pricing_table_dynamic_css( $id, $minified = true ) {
    global $features_metabox;
    
    // Retrieve all meta data for easy pricing tables
    $args = array(
        'post_type' => 'easy-pricing-table',
        'p' => $id,
        'post_status' => 'any'
    );
    $query = new WP_Query($args);

    ob_start();    
    echo '<style type="text/css">';
    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            $meta = get_post_meta(get_the_ID(), $features_metabox->get_the_id(), true);
            
            // Print css style per table
            if (isset($meta['dh-ptp-fancy-flat-template']) && $meta['dh-ptp-fancy-flat-template'] == 'selected') {
                // Print simple flat
                dh_ptp_fancy_flat_css(get_the_ID(), $meta);
            } elseif (isset($meta['dh-ptp-stylish-flat-template']) && $meta['dh-ptp-stylish-flat-template'] == 'selected') {
                // Print stylish flat
                dh_ptp_stylish_flat_css(get_the_ID(), $meta);
            } elseif (isset($meta['dh-ptp-design4-template']) && $meta['dh-ptp-design4-template'] == 'selected') {
                dh_ptp_design4_css(get_the_ID(), $meta);
            } elseif (isset($meta['dh-ptp-design5-template']) && $meta['dh-ptp-design5-template'] == 'selected') {
                dh_ptp_design5_css(get_the_ID(), $meta);
            } else {
                // Print simple flat
                dh_ptp_simple_flat_css(get_the_ID(), $meta);
            }
            
        endwhile;
    endif;
    echo "</style>" . "\n";
    $css = ob_get_clean();

    wp_reset_postdata();

    if ( !empty( $minified ) ) {
        /**
         * Minify CSS (https://github.com/GaryJones/Simple-PHP-CSS-Minification/blob/master/minify.php)
         */
        $css = preg_replace( '/\s+/', ' ', $css );
        $css = preg_replace( '/;(?=\s*})/', '', $css );
        $css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );
        $css = preg_replace( '/ (,|;|\{|}|\(|\)|>)/', '$1', $css );
        $css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );
        $css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );
        $css = preg_replace( '/0 0 0 0/', '0', $css );
        $css = preg_replace( '/#([a-f0-9])\\1([a-f0-9])\\2([a-f0-9])\\3/i', '#\1\2\3', $css );
    }

    echo "\n$css\n\n";
}


/**
 * This function decides which table style we should create. It enqueue the appropriate CSS file and calls the appropriate function.
 *
 * @return string pricing table html
 */
function dh_ptp_generate_pricing_table($id, $hide = false)
{
    global $wp_styles;
    global $features_metabox;
    
    $meta = get_post_meta($id, $features_metabox->get_the_id(), TRUE);

    // Enqueue assets & IE Hacks
    wp_enqueue_style('ept-font-awesome');
    wp_enqueue_style('ept-foundation', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/foundation/foundation.css');
    wp_enqueue_script('ept-modernizr', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/foundation/modernizr.js', array('jquery'));
    wp_enqueue_script('ept-foundation', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/foundation/foundation.min.js', array('ept-modernizr', 'jquery'));
    wp_enqueue_script('ept-foundation-tooltip', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/foundation/foundation.tooltip.js', array('ept-modernizr', 'ept-foundation', 'jquery'));
    wp_enqueue_script('ept-ui-tooltip', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/ui-tooltip.js', array('ept-foundation-tooltip'));
    
    wp_enqueue_style('ept-ie-style', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/ui/ui-ie.css');
    $wp_styles->add_data('ept-ie-style', 'conditional', 'lt IE 9');

    /**
     * Output the CSS just before the HTML to prevent showing unformatted content flash
     */
    dh_ptp_easy_pricing_table_dynamic_css($id);
    
    // Figure out which table we have here
    if (isset($meta['dh-ptp-fancy-flat-template']) && $meta['dh-ptp-fancy-flat-template'] == 'selected') {
        
        /**
         * fancy flat
         */
        
        //include css
        wp_enqueue_style( 'fancy-flat-table-style', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/pricing-tables/fancy-flat/pricingtable.css'  );

        //call appropriate function
        return dh_ptp_generate_fancy_flat_pricing_table_html($id, $hide);
    } elseif (isset($meta['dh-ptp-stylish-flat-template']) && $meta['dh-ptp-stylish-flat-template'] == 'selected') {
        
        /**
         * stylish flat
         */
        
        // Include dark theme by default
        wp_enqueue_style( 'stylish-flat-table-style', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/pricing-tables/stylish-flat/css/pricingtable.css'  );

        //call appropriate function
        return dh_ptp_generate_stylish_flat_pricing_table_html($id, $hide);
    } elseif (isset($meta['dh-ptp-design4-template']) && $meta['dh-ptp-design4-template'] == 'selected') {
        /**
         * Design 4
         */
        
        // Include default theme
        wp_enqueue_style( 'design4-table-style', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/pricing-tables/design4/css/pricingtable.css'  );

        //call appropriate function
        return dh_ptp_generate_design4_pricing_table_html($id, $hide);
    } elseif (isset($meta['dh-ptp-design5-template']) && $meta['dh-ptp-design5-template'] == 'selected') {
        
        /**
         * Design 5
         */
        
        // Enqueue CSS
        wp_enqueue_style('design5-table-style', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/pricing-tables/design5/css/design5-common.css'  );
        
        //call appropriate function
        return dh_ptp_generate_design5_pricing_table_html($id, $hide);
    } else {
        /**
         * Default: simple flat
         */
        
        //include css
        wp_enqueue_style( 'dh-ptp-design1', PTP_PLUGIN_PATH_FOR_SUBDIRS . '/assets/pricing-tables/design1/pricingtable.css'  );

        //call appropriate function
        return dh_ptp_generate_simple_flat_pricing_table_html($id, $hide);
    }
}

