<?php

// Include required for dh_ptp_generate_pricing_table()
include ( PTP_PLUGIN_PATH . '/includes/table-generation/table-generator.php');

// Standard shortcode
function dh_ptp_message_shortcode($atts)
{
	// extract id shortcode
    extract(shortcode_atts( array('id' => ''), $atts));  

    // check if id exists
    if ($id != '' ) {
    	global $features_metabox;
		$meta = get_post_meta($id, $features_metabox->get_the_id(), TRUE);

        // check if our pricing table contains any content
		if ($meta != "") {
            // if the table contains content, call the function that generates the table
			return do_shortcode(dh_ptp_generate_pricing_table($id));
		}
    }
	
	return __('Pricing table does not exist. Please check your shortcode.', PTP_LOC);
}
add_shortcode('easy-pricing-table', 'dh_ptp_message_shortcode');

// Switch shortcode
function dh_ptp_switch_shortcode($atts)
{
	// extract shortcode params
    $params = 
		shortcode_atts(
			array(
				'font_color' => '',
				'background_color' => '',
				'border_color' => '',
				'default_title' => '',
				'default_subtitle' => '',
				'default_pricing_table_id' => '',
				'alternate_title' => '',
				'alternate_subtitle' => '',
				'alternate_pricing_table_id' => ''
			),
			$atts
		);

	// Check if all parameters set
	$check = $params;
	unset($check['default_subtitle'], $check['alternate_subtitle']);
	foreach($check as $param) {
		if (strlen($param) == 0) {
			return __('All mandatory parameters must be specified for switch pricing table shortcode!', PTP_LOC);
		}
	}
	
	// Check if the table ids are not the same
	if ($params['default_pricing_table_id'] == $params['alternate_pricing_table_id']) {
		return __('Default and alternate pricing table id\'s must be different!', PTP_LOC);
	}
	
	// Additional options
	$settings = get_post_meta($params['default_pricing_table_id'], '1_dh_ptp_settings', true);
	$selected = '';
	if (isset($settings['dh-ptp-simple-flat-template']) && $settings['dh-ptp-simple-flat-template'] == 'selected') { $selected = 'design1'; }
	if (isset($settings['dh-ptp-fancy-flat-template']) && $settings['dh-ptp-fancy-flat-template'] == 'selected') { $selected = 'design2'; }
	if (isset($settings['dh-ptp-stylish-flat-template']) && $settings['dh-ptp-stylish-flat-template'] == 'selected') { $selected = 'design3'; }
	if (isset($settings['dh-ptp-design4-template']) && $settings['dh-ptp-design4-template'] == 'selected') { $selected = 'design4'; }
	if (isset($settings['dh-ptp-design5-template']) && $settings['dh-ptp-design5-template'] == 'selected') { $selected = 'design5'; }
	
	//switch defaults
	$rounded_corners = '';
	$font_family = '';
	$font_size = '';
	$box_shadow = '';
	
	$subtitle_color = '';
	$subtitle_font_size = '';
	$subtitle_font_family = '';
	
	$margin_top = '';
	$margin_bottom = '';
	
	// switch
	switch ($selected) {
		case 'design1':
			// switch
			$rounded_corners = isset($settings['rounded-corners'])?$settings['rounded-corners']:'0px';
			$font_family = 'inherit';
			$font_size = '1em';
			
			// subtitle
			$design1_plan_name_font_size = isset($settings['plan-name-font-size'])?$settings['plan-name-font-size']:1;
			$design1_plan_name_font_size_type = isset($settings['plan-name-font-size-type'])?$settings['plan-name-font-size-type']:"em";
			$subtitle_color = '#8D8D8D';
			$subtitle_font_size = $design1_plan_name_font_size.$design1_plan_name_font_size_type;
			$subtitle_font_family = 'inherit';
			
			// margins
			$margin_top = '25px';
			$margin_bottom = '25px';
			break;
		case 'design2':
			// switch
			$rounded_corners = '5px;';
			$font_family = 'Helvetica';
			$font_size = '16px';
			
			// subtitle
			$subtitle_color = '#8D8D8D';
			$subtitle_font_size = '20px';
			$subtitle_font_family = 'Helvetica';
			
			// margins
			$margin_top = '25px';
			$margin_bottom = '25px';
			break;
		case 'design3':
			// switch
			$rounded_corners = '3px;';
			$font_family = 'inherit';
			$font_size = '16px';

			// subtitle
			$subtitle_color = '#121212';
			$subtitle_font_size = '20px';
			$subtitle_font_family = 'inherit'; // fix
			
			// margins
			$margin_top = '15px';
			$margin_bottom = '35px';
			break;
		case 'design4':
			// switch
			$rounded_corners = '5px;';
			$font_family = 'inherit'; // fix
			$font_size = '16px';
			$box_shadow = '-webkit-box-shadow: 0 0 10px 0 #dbdbdb; box-shadow: 0 0 10px 0 #dbdbdb;';
			
			// subtitle
			$subtitle_color = '#8C8C8C';
			$subtitle_font_size = '20px';
			$subtitle_font_family = 'inherit'; // fix
			
			// margins
			$margin_top = '15px';
			$margin_bottom = '10px';
			break;
		case 'design5':
			// switch
			$rounded_corners = '5px';
			$font_family = 'Open Sans Condensed';
			$font_size = '16px';
			$box_shadow = 'box-shadow: 0px 2px 2px #8C8C8C; -webkit-box-shadow: 0px 2px 2px #8C8C8C;';
			
			// subtitle
			$subtitle_color = '#8C8C8C';
			$subtitle_font_size = '20px';
			$subtitle_font_family = 'Open Sans Condensed';
			
			// margins
			$margin_top = '15px';
			$margin_bottom = '10px';
			break;
	}
	
	if ($params['default_pricing_table_id'] != '' && $params['alternate_pricing_table_id'] != '') {
		// Switch style
		$switch_style = 'width: 350px; display: block; margin: 0 auto; border-radius: '.$rounded_corners.'; '.$box_shadow;

		// Common button styles
		$link_styles = '
			float:left;
			width: calc(50% - 1px);
			padding-top: 8px;
			padding-bottom: 7px;
			text-align: center;
			text-decoration: none;
			border-bottom: 1px solid '.$params['border_color'].';
			border-top: 1px solid '.$params['border_color'].';
			font-family: '.$font_family.';
			font-size: '.$font_size.';
		';
		
		// Subtitle style
		$subtitle_styles = '
			width: 350px;
			font-family: '.$subtitle_font_family.';
			font-size: '.$subtitle_font_size.';
			color: '.$subtitle_color.';
			padding-top: '.$margin_top.';
			padding-bottom: '.$margin_bottom.';
		';
		
		$id = 'dh_ptp_table_switch_'.$params['default_pricing_table_id'].'_'.$params['alternate_pricing_table_id'];
		$output =
			'<div id="'.$id.'">'.
				'<div class="dh_ptp_switch" style="'.$switch_style.'">'.
					'<a href="#" style="'.$link_styles.' border-left: 1px solid '.$params['border_color'].'; border-top-left-radius: '.$rounded_corners.'; border-bottom-left-radius: '.$rounded_corners.'; color: '.$params['background_color'].'; background: '.$params['font_color'].';" data-id="'.$params['default_pricing_table_id'].'" title="'.$params['default_title'].'">'.$params['default_title'].'</a>'.
					'<a href="#" style="'.$link_styles.' border-right: 1px solid '.$params['border_color'].'; border-top-right-radius: '.$rounded_corners.'; border-bottom-right-radius: '.$rounded_corners.'; color: '.$params['font_color'].'; background: '.$params['background_color'].';" data-id="'.$params['alternate_pricing_table_id'].'" title="'.$params['alternate_title'].'">'.$params['alternate_title'].'</a>'.
					'<div style="clear:both;"></div>'.
				'</div>'.
				'<div class="dh_ptp_switch_subtitle" style="'.$subtitle_styles.' display: block; margin: 0 auto; text-align: center;">' .
					'<span class="subtitle_'.$params['default_pricing_table_id'].'">'.$params['default_subtitle'] . '</span>' .
					'<span class="subtitle_'.$params['alternate_pricing_table_id'].'" style="display:none; ">'.$params['alternate_subtitle'] . '</span>' .
				'</div>';
		
		// Pricing tables
		$output .= do_shortcode(dh_ptp_generate_pricing_table($params['default_pricing_table_id']));
		$output .= do_shortcode(dh_ptp_generate_pricing_table($params['alternate_pricing_table_id'], true));
		
		// Javascript
		$output .= '<script type="text/javascript">';
		$output .= 'jQuery(document).ready(function($){ 
						$("#'.$id.' .dh_ptp_switch a").on("click", function(){ 
							// Get the two switch elements
							$elem_clicked = $(this);
							$elem_other = $( "#' . $id . ' .dh_ptp_switch a" ).not( $elem_clicked );
							// Get the colors
							clicked_color = $elem_clicked.css( "color" );
							clicked_bg = $elem_clicked.css( "background-color" );
							other_color = $elem_other.css( "color" );
							other_bg = $elem_other.css( "background-color" );
							// Switch colors
							$elem_clicked.css( { "color": other_color, "background-color": other_bg } );
							$elem_other.css( { "color": clicked_color, "background-color": clicked_bg } );
							// Toggle subtitles visibility
							$( "[class^=subtitle_]", "[id^=dh_ptp_table_switch_][id*='.$id.']" ).toggle();
							// Toggle pricing tables visibility
							$( "[id^=ptp-]", "[id^=dh_ptp_table_switch_][id*='.$id.']" ).toggle();
							return false; 
						});
					});';
		$output .= '</script>';
		
		$output .= '</div>';
		
		return $output;
	}
	
	return __('Pricing table(s) does not exist. Please check your shortcode.', PTP_LOC);
}
add_shortcode('easy-pricing-switch', 'dh_ptp_switch_shortcode');
?>