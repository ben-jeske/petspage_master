jQuery(document).ready(function($) {
    // Activate jquery ui tabs
    $("#dh_ptp_tabs_container").tabs();

	// Save tab state to dh_ptp_tab
	$("a[href=#dh_ptp_tabs_1], a[href=#dh_ptp_tabs_2], a[href=#dh_ptp_tabs_3]").on('click', function(){
		$('#dh_ptp_tab').val($(this).attr('href'));
	});

    //drag and drop for columns
    $("#wpa_loop-column").sortable({ axis: "x" });

	//activate color pickers
    $('.button-color').wpColorPicker({
	    palettes: ['#1abc9c', '#2ecc71','#3498db', '#9b59b6', '#34495e', '#f1c40f', '#e67e22', '#e74c3c', '95a5a6']
    });
    $('.button-border-color').wpColorPicker({
    	    palettes: ['#16a085', '#27ae60','#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400', '#c0392b', '7f8c8d']
    });
	$('.colorpicker-no-palettes').wpColorPicker();
	
	//$('.design4-color').wpColorPicker({
	//	palettes: ['#6baba1', '#e0a32e', '#e7603b', '#9ca780']
	//});

	 //make sure that only decimal numbers are allowed to input. 
	 //source: http://jqueryexamples4u.blogspot.in/2013/09/validate-input-field-allows-only-float.html
	 $('.float-input').keypress(function(event) {
	      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
	        event.preventDefault();
	      } 
	});

    //enable lightbox
    $(".inline-lightbox").colorbox({inline:true, width:"50%", speed: 0, fadeOut: 0});

	//set settings visibility based on currently selected theme
	setAdvancedDesignSettingsVisibility('.template-selected');
	
	/* color palettes */
	$('.palette').colorpalettes({
		palettes: ['#34495e/#c0392b', '#0e9577/#ce3306', '#66317d/#0a8bb0', '#08a7bd/#a81818', '#138f6b/#46b527', '#417d83/#ee4c7e', '#27b18f/#ecce44', '#b97a68/#f37e5d', '#114d68/#9e0165', '#444588/#ee1367'],
		change: function(event, ui) {
			// Handle palette click event
			var ballon = $(this).closest('.dh_ptp_color_palettes_container');
			var new_colors = ui.color.split('/');
			
			if (new_colors.length > 1) {
				ballon.find('.dh_ptp_color_palettes_result').css('background-color', new_colors[0]);
				ballon.find('.dh_ptp_color_palettes_result span').css('background-color', new_colors[1]);
				ballon.find('.dh_ptp_color_palettes_result span').css('display', 'block');
			} else {
				ballon.find('.dh_ptp_color_palettes_result').css('background-color', new_colors[0]);
				ballon.find('.dh_ptp_color_palettes_result span').css('display', 'none');
			}
			
		}
	});
	$('.dh_ptp_color_palettes_result').on('click', function(){
		if ($(this).parent().find('.color-palettes').is(":visible")) {
			$(this).parent().find('.color-palettes').hide();
		} else {
			$(this).parent().find('.color-palettes').show();
		}
	});
	
	/* easy palette button */
	$('.color-palette.ptp-fancy-column-color-scheme').easypalette({
		palettes: ['#f77fa8', '#ef5f54', '#f15477', '#a176c3', '#6b89c9', '#49a9d3', '#59b7b7', '#9ca46b', '#92d590', '#b9c869', '#79714c', '#9d7d60', '#dca562', '#ffa14f', '#ffe177'],
		change: function(event, ui) {
			// Handle palette click event
			var ballon = $(this).closest('.dh_ptp_easy_palette_container');
			var new_color = ui.color.toString();

			ballon.find('.dh_ptp_palette_result').css('background-color', new_color);
		}
	});
	
	$('.color-palette.ptp-stlyish-column-color-scheme').easypalette({
		palettes: ['#456366', '#696758', '#36393B', '#496449', '#3f4953', '#a64324', '#712941', '#493c45', '#742222', '#b28d19'],
		change: function(event, ui) {
			// Handle palette click event
			var ballon = $(this).closest('.dh_ptp_easy_palette_container');
			var new_color = ui.color.toString();

			ballon.find('.dh_ptp_palette_result').css('background-color', new_color);
		}
	});
	
	/* Design 4 column color scheme */
	$('.color-palette.ptp-design4-color-scheme').easypalette({
		palettes: ['#6baba1', '#e0a32e', '#e7603b', '#9ca780'],
		change: function(event, ui) {
			// Handle palette click event
			var ballon = $(this).closest('.dh_ptp_easy_palette_container');
			var new_color = ui.color.toString();

			ballon.find('.dh_ptp_palette_result').css('background-color', new_color);
		}
	});
	
	
	$('.dh_ptp_palette_result').on('click', function(){
		if ($(this).parent().find('.easy-palette').is(":visible")) {
			$(this).parent().find('.easy-palette').hide();
		} else {
			$(this).parent().find('.easy-palette').show();
		}
	});
	
	// Close when clicked outside 
	$( 'body' ).click( function( event ) {
		var $target = $(event.target);
		if (!$target.parents().is(".dh_ptp_easy_palette_container")){
		   $(".easy-palette").hide();
	    }
		if (!$target.parents().is(".dh_ptp_color_palettes_container")) {
			$(".color-palettes").hide();
		}
	});
	
	// Save & Preview button
	$('#save_preview').on('click', function(event) {
        event.preventDefault();
		
		// Add target
		var form = $(this).closest('form');
		form.prop('target', '_blank');
		
		// Add preview_url parameter
		var url = $(this).attr('data-url');
		if ($('#dh_ptp_preview_url')) {
			$('#dh_ptp_preview_url').remove();
		}
		var preview_url_input = '<input type="hidden" name="dh_ptp_preview_url" id="dh_ptp_preview_url" value="' + url + '"/>';
		$(this).after(preview_url_input);
		
		// Submit form
		form.submit();
		  
        return false;
	});
	
	$('#save').on('click', function(event) {
        event.preventDefault();
		
		// Add target
		var form = $(this).closest('form');
		form.removeAttr('target');

		// Remove preview url
		$('#dh_ptp_preview_url').remove();
		
		// Submit form
		form.submit();
		  
        return false;
	});
	
	// Design 2 options
	$('.design2-choose-your-colors').change(function() {
		if ($(this).is(':checked') && $(this).val() == 2) {
			$('.design2-color-scheme-1').addClass('hide');
			$('.design2-color-scheme-2').removeClass('hide');
		} else {
			$('.design2-color-scheme-1').removeClass('hide');
			$('.design2-color-scheme-2').addClass('hide');
		}		
	});
	
	// Design 3 options
	$('.design3-choose-your-colors').change(function() {
		if ($(this).is(':checked') && $(this).val() == 2) {
			$('.design3-color-scheme-1').addClass('hide');
			$('.design3-color-scheme-2').removeClass('hide');
		} else {
			$('.design3-color-scheme-1').removeClass('hide');
			$('.design3-color-scheme-2').addClass('hide');
		}		
	});
	
	// Design 5 options
	$('.design5-choose-your-colors').change(function() {
		if ($(this).is(':checked') && $(this).val() == 2) {
			$('.design5-color-scheme-1').addClass('hide');
			$('.design5-color-scheme-2').removeClass('hide');
		} else {
			$('.design5-color-scheme-1').removeClass('hide');
			$('.design5-color-scheme-2').addClass('hide');
		}		
	});
});

//activate twitter bootstrap popover
jQuery(function ($)
  { 
    $(".ptp-icon-help-circled").popover();  
    $(".plan-title #delete-button").popover({placement:'top'});  
    $(".plan-title .feature-button").popover({placement:'top'});  
  });  


// handle clicks on featured button
function buttonHandler(el)
{
	// required for wordpress
	var $ = jQuery;

	// toggle active button via css
	function toggleButtonClasses(el)
	{
		$(el).toggleClass('ptp-icon-star-empty');
		$(el).toggleClass('ptp-icon-star');
	}
	
	//toggle the value of our hidden input
	function setInputValue(el)
	{
		if($(el).val()=="unfeatured" || $(el).val()=="")
			$(el).val("featured");
		else if($(el).val()=="featured")
			$(el).val("unfeatured");
	}

	// toggles the elements class and value
	function myButtonClickHandler(el)
	{
		
		toggleButtonClasses(el);
		setInputValue(el.prev());

	}

	// use hasClass to figure out if current item is selected or not
	if (!$(el).hasClass('ptp-icon-star')) {
		// if the clicked item is not featured, unfeature the currently featured item ('.ptp-icon-star') by sending it to myButtonClickHandler
		myButtonClickHandler($('.ptp-icon-star'));
	}

	//	feature the clicked item by sending it to myButtonClickHandler
	myButtonClickHandler( $(el));

 	return false;
}


// handle clicks on featured button
function templateSelectorClickedHandler(el)
{
	// required for wordpress
	var $ = jQuery;

	// toggle active button via css
	function toggleButtonClasses(el)
	{
		$(el).toggleClass('template-selected');
	}
	
	//toggle the value of our hidden input
	function setInputValue(el)
	{
		if($(el).val()=="not-selected" || $(el).val()=="")
			$(el).val("selected");
		else if($(el).val()=="selected")
			$(el).val("not-selected");
	}

	// toggles the elements class and value
	function myButtonClickHandler(el)
	{
		
		toggleButtonClasses(el);
		setInputValue(el.find('.template-hidden-input'));

		//change visibility of advanced design settings
		setAdvancedDesignSettingsVisibility(el);


		//changeButtonText(el.find('.template-button'));

	}

	//toggle button text - disabled for now
	/*
	function changeButtonText(el)
	{
		if($(el).text()=="Use This Template")
			$(el).text("In Use");
		else if($(el).text == "In Use")
			$(el).text("Use This Template");
	}*/

	// use hasClass to figure out if current item is selected or not
	if (!$(el).parent().hasClass('template-selected')) {
		// if the clicked item is not featured, unfeature the currently featured item ('.ptp-icon-star') by sending it to myButtonClickHandler
		myButtonClickHandler($('.template-selected'));

		//now feature the current item
		myButtonClickHandler( $(el).parent());

	}

	return false;
}

//set settings visibility 
function setAdvancedDesignSettingsVisibility(el)
{
	// required for wordpress
	var $ = jQuery;

	// Set default height for features description column
	$('.features.explaination-desc').css('height', '125px');
	
	if ($(el).attr('id') == "simple-flat-selector") {
		// Settings
		$('#simple-flat-advanced-design-settings').show();
		$('#fancy-flat-advanced-design-settings').hide();
		$('#stylish-flat-advanced-design-settings').hide();
		$('#design4-advanced-design-settings').hide();
		$('#design5-advanced-design-settings').hide();
		
		// Content
		$('.ptp-standard-content').show();
		$('.ptp-design4-content').hide();
		$('.ptp-design5-content').hide();
		
		// Price field placeholder
		updatePlaceholder('e.g. $49/mo');
	} else if ($(el).attr('id') == "fancy-flat-selector") {
		// Settings
		$('#simple-flat-advanced-design-settings').hide();
		$('#fancy-flat-advanced-design-settings').show();
		$('#stylish-flat-advanced-design-settings').hide();
		$('#design4-advanced-design-settings').hide();
		$('#design5-advanced-design-settings').hide();
		
		// Content
		$('.ptp-standard-content').show();
		$('.ptp-design4-content').hide();
		$('.ptp-design5-content').hide();
		
		// Price field placeholder
		updatePlaceholder('e.g. $49/mo');
	} else if ($(el).attr('id') == "stylish-flat-selector") {
		// Settings
		$('#simple-flat-advanced-design-settings').hide();
		$('#fancy-flat-advanced-design-settings').hide();
		$('#stylish-flat-advanced-design-settings').show();
		$('#design4-advanced-design-settings').hide();
		$('#design5-advanced-design-settings').hide();
		
		// Content
		$('.ptp-standard-content').show();
		$('.ptp-design4-content').hide();
		$('.ptp-design5-content').hide();
		
		// Price field placeholder
		updatePlaceholder('e.g. $49/mo');
	} else if ($(el).attr('id') == "design4-selector") {
		// Settings
		$('#simple-flat-advanced-design-settings').hide();
		$('#fancy-flat-advanced-design-settings').hide();
		$('#stylish-flat-advanced-design-settings').hide();
		$('#design4-advanced-design-settings').show();
		$('#design5-advanced-design-settings').hide();
		
		// Content
		$('.ptp-standard-content').hide();
		$('.ptp-design4-content').show();
		$('.ptp-design5-content').hide();
		
		// Price field placeholder
		updatePlaceholder('e.g. $49.99');
	} else if ($(el).attr('id') == "design5-selector") {
		// Settings
		$('#simple-flat-advanced-design-settings').hide();
		$('#fancy-flat-advanced-design-settings').hide();
		$('#stylish-flat-advanced-design-settings').hide();
		$('#design4-advanced-design-settings').hide();
		$('#design5-advanced-design-settings').show();
		
		// Content
		$('.ptp-standard-content').show();
		$('.ptp-design4-content').hide();
		$('.ptp-design5-content').show();
		
		// CSS Tricks
		$('.features.explaination-desc').css('height', 'auto');
		
		// Price field placeholder
		updatePlaceholder('e.g. $49.99');
	}
}

function updatePlaceholder(placeholder_text) {
	jQuery('.planprice').each(function(index) {
		jQuery( this ).attr('placeholder', placeholder_text);
	});
}