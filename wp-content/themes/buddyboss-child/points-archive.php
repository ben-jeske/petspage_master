<?php
/**
 * myCRED Rank Archive
 * @since 0.1
 * @version 1.0
 */
get_header();
 
// Load myCRED (if installed)
$mycred = NULL;
if ( function_exists( 'mycred_get_settings' ) )
        $mycred = mycred_get_settings();
 
$user_id = false;
$progress = false;
echo "asdasdasdasdasdasdas";exit;
// Only show the progress bar for logged in users.
if ( is_user_logged_in() ) {
        // Grab the current user id
        $user_id = get_current_user_id();
 
        
        // Make sure the current user is not excluded from using myCRED
        if ( ! $mycred->exclude_user( $user_id ) ) {
                // If ranks are based on total gains
                if ( $mycred->rank['base'] == 'total' )
                        $key = $mycred->get_cred_id() . '_total';
                // Else ranks are based on the current balance
                else
                        $key = $mycred->get_cred_id();
 
                // Grab the "balance" or "total"
                $users_balance = $mycred->get_users_cred( $user_id, $key );
        }
}
 
// Highlight admins
$admin = false;
if ( is_user_logged_in() && current_user_can( 'edit_users' ) )
        $admin = true; ?>
 
        <div class="row">
                <div class="col-md-12" id="content-header">
                        <h1 class="entry-title" id="bp-klein-page-title">myCRED Membership Ranks</h1>
                </div>
                <div class="klein-breadcrumbs col-md-12">
                        <a title="Go to myCRED." href="http://mycred.me" class="home">myCRED</a><span class="breadcrumb-separator">/</span><a title="Go to Members." href="http://mycred.me/members/" class="members">Members</a><span class="breadcrumb-separator">/</span>Ranks
                </div>
        </div>
       
        <div class="row">
                <section id="primary" class="content-area col-md-8 col-sm-8">
                        <div id="content" class="site-content" role="main">
 
                        <!-- Lets start the WordPress loop -->
                        <?php if ( have_posts() ) : ?>
                                <?php while ( have_posts() ) : the_post(); ?>
 
                                <article id="post-<?php the_ID(); ?>" <?php post_class( array( 'blog-list' ) ); ?>>
       
                                <div class="entry-content">
                                        <div class="blog-author">
                                        <?php if ( function_exists( 'bp_core_fetch_avatar' ) ) { ?>
                                                <div class="blog-author-avatar"<?php if ( has_post_thumbnail() ) echo ' style="background-color:white;"'; ?>>
                                                        <?php
        // The default rank does not have a logo, instead we insert the users avatar
        if ( has_post_thumbnail() )
                the_post_thumbnail( 'thumbnail' );
        else
                echo bp_core_fetch_avatar( array( 'type' => 'full', 'item_id' => get_current_user_id() ) ); ?>
                                                </div>
                                                <div class="blog-author-name center"></div>
                                                <div class="blog-author-links"></div>
                                <?php } else { ?>
                                        <div class="blog-author-avatar no-bp"><?php echo get_avatar( get_current_user_id(), 75 ); ?></div>
                                        <div class="blog-author-name no-bp center">
                                                <a class="tip" data-delay="0" data-placement="bottom" data-original-title="<?php _e( 'Posts', 'klein' ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                                                        <?php echo get_the_author(); ?>
                                                </a>
                                        </div>
                                <?php } ?>
                        </div>
                        <div class="blog-content">
                                <div class="blog-pad blog-content-title">
                                        <h2>Rank: <?php the_title(); ?></h2>
                                </div>
                                <div class="blog-pad blog-content-excerpt">
                                        <?php
                                        // Get the rank details (ID, Minimum and Maximum points
                                        $post_id = get_the_ID();
                                        $min = get_post_meta( $post_id, 'mycred_rank_min', true );
                                        $max = get_post_meta( $post_id, 'mycred_rank_max', true );
                                        ?>
 
                                                <p>This rank requires a minimum token earnings of <?php echo $mycred->format_creds( $min ); ?>.</p>                    
                                        <?php if ( is_user_logged_in() ) : ?>
 
                                                <h5>Your rank progress</h5>
                                                <?php
                                        // Calculate progress in percentage with one decimal
                                        $progress = number_format( ( ( $users_balance / $max ) * 100 ), 1 );
                                        $options = 'options="striped,animated"';
 
                                        // If we have surpassed this role show 100
                                        if ( $users_balance > $min && $users_balance > $max ) {
                                                $progress = number_format( 100, 1 );
                                                $options = '';
                                        }
                                        // If we have not yet come up to this rank
                                        elseif ( $users_balance < $min ) {
                                                $progress = number_format( 0, 1 );
                                                $options = '';
                                        }
 
                                        // Lets use the Visual Editors "Progress Bar"
                                        echo do_shortcode( '[vc_row][vc_column width="1/1"][vc_progress_bar values="' . $progress . '|' . get_the_title() . '" bgcolor="custom" ' . $options . ' custombgcolor="#E74C3C" title="" units="%"][/vc_column][/vc_row]' ); ?>
                                        <?php endif; ?>
                                       
                                        <h5>Privileges</h5>
                                        <?php
                                       
                                        // Show rank privileges
                                        $priv = array();
                                        if ( get_post_meta( get_the_ID(), 'allow_messages', true ) )
                                                $priv[] = 'Send messages in BuddyPress.';
                               
                                        if ( ! empty( $priv ) ) {
                                                echo '<ul>';
                                               
                                                foreach ( $priv as $privilige )
                                                        echo '<li>' . $privilige . '</li>';
                                               
                                                echo '</ul>';
                                        }
                                        else {
                                                echo '<p>This rank has no special privileges.</p>';
                                        } ?>
 
                                        <?php
                                        // Get some users of this rank, 9 fits nicely with our theme
                                        $users_of_rank = mycred_get_users_of_rank( get_the_ID(), 9 );
                                        if ( ! empty( $users_of_rank ) ) {
                                                echo '<small><strong>my</strong>CRED members with this rank:</small><br />';
                                                echo '<div class="mycred-users-of-rank">';
                                                // Loop though users and show their avatar
                                                foreach ( $users_of_rank as $member )
                                                        echo '<a href="' . bp_core_get_user_domain( $member['user_id'] ) . '" style="display: block; float:left;">' . get_avatar( $member['user_id'], 50 ) . '</a>';
                                                echo '</div>';
                                        }
                                        else {
                                                echo '<p>There are no members with this rank.</p>';
                                        }
                                        ?>
 
                                </div>
                                <div class="blog-pad blog-content-meta">
                                        <footer class="entry-meta">&nbsp;</footer>
                                </div> 
                        </div>
                        <!-- blog content end -->
        </div><!-- .entry-content -->
        <div class="clear"></div>
</article><!-- #post-## -->
       
                                <?php endwhile; ?>
       
                                <?php klein_content_nav( 'nav-below' ); ?>
       
                        <?php else : ?>
       
                                <?php get_template_part( 'no-results', 'archive' ); ?>
       
                        <?php endif; ?>
       
                        </div><!-- #content -->
                </section><!-- #primary -->
       
                <div class="col-md-4 col-sm-4">
                        <?php get_sidebar(); ?>
                </div>
               
        </div><!--.row-->      
<?php get_footer(); ?>
