<?php
/**
// Template name: Member
 */
 if ( is_user_logged_in() ) 
{
	add_filter('add_to_cart_text', 'woo_custom_cart_button_text');
	function woo_custom_cart_button_text() 
	{
						return __('Upgrade', 'woocommerce');
	}
}

if ( !is_user_logged_in() ) 
{
	add_filter('add_to_cart_text', 'woo_custom_cart_button_text');
	function woo_custom_cart_button_text() 
	{
						return __('Sign Up', 'woocommerce');
	}
}

get_header(); ?>
<style>
@charset "utf-8";
/* CSS Document */
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	color:#676767;
	font-weight:400;
}
* {
	margin:0px;
	padding:0px
}
a {
	text-decoration:none;
}
img, a img {
    border:none;
    outline:none;
}
h1, h2, h3, h4, h5, h6 {
	font-family: 'HelveticaLTStdLightCondensed';
}
p {
	font-family: 'HelveticaLTStdLightCondensed';
}
ul, ol {
	list-style:none;
}
#san_container {
	width:100%;
	float:left;
	margin:0px;
	padding:0px;
	background:#F6F7F8;
}
#san_wrapper {
	width:910px;
	margin:0px auto;
	padding:0px;
}
.san_section {
	width:100%;
	float:left;
	margin:0px;
	padding:0px;
}
.san_section h2 {
	width:100%;
	float:left;
	margin:0px;
	padding:20px 0px;
	font-family: 'HelveticaLTStdRoman';
	font-size:22px;
	line-height:30px;
	font-weight:normal;
	color:#1e1e1e;
}
.san_main_area {
	width:100%;
	float:left;
	margin:0px;
	padding:0px 0px 0px 0px;
}
.san_left_area {
	width:300px;
	float:left;
	margin:0px;
	padding:0px 0px;
	border-top:1px solid #c9c9c9;
	border-left:1px solid #c9c9c9;
	height:140px;
}
.san_left_area h3 {
	width:98%;
	float:left;
	margin:0px;
	color:#664688;
	font-family: 'HelveticaLTStdRoman';
	font-size:22px;
	line-height:30px;
	font-weight:normal;
	padding:55px 0px;
	text-align:center;
}
.san_left_area p {
	width:93%;
	float:left;
	margin:0px;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	line-height:24px;
	font-weight:normal;
	padding:10px;
}
.san_center_area {
	width:300px;
	float:left;
	margin:0px;
	padding:0px 0px;
	border-top:1px solid #c9c9c9;
	border-left:1px solid #c9c9c9;
	height:140px;
}
.san_center_area h3 {
	width:93%;
	float:left;
	margin:0px;
	color:#664688;
	font-family: 'HelveticaLTStdRoman';
	font-size:22px;
	line-height:30px;
	font-weight:normal;
	padding:10px;
	height:120px;
	text-align:center;
}
.san_center_area h5 {
	width:100%;
	float:left;
	margin:0px;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	line-height:30px;
	font-weight:normal;
	padding:30px 0px 5px;
	text-align:center;
}
.san_center_area a {
	float:left;
	margin:0px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	font-weight:normal;
	padding:5px 20px 7px;
	margin:0px 33%;
	background:#523071;
	color:#ffffff;
}
.san_center_area a:hover {
	background:#300948;
	color:#ffffff;
}
.san_right_area {
	width:300px;
	float:left;
	margin:0px;
	padding:0px 0px;
	border-top:1px solid #c9c9c9;
	border-left:1px solid #c9c9c9;
	border-right:1px solid #c9c9c9;
}
.san_right_area h3 {
	width:93%;
	float:left;
	margin:0px;
	color:#664688;
	font-family: 'HelveticaLTStdRoman';
	font-size:22px;
	line-height:30px;
	font-weight:normal;
	padding:10px;
	height:120px;
}
.san_right_area p {
	width:93%;
	float:left;
	margin:0px;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	line-height:24px;
	font-weight:normal;
	padding:10px;
}
.san_right_area p span {
	font-weight:bold;
}
.san_section h2 span {
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	color:#1e1e1e;
	float:left; line-height:24px;
}
.san_section h4 {
	float:left;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	line-height:24px;
	font-weight:normal;
	width:100%;
	padding:0px;
}
/*-----------------------------------------Update-----------------------------------------*/
#rav_container {
	width:100%;
	float:left;
	margin:0px;
	padding:0px;

}
#rav_wrapper {
	width:1000px;
	margin:0px auto;
	padding:0px;
}
.rav_section {
	width:100%;
	float:left;
	margin:0px;
	padding:0px;
}
.rav_section h2 {
	width:100%;
	float:left;
	margin:0px;
	padding:20px 0px;
	font-family: 'HelveticaLTStdRoman';
	font-size:22px;
	line-height:30px;
	font-weight:normal;
	color:#1e1e1e;
}
.rav_heading{width:100%; float:left; margin:0px; padding:0px; border-top:1px solid #3B1555; border-bottom:1px solid #3B1555; background:#644487;}
.rav_heading_area{width:16.5%; float:left; padding:5px 0px; margin:0px 5px;}
.rav_heading_area h3{width:100%; float:left; margin:0px; padding:28px 0px; font-family:Arial, Helvetica, sans-serif; font-size:17px; line-height:24px;  color:#ffffff; text-align:center; font-weight:normal; }
.rav_heading_area h3 a{width:100%; float:left; margin:0px; padding:0px 0px; font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#ffffff; text-align:center; font-weight:normal; }
.rav_heading_area h3 a:hover{color:#ffffff;}
#cat1{ background:#5a387a;}
.rav_heading_inner{width:30%; float:left;}
.rav_heading_inner h3{width:95%; float:left; margin:0px; padding:32px 0px 35px 10px; font-family:Arial, Helvetica, sans-serif; font-size:20px; line-height:24px;  color:#ffffff !important; font-weight:normal;  }
#vis_area h3{padding:25px 0px 25px 10px !important; float:left;}
.rav_inner{ width:100%; float:left; margin:0px; padding:0px; background:#ffffff; border-bottom:1px solid #bcbcbc;}
.rav_inner_left{width:30%; float:left; margin:0px; padding:0px;}
.rav_inner_left p {
	width:93%;
	float:left;
	margin:0px;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	line-height:24px;
	font-weight:normal;
	padding:20px 0px 20px 10px;
	font-weight:bold;
}
.rav_inner_area{width:16.5%; float:left; margin:0px 5px !important; padding:0px ;}
.rav_inner_area p {
	width:100%;
	float:left;
	margin:0px;
	color:#1e1e1e;
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	line-height:24px;
	font-weight:normal;
	padding:10px 0px 10px;
	text-align:center;
}
.rav_inner_area img{width:auto; float:left; margin:0px 0px 0px 42%; padding:22px 0px;}
#rav_price{background:#b8a1c5; float:left; margin:0px; }
#rav_price1{padding:0px 0px; background:#b8a1c5;}
.rav_inner_area a {
	float:left;
	margin:0px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17px;
	font-weight:normal;
	padding:10px 20px 12px;
	margin:10px 7% 20px;
	background:#523071;
	color:#ffffff;
	text-decoration:none;
	border:none !important;
	border-radius:0px !important;
}
body .site {
    background: none !important;
    border: none !important;
    border-radius: 0px !important;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05)  !important;
    max-width: 958px;
    padding: 0px !important;
}
#rav_price2133{ background:#B8A1C5; padding:2px 5px;}
#cat2133{background:#5A387A;}
.rav_inner_area a:hover {
	background:#300948;
	color:#ffffff;
}
#rav_vis{padding:32px 0px;}
#rav_limited{padding:20px 0px; font-weight:bold;}
#rav_p{padding:0px 0px; background: none repeat scroll 0 0 #B8A1C5;}
#rav_S{padding:0px 0px;}
#rav_a{padding:0px 0px; background: none repeat scroll 0 0 #B8A1C5;}
#rav_k{padding:0px 0px;}
#rav_s{padding:0px 0px; background: none repeat scroll 0 0 #B8A1C5;}

.rav_inner_san{ width:100%; float:left; margin:0px; padding:0px; background:#ffffff; border-bottom:1px solid #bcbcbc;}
#rav_price_san{background:#b8a1c5; float:left; margin:0px; }
.rav_inner_san_area {
    float: left;
    margin: 0;
    padding: 0 5px;
    width: 16.5%;
}

.rav_inner_san_area a {
    background: none repeat scroll 0 0 #523071;
    border: medium none !important;
    border-radius: 0 !important;
    color: #FFFFFF;
    float: left;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 17px;
    font-weight: normal;
    margin: 15px 18% 15px;
    padding: 10px 20px 12px;
    text-decoration: none;
}


.rav_inner_san_left {
    float: left;
    margin: 0;
    padding: 0;
    width: 30%;
}
.rav_inner_san_left p {
    color: #1E1E1E;
    float: left;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 24px;
    margin: 0;
    padding: 20px 0 20px 10px;
    width: 93%;
}
#rav_price12{background: none repeat scroll 0 0 #B8A1C5;}
#rav_price3{background: none repeat scroll 0 0 #B8A1C5;}
#rav_price2{background: none repeat scroll 0 0 #B8A1C5;}
</style>
<link href="../font/style.css" rel="stylesheet" type="text/css" />
<style>
  .tooltips p a {
    color: black;
  }
  .tooltips p a:hover {
    color: black;
    text-decoration: none;
   }
</style>
<div id="rav_container">
  <div id="rav_wrapper">
    <div class="rav_section">
      <h2 style="text-align: center;">Welcome Pet Business Professionals!</h2>
      <h2>Upgrade to a Business Membership to socialize with Pet Lovers from around the world representing your business! You'll enjoy all the great benefits of upgrading listed below!</h2>
      <div class="rav_heading">
      <div class="rav_heading_inner">
      <h3>Professional Membership </h3>
      </div>
      
        
      <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
                          <div class="rav_heading_area" id="cat<?php echo get_the_ID(); ?>">
                            <h3><a href="<?php echo get_permalink($loop->post->ID) ?>" class="tooltips" title="<?php echo get_post_meta( get_the_ID(), 'hover', true ); ?>"><?php the_title(); ?></a></h3>
                            
                          </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>      
                      
      </div>
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p></p>
      </div>
      
      <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
                  <div class="rav_inner_area" id="rav_price<?php echo get_the_ID(); ?>">
                  <p><?php echo $product->get_price_html(); ?></p>
                  </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>
      

            
      </div>
      
      <div class="rav_heading">
      <div class="rav_heading_inner" id="vis_area">
      <h3>Benefits</h3>
      </div>
      <div class="rav_heading_area" id="cat1">
      <h3 id="rav_vis"></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      <div class="rav_heading_area">
      <h3></h3>
      </div>
      </div>
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Stand out in Feeds with a Professional Title relevant to your business.">Professional Member Title</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>

      
      </div>
      
       <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Display your Business name, information and more to potential customers.">Upgraded Business Page</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>

      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Display and Post Updates as your Business in all Feeds.">Post Updates as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price1">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_p">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_s">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>

      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Contact potential customers by sending them Friend Requests.">Friend Requests as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Send Private Messages as your Business and reach out to your customers.">Messages as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Post on our Forums representing your Business. Post offers and information about your product to potential customers.">Forum Posts as Business</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      </div>
      
      <div class="rav_inner">
      <div class="rav_inner_left">
      <p><a href="#" class="tooltips" title="Stand out on the site with a larger display name in all site Feeds.">Larger Display Name in Feeds</a></p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area" id="rav_price12">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area" id="rav_price2">
      <img src="http://petspagetest.com/wp-content/uploads/2014/05/sprite.png" alt="" />
      </div>
      
      </div>
      

      
      
       
      </div>
 <!--     <div class="rav_inner">
      <div class="rav_inner_left">
      <p>Offers</p>
      </div>
      <div class="rav_inner_area" id="rav_price">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      <div class="rav_inner_area">
      <img src="<?php bloginfo('template_url') ?>/images/sprite.png" alt="" />
      </div>
      </div>-->
    <?php 
$flag="";
$user_id = get_current_user_id();
      $user_info = get_userdata($user_id);
      $user_roles = $user_info->roles;
      foreach($user_roles as $key => $value)
      {
        $value;
      } ?>

      <div class="rav_inner_san">
      <div class="rav_inner_san_left">
      <p>&nbsp;</p>
      </div>
              <?php
         // The Query
         query_posts('product_cat=advertising&post_type=product');
         // The Loop
         while ( have_posts() ) : the_post();
      ?>
        <div class="rav_inner_san_area" id="rav_price_<?php echo get_the_ID(); ?>">
        		<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
        </div>
      <?php
		endwhile;
		// Reset Query
		wp_reset_query();
      ?>
     </div>
      
    </div>
    
  </div>
</div>
    <h3 style="text-align: center; margin-top: 25px; color:#6F5092 !important;">As a Professional Member, you’ll also be eligible for our exclusive Social Booster and Social Growth Advertising Services.</h3>

		<h3 style="text-align: center; margin-top: 25px; color:#6F5092 !important;" >Please <a style="color: black;" href="mailto:karen@petspage.com">send an e-mail to us</a> or call <a style="color: black;" href="callto:2085144757">(208) 514-4757</a> to find out more!</h3>
<?php get_footer(); ?>