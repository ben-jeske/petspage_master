<?php
/**
 * The template for displaying BuddyPress content.
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */

get_header(); ?>
<style>
    .widget-area {
    float: right;
    width: 100%;
    }
</style>
	<!-- if widgets are loaded for any BuddyPress component, display the BuddyPress sidebar -->
	<?php if (
		( is_active_sidebar('members') && bp_is_current_component( 'members' ) && !bp_is_user() ) ||
		( is_active_sidebar('profile') && bp_is_user() ) ||
		( is_active_sidebar('groups') && bp_is_current_component( 'groups' ) && !bp_is_group() ) ||
		( is_active_sidebar('group') && bp_is_group() ) ||
		( is_active_sidebar('activity') && bp_is_current_component( 'activity' ) && !bp_is_user() ) ||
		( is_active_sidebar('blogs') && is_multisite() && bp_is_current_component( 'blogs' ) && !bp_is_user() ) ||
		( is_active_sidebar('forums') && bp_is_current_component( 'forums' ) && !bp_is_user() )
	): ?>
		<div class="page-right-sidebar">

	<!-- if not, hide the sidebar -->
	<?php else: ?>
		<div class="page-full-width">
	<?php endif; ?>

			<!-- BuddyPress template content -->
            <div id="mainbar" style="width: 69.2%;float: left;">
			<div id="primary" class="site-content" style="width: 100%;">

					<div id="content" role="main">

						<article>
						<?php while ( have_posts() ): the_post(); ?>
							<?php get_template_part( 'content', 'buddypress' ); ?>
							<?php comments_template( '', true ); ?>
						<?php endwhile; // end of the loop. ?>
						</article>

					</div><!-- #content -->

			</div><!-- #primary -->
            </div>
            
            <div id="sidebar" style="width: 26%;float: right;"> 
                <div id="scroller-anchor" ></div> 
                    <div id="scroller" style="margin-top:30px; width: 100%; margin-bottom: 50px;"> 
                        <?php echo adrotate_ad(490); ?>
                    </div>
                    <span style="margin-top: 50px;">
                        <?php get_sidebar('buddypress'); ?>
                    </span>
            </div>

		</div><!-- closing div -->
        <script type='text/javascript'>//<![CDATA[ 
$(function() {
    
  var a = function() {
    var b = $(window).scrollTop();
    
    var d = $("#scroller-anchor").offset().top;
    console.log(b+">"+d);
    var c=$("#scroller");
    if (b>d) {
      c.css({position:"fixed",top:"0px",width:"10% !important",transition:"2s"})
    } else {
      if (b<=d) {
        c.css({position:"relative",top:"",width:"10% !important"})
      }
    }
  };
  $(window).scroll(a);a()
});
//]]>  

</script>
<?php get_footer(); ?>
