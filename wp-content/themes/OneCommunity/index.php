<?php get_header(); ?>


<div id="content">


<div class="item-body" style="margin-left: -16px; margin-right: -16px;">


<?php
$temp = $wp_query;
$wp_query= null;
$wp_query = new WP_Query();
$wp_query->query('posts_per_page=4'.'&paged='.$paged);
while ($wp_query->have_posts()) : $wp_query->the_post();
?>


<?php
global $more;
$more = 0;
the_content();
?>



<?php endwhile; // end of loop
 ?>


<?php $wp_query = null; $wp_query = $temp;?>


</div>
</div>


<div id="sidebar">
	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar-blog')) : ?><?php endif; ?>
	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar-ad-blog')) : ?><?php endif; ?>
</div><!--sidebar ends-->

<?php get_footer(); ?>