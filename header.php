<?php
/**
 * The Header for your theme.
 *
 * Displays all of the <head> section and everything up until <div id="main">
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE > 8]>
<html class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if ! IE  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-2.ico" type="image/x-icon">
<!-- BuddyPress and bbPress Stylesheets are called in wp_head, if plugins are activated -->
<?php wp_head(); ?>
</head>

<body <?php if ( current_user_can('manage_options') ) : ?>id="role-admin"<?php endif; ?> <?php body_class(); ?>>

<?php do_action( 'buddyboss_before_header' ); ?>

<header id="masthead" class="site-header" role="banner">

	<div class="header-inner">

		<?php if (get_option("buddyboss_custom_logo", FALSE)!= FALSE) {
				$logo = get_option("buddyboss_custom_logo");
			} else {
				$logo = get_bloginfo("template_directory")."/images/logo.jpg";
			} ?>

		<div id="logo">
				<a href="<?php echo site_url() ?>" title="<?php _e( 'Home', 'buddypress' )?>"><img src="<?php echo $logo ?>"/></a>
		</div>
        
        <div class="custom-tagline">A Place for Social Pet Lovers!</div>

	</div>

	<nav id="site-navigation" class="main-navigation" role="navigation">
		<div class="nav-inner">

			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'nav-menu clearfix' ) ); ?>
		</div>
	</nav><!-- #site-navigation -->
</header><!-- #masthead -->

<?php do_action( 'buddyboss_after_header' ); ?>

<div id="main-wrap"> <!-- Wrap for Mobile -->
    <div id="mobile-header">
    	<div class="mobile-header-inner">
        	<div class="left-btn">
				<?php if (is_user_logged_in()){ ?>
                    <a href="#" id="user-nav" class="closed "></a>
                <?php }
				else if (!is_user_logged_in() && buddyboss_is_bp_active() && !bp_hide_loggedout_adminbar( false )){?>
                    <a href="#" id="user-nav" class="closed "></a>
                <?php } ?>
            </div>

            <div class="mobile-header-content">
            	<a class="mobile-site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
            </div>

            <div class="right-btn">
                <a href="#" id="main-nav" class="closed"></a>
            </div>
        </div>
    </div><!-- #mobile-header -->

    <div id="inner-wrap"> <!-- Inner Wrap for Mobile -->
    	<?php do_action( 'buddyboss_inside_wrapper' ); ?>
        <div id="page" class="hfeed site">
			<div id="swipe-area">
            </div>
            <div id="main" class="wrapper">
