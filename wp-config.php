<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'petspage_wordpress');

/** MySQL database username */
//define('DB_USER', 'petspage_secure');
define('DB_USER', 'root');
/** MySQL database password */
//define('DB_PASSWORD', 'petspage83704');
define('DB_PASSWORD', 'root');
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define(CUSTOM_USER_TABLE,'wp_users');
define(CUSTOM_USER_META_TABLE,'wp_usermeta');

define('WP_CACHE', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4-/E Fi:>s$Kq)i0}!>ZD-%`Px)9:S37G?QN>Muol3_<YXe:95d<zi0Q90o)9~q`');
define('SECURE_AUTH_KEY',  'a(31K,0rDn,JfD<m1ydB/7p#UQ-rDC})tMPsf|yqVa$reH4cv^3uD|e9?lBtT)T~');
define('LOGGED_IN_KEY',    'b*@yn(Hv^+Nk$&K4!(+MECxO#z,l>Q%+I1Y`ud}@N<i&TMgn0E)3@QQ,u{vVn5{2');
define('NONCE_KEY',        'V2cd-uxd(x-Tkd~H?(GP}v]N*1..9LU|raD$@ 3&IkoPqFIntz-`I;>n2qroG&QO');
define('AUTH_SALT',        '/2W+lYTn?>J|#-TPCH9d+v<mL#zXxzzBIYcD;mQxO?(]j`F3TPv[Ntcwoq,(O:D7');
define('SECURE_AUTH_SALT', '?~P?AJT1_oZ vus0MD!R9kcnW)HAmU%%d^}gDo1 :9AoXz1/G^fsI9hjH]L{xp-b');
define('LOGGED_IN_SALT',   '_v]Lto6+s^2E+X~- +Hx6E+$K++>X%(,1D9xSi(g^P8<HWRX`cn%55Y42o^2^ zR');
define('NONCE_SALT',       'T+5k1?z~Q +Gvt`WQ]-^aOomD9,)v8L7{=ej:*UXW((&@}/>46ytE7zMZLDzS{t$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'en_US');

define('BPFB_THUMBNAIL_IMAGE_SIZE', '575x575');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define('MULTISITE', false);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'petspage.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);

define('SCRIPT_DEBUG', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
