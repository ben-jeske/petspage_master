/*********************************************************************************
	Main Code
*/

$(document).ready(function(e) {
	showRecaptcha();
	/***************************************************************************
		Settings for Login form
	*/
	
	/* Settings for 'login_username' field */
	var lguntt = new $.Zebra_Tooltips($('#login_username'), {'background_color': '#664688', 'color': '#FFF'});
	
	lguntt.show($('#login_username'), true);
	lguntt.hide($('#login_username'), true);
	
	$('#login_username').focus(function(e) {
		if($(this).parent().hasClass('_5634'))
		{
			lguntt.show($(this), true);
		}
		else
		{
			lguntt.hide($(this), true);
		}
	});
	
	$('#login_username').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$(this).parent().addClass('_5634');
		}
		else
		{	
			$(this).parent().removeClass('_5634');
			lguntt.hide($(this), true);
		}
		
		if($(this).parent().hasClass('_5634'))
		{
			lguntt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	
	
	/* Settings for 'login_username' field */
	var lgpwtt = new $.Zebra_Tooltips($('#login_password'), {'background_color': '#664688', 'color': '#FFF'});
	
	lgpwtt.show($('#login_password'), true);
	lgpwtt.hide($('#login_password'), true);
	
	$('#login_password').focus(function(e) {
		if($(this).parent().hasClass('_5634'))
		{
			lgpwtt.show($(this), true);
		}
		else
		{
			lgpwtt.hide($(this), true);
		}
	});
	
	$('#login_password').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$(this).parent().addClass('_5634');
		}
		else
		{	
			$(this).parent().removeClass('_5634');
			lgpwtt.hide($(this), true);
		}
		
		if($(this).parent().hasClass('_5634'))
		{
			lgpwtt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	/* Final check on form submit */
	$('#login_form').submit(function(e) {
		
		var error = false;
		
		// displayname
		if(is_empty($('#login_username').val()))
		{
			$('#login_username').parent().addClass('_5634');
			error = true;
		}
		
		// username
		if(is_empty($('#login_password').val()))
		{
			$('#login_password').parent().addClass('_5634');
			error = true;
		}
		
		if(error)
		{
			
		}
		else
		{
			$.ajax(
				'/registermember/',
				{
					data: {
						action:'do_login',
						user_login:$('#login_username').val(),
						user_password:$('#login_password').val()
					},
					type: "post",
					dataType: "json"
				}
			).done(function(res) {
				if(res.error)
				{
					show_message_box('There was an error', res.message);
				}
				else
				{
					window.location = '/';
				}
			});
		}
		e.preventDefault();
		return false;
	});
	
	/**************************************************************************************
	 * Lost Password Form
	 */
	
	
	/* Settings for 'user_login' field */
	var uslgtt = new $.Zebra_Tooltips($('#user_login'), {'background_color': '#664688', 'color': '#FFF'});
	
	uslgtt.show($('#user_login'), true);
	uslgtt.hide($('#user_login'), true);
	
	$('#user_login').focus(function(e) {
		if($(this).parent().hasClass('_5634'))
		{
			uslgtt.show($(this), true);
		}
		else
		{
			uslgtt.hide($(this), true);
		}
	});
	
	$('#user_login').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$(this).parent().addClass('_5634');
		}
		else
		{	
			$(this).parent().removeClass('_5634');
			uslgtt.hide($(this), true);
		}
		
		if($(this).parent().hasClass('_5634'))
		{
			uslgtt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	
	/* Final check on form submit */
	$('.lost_reset_password').submit(function(e) {
		
		var error = false;
		
		// displayname
		if(is_empty($('#user_login').val()))
		{
			$('#user_login').parent().addClass('_5634');
			error = true;
		}
		
		if(error)
		{
			
		}
		else
		{
			$.ajax(
				'/registermember/',
				{
					data: {
						action:'do_reset',
						user_login:$('#user_login').val()
					},
					type: "post",
					dataType: "json"
				}
			).done(function(res) {
				$('#lost_password_popup').hide();
				_t(res);
				if(res.error)
				{
					show_message_box('There was an error', res.message);
				}
				else
				{
					show_message_box('Reset Successful', res.message);
				}
			});
		}
		e.preventDefault();
		return false;
	});
	
	/***************************************************************************
		Settings for Register form
	*/
	
	$('.uiStickyPlaceholderInput input').bind('keyup keydown change',function(e) {
		var placeholder = $(this).parent().find('.placeholder');
		if($(this).val() == '')
		{
			placeholder.stop().show();
		}
		else
		{
			placeholder.stop().hide();
		}
	});
	
	/* Settings for 'displayname' field */
	var dptt = new $.Zebra_Tooltips($('.displayname'), {'background_color': '#664688', 'color': '#FFF'});
	
	dptt.show($('.displayname'), true);
	dptt.hide($('.displayname'), true);
	
	$('.displayname').focus(function(e) {
		if($('#u_0_0').hasClass('_5634'))
		{
			dptt.show($(this), true);
		}
		else
		{
			dptt.hide($(this), true);
		}
	});
	
	$('.displayname').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$('#u_0_0').addClass('_5634');
		}
		else
		{	
			$('#u_0_0').removeClass('_5634');
			dptt.hide($(this), true);
		}
		
		if($('#u_0_0').hasClass('_5634'))
		{
			dptt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	
	/* Settings for 'username' field */
	var untt = new $.Zebra_Tooltips($('.username'), {'background_color': '#664688', 'color': '#FFF'});
	untt.hide($('.username'), true);
	
	$('.username').focus(function(e) {
		if($('#u_0_2').hasClass('_5634'))
		{
			untt.show($(this), true);
		}
		else
		{
			untt.hide($(this), true);
		}
	});
	
	$('.username').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$('#u_0_2').addClass('_5634');
		}
		else
		{	
			$('#u_0_2').removeClass('_5634');
			untt.hide($(this), true);
		}
		
		if($('#u_0_2').hasClass('_5634'))
		{
			untt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	/* Settings for 'emailaddress' field */
	var eatt = new $.Zebra_Tooltips($('.emailaddress'), {'background_color': '#664688', 'color': '#FFF'});
	eatt.hide($('.emailaddress'), true);
	
	$('.emailaddress').focus(function(e) {
		if($('#u_0_4').hasClass('_5634'))
		{
			eatt.show($(this), true);
		}
		else
		{
			eatt.hide($(this), true);
		}
	});
	
	$('.emailaddress').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$('#u_0_4').addClass('_5634');
		}
		else if(!valid_email(text))
		{
			$(this).addClass('invalid');
		}
		else
		{	
			$('#u_0_4').removeClass('_5634');
			eatt.hide($(this), true);
		}
		
		if($('#u_0_4').hasClass('_5634'))
		{
			eatt.hide($(this), true);
		}
	});
	/* End of Setting */
	
	/* Settings for 'password' field */
	var pwtt = new $.Zebra_Tooltips($('.password'), {'background_color': '#664688', 'color': '#FFF'});
	pwtt.hide($('.password'), true);
	
	$('.password').focus(function(e) {
		if($('#u_0_7').hasClass('_5634'))
		{
			pwtt.show($(this), true);
		}
		else
		{
			pwtt.hide($(this), true);
		}
	});
	
	$('.password').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$('#u_0_7').addClass('_5634');
		}
		else if(!is_password(text))
		{
			$('#u_0_7').addClass('_5634');
		}
		else
		{	
			$('#u_0_7').removeClass('_5634');
			pwtt.hide($(this), true);
		}
		
		if($('#u_0_7').hasClass('_5634'))
		{
			pwtt.hide($(this), true);
		}
	});
	
	/* End of Setting */
	
	/* Settings for 'confirmpassword' field */
	var cpwtt = new $.Zebra_Tooltips($('.confirmpassword'), {'background_color': '#664688', 'color': '#FFF'});
	cpwtt.hide($('.confirmpassword'), true);
	
	$('.confirmpassword').focus(function(e) {
		if($('#u_0_9').hasClass('_5634'))
		{
			cpwtt.show($(this), true);
		}
		else
		{
			cpwtt.hide($(this), true);
		}
	});
	
	$('.confirmpassword').blur(function(e) {
		var text = $(this).val();
		if(is_empty(text))
		{
			$('#u_0_9').addClass('_5634');
		}
		else if(!matches(text, $('.password')))
		{
			$('#u_0_9').addClass('_5634');
		}
		else
		{	
			$('#u_0_9').removeClass('_5634');
			cpwtt.hide($(this), true);
		}
		
		if($('#u_0_9').hasClass('_5634'))
		{
			cpwtt.hide($(this), true);
		}
	});
	
	/* End of Setting */
	
	var amwtt = new $.Zebra_Tooltips($('#agreement'), {position:'right', 'background_color': '#664688', 'color': '#FFF'});
	amwtt.hide($('#agreement'), true);
	$('#agreement').focus(function(e) {
		if($('#u_0_11').hasClass('_5634'))
		{
			amwtt.show($(this), true);
		}
		else
		{
			amwtt.hide($(this), true);
		}
	});
	
	$('#agreement').click(function(e) {
		amwtt.hide($(this), true);
			$('#u_0_11').removeClass('_5634');
	});
	
	/* Final check on form submit */
	$('#reg').submit(function(e) {
		
		var error = false;
		
		// displayname
		if(is_empty($('.displayname').val()))
		{
			$('#u_0_0').addClass('_5634');
			error = true;
		}
		
		// username
		if(is_empty($('.username').val()))
		{
			$('#u_0_2').addClass('_5634');
			error = true;
		}
		
		// emailaddress
		if(is_empty($('.emailaddress').val()))
		{
			$('#u_0_4').addClass('_5634');
			error = true;
		}
		else if(!valid_email($('.emailaddress').val()))
		{
			$(this).addClass('invalid');
			error = true;
		}
		
		// password
		if(is_empty($('.password').val()))
		{
			$('#u_0_7').addClass('_5634');
			error = true;
		}
		else if(!is_password($('.password').val()))
		{
			$('#u_0_7').addClass('_5634');
			error = true;
		}
		
		// confirmpassword
		if(is_empty($('.confirmpassword').val()))
		{
			$('#u_0_9').addClass('_5634');
			error = true;
		}
		else if(!matches($('.confirmpassword').val(), $('.password')))
		{
			$('#u_0_9').addClass('_5634');
			error = true;
		}
		
		if($('#agreement').prop('checked') != true)
		{
			$('#u_0_11').addClass('_5634');
			amwtt.show($('#agreement'), true);
			error = true;
		}
		else
		{
			$('#u_0_11').removeClass('_5634');
			amwtt.hide($('#agreement'), true);
		}
		
		
		
		if(error)
		{
			
		}
		else
		{
			$.ajax(
				'/registermember/',
				{
					data: {
						email:$('.emailaddress').val(),
						username:$('.username').val(),
						password:$('.password').val(),
						displayname:$('.displayname').val(),
						// Send Google reCaptcha Details
						recaptcha_challenge: Recaptcha.get_challenge(),
						recaptcha_response: Recaptcha.get_response()
						
					},
					type: "post",
					dataType: "json"
				}
			).done(function(res) {
				if(res.error)
				{
					show_message_box('There was an error', res.message);
					Recaptcha.reload();
				}
				else
				{
					window.location = res.redirect_to;
				}
			});
		}
		e.preventDefault();
		return false;
	});

});

function is_empty(text) {
	if(text == '')
	{
		return true;
	}
	else
	{
		return false;
	}
}

function valid_email(email) {
	var validate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
	return validate.test(email);
}

function is_password(pwd) {
	var validate = /[A-Z]/g;
	if(validate.test(pwd))
	{
		validate = /\d/g;
		if(validate.test(pwd))
		{
			return true;
		}
		return false;
	}
	return false;
}

function matches(text, obj) {
	if(obj.val() == text)
	{
		return true;
	}
	return false;
}

function _t(msg) {
	if(console != 'undefined')
	{
		console.log(msg);
	}
}